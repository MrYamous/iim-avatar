<?php

namespace App\Models\User;

use App\Models\Axe;
use App\Models\Challenge;
use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use App\Models\Clan;
use App\Models\Cour;
use App\Models\File;
use App\Models\Item;
use App\Models\Project;
use App\Models\Promotion;
use App\Models\Quiz\Answer;
use App\Models\Ssk;
use App\Models\Team;
use App\Models\UsersLevel;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use HasApiTokens;
    
    //protected $table = "users";
    const POINT = 'points';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'fk_clan_id', 'fk_avatar_id', 'fk_axe_id', 'fk_promotion_id'
    ];
    
    protected $appends = ["name", 'level', 'ranking', 'percent'];
    
    public function getNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }
    
    
    public function getChallenges()
    {
        return $this->belongsToMany(Challenge::class, 'challenges_has_users', 'fk_user_id', 'fk_challenge_id');
    }
    
    public function getClan()
    {
        return $this->hasOne(Clan::class, 'id', 'fk_clan_id');
    }
    
    public function getTeams()
    {
        return $this->belongsToMany(Team::class, "team_has_user", 'fk_user_id', 'fk_team_id');
    }
    
    public function getInvitedTeams()
    {
        return $this->belongsToMany(Team::class, "team_has_invited_user", 'fk_user_id', 'fk_team_id');
    }
    
    public function filterTeam()
    {
        $invitedTeams = $this->getInvitedTeams;
        $teams = $this->getTeams;
        
        return $invitedTeams->diff($teams);
    }
    
    public function getSsks()
    {
        return $this->belongsToMany(Ssk::class, 'ssks_users', 'fk_user_id', 'fk_ssk_users');
    }
    
    public function getAvatar()
    {
        return $this->hasOne(File::class, "id", 'fk_avatar_id');
    }
    
    public function getItems()
    {
        return $this->belongsToMany(Item::class, "items_has_users", "fk_user_id", "fk_items_id");
    }
    
    function getTotalAttribute()
    {
        //Get score of each ssk of user
        $this->load("getSsks");
        $nb = 1;
        foreach ($this->getSsks as $ssk) {
            $r = getScoreByUserAndSsk($ssk->id, $this->id);
            if (is_int($r)) {
                $nb = $nb + $r;
            }
        }
        
        return $nb;
    }
    
    public function getThischallenge($challenge)
    {
        foreach ($this->getChallenges as $cha) {
            if ($cha->id === $challenge->id) {
                return true;
            }
        }
        
        return false;
    }
    
    public function nextLevel()
    {
        if ($this->points === 0) {
            return null;
        }
        return UsersLevel::where("start", "<=", $this->points)->where("end", ">=", $this->points)->first();
    }
    
    public function getPercentAttribute()
    {
        if ($this->points === 0) {
            return 0;
        }
        dd($this);
        $level = UsersLevel::where("start", "<=", $this->points)->where("end", ">=", $this->points)->first();
        $end = $level->end;
        $point = $this->points;
        
        return round(($point / $end) * 100, 1);
        
    }
    
    public function getTeamWhereImCreator()
    {
        $user = $this;
        
        return Team::whereHas('getCreator', function ($query) use ($user) {
            $query->where('id', $user->id);
        })->get();
    }
    
    public function getLevelAttribute()
    {
        if (auth()->check()) {
            $level = UsersLevel::where("start", "<=", $this->points)->where("end", ">=", $this->points)->first();
            
            return $level ? $level->level : 0;
        }
    }
    
    
    public function getAxe()
    {
        return $this->hasOne(Axe::class, 'id', 'fk_axe_id');
    }
    
    public function getPromotion()
    {
        return $this->hasOne(Promotion::class, 'id', 'fk_promotion_id');
    }
    
    public function getFollowChallenges()
    {
        return $this->belongsToMany(Challenge::class, "user_has_follow_challenge", "fk_user_id", "fk_challenge_id");
    }
    
    
    public function getFollowTeams()
    {
        return $this->belongsToMany(Team::class, "user_has_follow_teams", "fk_user_id", "fk_team_id");
    }
    
    
    public function getAnswers()
    {
        return $this->belongsToMany(Answer::class, "user_has_answer", "fk_user_id", "fk_answer_id");
    }
    
    public function getConversations()
    {
        return $this->belongsToMany(Conversation::class, "user_has_conversation", "fk_user_id", "fk_conversation_id");
    }
    
    public function getMessages()
    {
        return $this->hasMany(Message::class, "fk_user_id", "id");
    }
    
    public function getRankingAttribute()
    {
        $userRank = User::all()->sortByDesc(User::POINT);
        $rankInstance = $this;
        $rankSearch = $userRank->search(function ($userRank) use ($rankInstance) {
            return $userRank->id === $rankInstance->id;
        });
        
        return $rankSearch + 1;
    }
    
    public function updateLastLogin()
    {
        $this->last_login = Carbon::now();
        $this->save();
    }
    
    public function getAllMyChallenges()
    {
        $chal = $this->getChallenges;
        $ta = $this->getTeams->load('getChallenges');
        $a = collect();
        foreach ($ta as $item) {
            $item->getChallenges->each(function ($e) use ($a) {
                $a->push($e);
            });
        }
        
        return $a->merge($chal);
    }
}
