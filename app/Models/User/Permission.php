<?php

namespace App\Models\User;

use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    protected $guarded = [];
}
