<?php

namespace App\Models\User;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    protected $table = "roles";

    protected $guarded = [];

    protected $hidden = [];

}
