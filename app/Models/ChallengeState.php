<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChallengeState extends Model
{
    protected $table = "cms_challenge_state";
    
    protected $guarded = [];
    
    
}
