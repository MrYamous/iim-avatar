<?php

namespace App\Models;

use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Cour extends Model
{
    public function users(){
        return $this->belongsToMany(User::class,'user_cour','cour_id','user_id');
    }
    public function teachers(){
        return $this->belongsToMany(User::class,'cour_teacher','cour_id','teacher_id');
    }

    public function scopeFinished($query){
        $query->whereDate('end_at','<',Carbon::now())->orWhereNull('end_at');
    }
}
