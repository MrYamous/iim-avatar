<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SskScore extends Model
{
    protected $table = "ssk_score";

    protected $guarded = [];
}
