<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Constrainte extends Model
{
    protected $table = "cms_constraint";

    protected $guarded = [];
}
