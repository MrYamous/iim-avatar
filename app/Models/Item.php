<?php

namespace App\Models;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Item extends Model
{
    protected $table = "cms_items";
    protected $appends = ["buy"];

    protected $guarded = [];

    public function getUsers()
    {
        return $this->belongsToMany(User::class, "items_has_users", "fk_items_id", "fk_user_id");
    }

    public function getFile()
    {
        return $this->hasOne(File::class, 'id', 'fk_file_id');
    }

    public function getBuyAttribute()
    {
        $buy = auth()->user()->getItems->contains($this->id);

        return $buy;
    }

}
