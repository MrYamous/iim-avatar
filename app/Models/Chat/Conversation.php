<?php

namespace App\Models\Chat;

use App\Models\Team;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = "cms_conversations";
    
    protected $guarded = [];
    
    public function getMessages()
    {
        return $this->hasMany(Message::class, "fk_conversation_id", "id");
    }
    
    public function getMembers()
    {
        return $this->belongsToMany(User::class, "user_has_conversation", "fk_conversation_id", "fk_user_id");
    }
    
    public function getTeam()
    {
        return $this->hasOne(Team::class, 'id', 'fk_team_id');
    }
    
    public function unseen()
    {
        $user = auth()->user();
        // REGARDER SI LE CREATED AT EST SUPERIEUR AU LAST LOGIN DE LUSER
        
        $messages = $this->getMessages;
        $m = collect();
        $messages->each(function ($e) use ($user, $m) {
            if ($e->created_at > Carbon::parse($user->last_login)) {
                return $m->push($e);
            }
        });
        
        return $m;
    }
}
