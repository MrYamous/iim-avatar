<?php

namespace App\Models\Chat;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "cms_messages";
    protected $guarded = [];
    protected $casts = [
        'created_at' => 'string',
    ];

    public function getConversation()
    {
        return $this->hasOne(Conversation::class, "id", 'fk_conversation_id');
    }

    public function getUser()
    {
        return $this->hasOne(User::class, "id", 'fk_user_id');
    }

}
