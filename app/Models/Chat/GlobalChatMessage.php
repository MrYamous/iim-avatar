<?php

namespace App\Models\Chat;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class GlobalChatMessage extends Model
{
    protected $table = "cms_globals_chats";
    
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'string',
    ];
    
    public function getUser()
    {
        return $this->hasOne(User::class, "id", "fk_user_id");
    }

}
