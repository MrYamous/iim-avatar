<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeChallenge extends Model
{
    protected $table = "type_challenges";

    protected $guarded = [];
}
