<?php

namespace App\Models;

use App\Events\teamIsComplete;
use App\Events\userJoinTeam;
use App\Models\Chat\Conversation;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model
{
    const relation = ["getCreator", "getConfirmMembers", "getInvitedMembers", "getChallenges", 'getConversation'];
    protected $table = "cms_teams";
    
    protected $guarded = [];
    
    
    public function getCreator()
    {
        return $this->hasOne(User::class, 'id', 'fk_creator_id');
    }
    
    public function getConfirmMembers()
    {
        return $this->belongsToMany(User::class, "team_has_user", 'fk_team_id', 'fk_user_id');
    }
    
    public function getInvitedMembers()
    {
        return $this->belongsToMany(User::class, "team_has_invited_user", 'fk_team_id', 'fk_user_id');
    }
    
    public function getChallenges()
    {
        return $this->belongsToMany(Challenge::class, "team_has_challenge", 'fk_team_id', 'fk_challenge_id');
    }
    
    public function getState($challengeId)
    {
        return $this->hasOne(ChallengeState::class, 'id', DB::table("team_has_challenge")->where([['fk_challenge_id', $challengeId], ['fk_team_id', $this->id]])->first()->fk_state_id);
    }
    
    public function isInvited(User $user)
    {
        return DB::table("team_has_invited_user")
            ->where('fk_team_id', $this->id)
            ->where('fk_user_id', $user->id)
            ->first();
    }
    
    public function joinTeam(User $user)
    {
        if (DB::table("team_has_user")
            ->where('fk_team_id', $this->id)
            ->where('fk_user_id', $user->id)
            ->first()) return false;
        
        $this->getInvitedMembers()->detach($user->id);
        $this->getConfirmMembers()->attach($user->id);
        
        event(new userJoinTeam($this, $this->getCreator));
        $this->isCompleteTeam();
        
        return true;
    }
    
    public function getConversation()
    {
        return $this->belongsTo(Conversation::class, "fk_conversation_id", "id");
    }
    
    public function members()
    {
        $all = collect();
        $members = $this->getConfirmMembers->load('getAvatar');
        $invited = $this->getInvitedMembers->load('getAvatar');
        
        $invited->each(function ($m) use ($members, $all) {
            $all->push($this->formatMember($m, $members));
        });
        
        $members->each(function ($m) use ($invited, $members, $all) {
            if (!$invited->contains($m->id)) {
                $all->push($this->formatMember($m, $members));
            }
        });
        
        return $all;
    }
    
    private function formatMember($m, $arr)
    {
        if ($arr->contains($m)) {
            $m->confirmed = true;
            $m->creator = false;
        } else {
            $m->confirmed = false;
            $m->creator = false;
        }
        if ($m->id === $this->fk_creator_id) {
            $m->confirmed = true;
            $m->creator = true;
        }
        
        return $m;
    }
    
    private function isCompleteTeam()
    {
        $members = $this->getConfirmMembers;
        $invited = $this->getInvitedMembers;
        if ($invited->diff($members)->isEmpty()) {
            // IS COMPLETE TEAM
            $this->complete = 1;
            $this->save();
            event(new teamIsComplete($this, $this->getCreator));
        };
    }
}
