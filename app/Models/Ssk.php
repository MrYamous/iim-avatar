<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ssk extends Model
{
    protected $table = "cms_ssk";

    protected $guarded = [];

    public function getScore()
    {
        return $this->hasMany(SskScore::class, "fk_ssk_id", "id");
    }
}
