<?php

namespace App\Models;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Render extends Model
{
    protected $table = "renders";

    protected $guarded = [];


    public function getUser()
    {
        return $this->hasOne(User::class, "id", 'fk_user_id');
    }

    public function getChallenges()
    {
        return $this->belongsToMany(Challenge::class, "challenges_has_renderer_members", "fk_render_id", "fk_challenge_id");
    }

    public function getFile()
    {
        return $this->hasOne(File::class, "id", "fk_file_id");
    }
}
