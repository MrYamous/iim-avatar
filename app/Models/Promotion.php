<?php

namespace App\Models;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model{
    //
    protected $table = 'cms_promotions';
    protected $guarded = [];

    public function getUsers()
    {
        return $this->hasMany(User::class, "fk_promotion_id", 'id');
    }
}
