<?php

namespace App\Models;

use App\Events\userJoinTeam;
use App\Models\User\User;
use Doctrine\DBAL\Types\Type;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Challenge extends Model
{
    const relation = ["getClans", "getLevel", "getConstraintes", "getSsks", "getUsers", 'getTeams'];
    const TYPE_TEAM = "team";
    const TYPE_SINGLE = "single";
    protected $table = "challenges";
    private $isTeamOfUser;
    protected $guarded = [];
    
    protected $appends = ["array_states"];
    
    public function getArrayStatesAttribute()
    {
        return json_decode($this->states);
    }
    
    //POLOLO LA QUALITER DE CE CODE MON GARS
    public function scopeDisponibleChallenge($query, User $user)
    {
        $all = collect();
        $running = Challenge::runningChallenge(auth()->user())->get();
        $done = Challenge::doneChallenge(auth()->user())->get();
        $running->merge($done)->unique('id')->each(function ($e) use ($all) {
            $all->push($e->id);
        });
        
        return $query->where('validated', 1)->whereNotIn('id', $all);
    }
    
    public function scopeRunningChallenge($query, User $user)
    {
        if (!$this->isTeamOfUser) {
            $this->getTeamsOfUser($user);
        }
        $teams = $this->isTeamOfUser;
        
        return $query->where('validated', 1)->whereHas('getUsers', function ($subQ) use ($user) {
            $subQ->where('users.id', $user->id);
        })->orWhereHas('getTeams', function ($subQ) use ($teams) {
            $subQ->whereIn('cms_teams.id', $teams);
        });
    }
    
    public function scopeDoneChallenge($query, User $user)
    {
        if (!$this->isTeamOfUser) {
            $this->getTeamsOfUser($user);
        }
        $teams = $this->isTeamOfUser;
        
        return $query->where('validated', 1)->whereHas('getWinners', function ($subQ) use ($user) {
            $subQ->where('challenges_has_winners.fk_winner_id', $user->id);
        })->whereHas('getWinners', function ($subQ) use ($teams) {
            $subQ->whereIn('challenges_has_winners.fk_winner_id', $teams);
        });
    }
    
    
    public function getClans()
    {
        return $this->belongsToMany(Clan::class, "challenges_has_clans", "fk_challenge_id", "fk_clan_id");
    }
    
    public function getImages()
    {
        return $this->belongsToMany(File::class, "challenges_has_files", "fk_challenge_id", "fk_file_id");
    }
    
    public function getConstraintes()
    {
        return $this->belongsToMany(Constrainte::class, "challenges_has_constraint", "fk_challenge_id", "fk_constraint_id");
    }
    
    public function getLevel()
    {
        return $this->hasOne(TypeChallenge::class, "id", "fk_type_id");
    }
    
    public function getSsks()
    {
        return $this->belongsToMany(Ssk::class, "challenges_has_ssk", "fk_challenge_id", "fk_ssk_id");
    }
    
    public function getOwner()
    {
        return $this->hasOne(User::class, "id", "fk_owner_id");
    }
    
    public function getUsers()
    {
        return $this->belongsToMany(User::class, "challenges_has_users", 'fk_challenge_id', 'fk_user_id');
    }
    
    public function getRenderedUser()
    {
        return $this->belongsToMany(Challenge::class, "challenges_has_renderer_members", "fk_challenge_id", "fk_user_id");
    }
    
    public function getRender()
    {
        return $this->belongsToMany(Render::class, "challenges_has_renderer_members", "fk_challenge_id", "fk_render_id");
    }
    
    public function getTeams()
    {
        return $this->belongsToMany(Team::class, "team_has_challenge", "fk_challenge_id", "fk_team_id");
    }
    
    
    public function userHasSendRender($u)
    {
        foreach ($this->getRender as $render) {
            if ($u->id === $render->fk_user_id) {
                return true;
            }
        }
        
        return false;
    }
    
    public function getRenders()
    {
        return $this->belongsToMany(Render::class, "challenges_has_renderer_members", "fk_challenge_id", 'fk_render_id');
    }
    
    public function getWinners()
    {
        if ($this->type === "single") {
            return $this->belongsToMany(User::class, "challenges_has_winners", "fk_challenge_id", "fk_winner_id");
        } else {
            return $this->belongsToMany(Team::class, "challenges_has_winners", "fk_challenge_id", "fk_winner_id");
            
        }
        
    }
    
    public function joinChallenge($user = false, $team = false)
    {
        
        if ($this->type === "single") {
            if (DB::table("challenges_has_users")->where("fk_challenge_id", $this->id)->where("fk_user_id", $user->id)->first()) {
                return false;
            }
            $this->getUsers()->attach($user->id);
            
            //            event(new userJoinChallenge($this));
            
            return true;
        }
        
        if ($this->type === "team") {
            if (DB::table("team_has_challenge")->where("fk_challenge_id", $this->id)->where("fk_team_id", $team->id)->first()) {
                return false;
            }
            
            $this->getTeams()->attach($team->id);
            
            return true;
        }
        
    }
    
    private function getTeamsOfUser(User $user): self
    {
        $teams = collect();
        $user->getTeams->each(function ($e) use ($teams) {
            return $teams->push($e->id);
        });
        $this->isTeamOfUser = $teams;
        
        return $this;
    }
    
    /**
     * @param User $user
     */
    public function isFollow(User $user)
    {
        if ($user->getFollowChallenges->contains($this)) {
            return true;
        }
        
        return false;
    }
}
