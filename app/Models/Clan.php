<?php

namespace App\Models;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Clan extends Model
{
    protected $table = "clans";
    
    protected $guarded = [];
    
    protected $appends = ["score", "leaderboard"];
    
    public function countChallengeOfUsers()
    {
        $a = collect();
        $this->getUsers->each(function ($e) use ($a) {
            $a->push($e->getAllMyChallenges()->count());
        });
        
        return $a->sum();
    }
    
    public function getUsers()
    {
        return $this->hasMany(User::class, 'fk_clan_id', 'id');
    }
    
    public function getImages()
    {
        return $this->belongsToMany(File::class, "clans_has_file", "fk_clan_id", "fk_file_id");
    }
    
    public function getScoreAttribute()
    {
        $this->load("getUsers");
        
        return $this->getUsers->sum(User::POINT);
    }
    
    public function getLeaderboardAttribute()
    {
        $clan = Clan::all()->sortByDesc('score');
        $clanInstance = $this;
        $clanSearch = $clan->search(function ($clan) use ($clanInstance) {
            return $clan->id === $clanInstance->id;
        });
        
        return $clanSearch + 1;
        
    }
    
    public function getLogo()
    {
        return $this->hasOne(File::class, "id", "fk_logo_id");
    }
}
