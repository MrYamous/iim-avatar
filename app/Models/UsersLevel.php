<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersLevel extends Model
{
    protected $table = "cms_users_level";

    protected $guarded = [];
}
