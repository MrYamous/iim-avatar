<?php

namespace App\Models;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Axe extends Model
{
    protected $table = 'cms_axes';
    protected $guarded = [];

    public function getUsers()
    {
        return $this->hasMany(User::class, "fk_axe_id", 'id');
    }

}
