<?php

namespace App\Models\Quiz;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $table = "user_has_answer";
    
    protected $guarded = [];
    
    public function getUsers()
    {
        return $this->belongsTo(User::class, "fk_user_id", "id");
    }
    
    public function getAnswer()
    {
        return $this->belongsTo(Answer::class, 'fk_answer_id', 'id');
    }
}
