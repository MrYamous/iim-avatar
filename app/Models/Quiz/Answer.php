<?php

namespace App\Models\Quiz;

use App\Models\Clan;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "cms_answers";
    protected $guarded = [];

    public function getQuestions()
    {
        return $this->belongsToMany(Question::class, 'question_has_response', 'fk_answer_id', "fk_question_id");
    }

    public function getClan()
    {
        return $this->hasOne(Clan::class, "id", 'fk_clan_id');
    }

}
