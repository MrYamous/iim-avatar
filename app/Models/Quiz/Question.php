<?php

namespace App\Models\Quiz;

use App\Models\File;
use App\Models\Quiz\Answer;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "cms_questions";
    protected $guarded = [];
    
    public function getAnswers()
    {
        return $this->belongsToMany(Answer::class, 'question_has_answer', "fk_question_id", "fk_answer_id");
    }
    
    public function getFile()
    {
        return $this->hasOne(File::class, "id", "fk_file_id");
    }

}
