<?php

namespace App\Providers;

use App\Models\Challenge;
use App\Models\Clan;
use App\Models\Cour;
use App\Models\User\User;
use App\Observers\ChallengeObserver;
use App\Observers\ClanObserver;
use App\Observers\CourObserver;
use App\Observers\ProjectObserver;
use App\Observers\UserObserver;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Carbon::setLocale(config('app.locale'));
        User::observe(UserObserver::class);
        Challenge::observe(ChallengeObserver::class);
        Project::observe(ProjectObserver::class);
        Clan::observe(ClanObserver::class);
        Cour::observe(CourObserver::class);
        
        Passport::routes();
    }
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
