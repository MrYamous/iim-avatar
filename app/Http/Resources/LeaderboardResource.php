<?php

namespace App\Http\Resources;

use App\Http\Resources\Item\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LeaderboardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'score'       => $this->score,
            'leaderboard' => $this->leaderboard,
            'title'       => $this->title,
            "members"     => UserResource::collection($this->getUsers->load("getAvatar")->sortByDesc('points')),
            "logo"        => new FileResource($this->getLogo),
            "images"     => FileResource::collection($this->getImages)
        ];
    }
}
