<?php

namespace App\Http\Resources;

use App\Http\Resources\Clan\ClanResource;
use App\Http\Resources\Item\ItemResource;
use App\Models\Axe;
use App\Models\Promotion;
use Illuminate\Http\Resources\Json\JsonResource;

class UserEditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"          => $this->id,
            'clans'       => new ClanResource($this->whenLoaded("getClan")),
            'items'       => ItemResource::collection($this->whenLoaded("getItems")),
            'slug'        => $this->slug,
            //            'gold'   => $this->gold,
            'name'        => $this->name,
            'first_name'  => $this->first_name,
            'last_name'   => $this->last_name,
            'email'       => $this->email,
            'avatar'      => $this->whenLoaded("getAvatar"),
            'level'       => $this->level,
            'description' => $this->description,
            'points'      => $this->points,
            'gold'        => $this->gold,
            'axe'         => $this->fk_axe_id,
            'promotion'   => $this->fk_promotion_id,
            'count_teams' => $this->getTeams->count(),
            "axes"        => Axe::all(),
            "promotions"   => Promotion::all(),
        ];
    }
}
