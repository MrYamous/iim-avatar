<?php

namespace App\Http\Resources\Item;


use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "cost" => $this->cost,
            "users" => UserResource::collection($this->whenLoaded("getUsers")),
            "file" => new FileResource($this->whenLoaded("getFile")),
            "buy" => $this->buy,

        ];
    }
}
