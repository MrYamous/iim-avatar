<?php

namespace App\Http\Resources\Chat;

use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"             => $this->id,
            "name"           => $this->name,
            "messages"       => ChatMessageResource::collection($this->whenLoaded("getMessages")),
            "members"        => UserResource::collection($this->whenLoaded('getMembers')),
            "last_send"      => $this->getMessages->isNotEmpty() ? $this->getMessages->last()->created_at : null,
            "message_count"  => $this->whenLoaded("getMessages")->count(),
            "unseen_message" => $this->unseen()->count(),
            "team"           => $this->getTeam ? new TeamResource($this->getTeam) : null
        ];
    }
}
