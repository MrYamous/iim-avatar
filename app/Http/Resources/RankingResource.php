<?php

namespace App\Http\Resources;

use App\Http\Resources\Clan\ClanResource;
use App\Http\Resources\Item\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RankingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'points' => $this->points,
            'ranking' => $this->ranking,
            'name' => $this->name,
            'level' => $this->level,
            'clans' => new ClanResource($this->whenLoaded("getClan")),
            'avatar' => new FileResource($this->whenLoaded("getAvatar"))
        ];
    }
}
