<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserClanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->load('getUsers');
        
        return [
            "title"            => $this->title,
            "description"      => $this->description,
            "rgb_color"        => $this->rgb_color,
            "hexa_color"       => $this->hexa_color,
            "score"            => $this->score,
            "leaderboard"      => $this->leaderboard,
            "total_user"       => $this->getUsers->count(),
            "image"            => $this->getImages,
            "logo"             => $this->getLogo,
            "users"            => UserResource::collection($this->getUsers->load('getAvatar')->sortByDesc('points')),
            "challenges_count" => $this->countChallengeOfUsers()
        ];
    }
}
