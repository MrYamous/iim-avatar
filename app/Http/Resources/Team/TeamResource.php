<?php

namespace App\Http\Resources\Team;

use App\Http\Resources\Chat\ConversationResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"           => $this->id,
            "name"         => $this->name,
            "slug"         => $this->slug,
            "members"      => UserTeamResource::collection($this->members()),
            'creator'      => new UserResource($this->whenLoaded('getCreator')),
            'conversation' => new ConversationResource($this->whenLoaded('getConversation')),
        ];
    }

}
