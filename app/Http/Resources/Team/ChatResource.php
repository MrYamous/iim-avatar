<?php

namespace App\Http\Resources\Team;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "message" => $this->message,
            "users"   => new UserResource($this->whenLoaded('getUser')),
            "team"   => new TeamResource($this->whenLoaded('getTeam')),
            "send_to" => $this->created_at
        ];
    }
}
