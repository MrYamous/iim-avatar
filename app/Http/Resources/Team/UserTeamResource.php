<?php

namespace App\Http\Resources\Team;

use App\Http\Resources\Clan\ClanResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"        => $this->id,
            'name'      => $this->name,
            'thumbnail' => $this->getAvatar ? $this->getAvatar->file : null,
            'confirmed' => $this->confirmed,
            'creator'   => $this->creator,
            'clans'     => new ClanResource($this->whenLoaded("getClan")),
        ];
    }
}
