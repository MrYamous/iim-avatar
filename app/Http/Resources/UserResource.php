<?php

namespace App\Http\Resources;

use App\Http\Resources\Clan\ClanResource;
use App\Http\Resources\Item\ItemResource;
use App\Models\UsersLevel;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"          => $this->id,
            'clans'       => new ClanResource($this->whenLoaded("getClan")),
            'items'       => ItemResource::collection($this->whenLoaded("getItems")),
            'slug'        => $this->slug,
            'axe'         => $this->getAxe,
            'promo'       => $this->getPromotion,
            'name'        => $this->name,
            'first_name'  => $this->first_name,
            'last_name'   => $this->last_name,
            'email'       => $this->email,
            'avatar'      => $this->whenLoaded("getAvatar"),
            'level'       => $this->level,
            'description' => $this->description,
            'points'      => $this->points,
            'gold'        => $this->gold,
            'count_teams' => $this->getTeams->count(),
            'next_level' => $this->nextLevel(),
            'percent' => $this->percent
        ];
    }
}
