<?php

namespace App\Http\Resources\Clan;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ClanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title"      => $this->title,
            "description" => $this->description,
            "rgb_color" => $this->rgb_color,
            "hexa_color" => $this->hexa_color,
            "score"      => $this->score,
            "total_user" => $this->whenLoaded($this->getUsers->count()),
            "image" => $this->getImages,
            "logo" => $this->getLogo,
            "users"      => UserResource::collection($this->getUsers)
        ];
    }
}
