<?php

namespace App\Http\Resources;

use App\Http\Resources\Clan\ClanResource;
use App\Http\Resources\Team\TeamResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ChallengesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            "id"                 => $this->id,
            "title"              => $this->title,
            "description"        => $this->description,
            "reglementation"     => $this->reglementation,
            "objectifs"          => $this->objectifs,
            "rendu"          => $this->rendu,
            "point_contribution" => $this->point_contribution,
            "point_win"          => $this->point_win,
            "type"               => $this->type,
            "start_date"         => Carbon::parse($this->start_date),
            "end_date"           => Carbon::parse($this->end_date),
            "teams"              => TeamResource::collection($this->whenLoaded("getTeams")),
            "clans"              => ClanResource::collection($this->whenLoaded("getClans")),
            //"level" => TypeChallengeResource::collection($this->whenLoaded("getLevel")),
            "owner"              => new UserResource($this->whenLoaded("getOwner")),
            "users"              => UserResource::collection($this->whenLoaded("getUsers")),
            "file"               => $this->file,
            "state"              => $this->array_states,
            "count_state"        => count($this->array_states),
            "following"          => $this->isFollow(auth()->user())
        ];
    }
}
