<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    public function storeFile(Request $request)
    {
        if ($request->hasFile('file')) {
            $ext = $request->file("file")->getClientOriginalExtension();
            $img = null;
            $name = false;
            $id = false;
            
            if ($request->has('name')) {
                $name = $request->name;
            }
            if (Auth::check()) {
                $id = Auth::user()->id;
            }
            if ($ext === "png" || $ext === "jpg" || $ext === "jpeg" || $ext === "webp" || $ext === "gif") {
                $img = $this->uploadImage($request->file('file'), isset($request->title) ? $request->title : "title" . "-" . str_random(8) . '-og');
            } else {
                $img = $this->saveFile($request, $request->file("file"), $ext, $name, $id);
            }
            
            return response()->json(
                [
                    "status" => "success",
                    "data"   => $img,
                ]
            );
        } else {
            return response()->json(
                [
                    "status" => "error",
                    "data"   => "No file provided to payload",
                ], 500
            );
        }
    }
    
    /*public function getFile(Request $request, $name)
    {
        $path = $this->getPathOfFile($name);
        if (is_array($path)) {
            if ($path[0] === "error") {
                return response()->json([
                    "status" => $path[0],
                    "message" => $path[1]
                ]);
            } else {
                $file = $path[0];
                $path = $path[1];
                $headers = ['Content-Type:' . "application/" . $file->type];
                return response()->download($file->file, $file->name . $file->type, $headers);
            }
        }


        dd($name, $path);
    }*/
    
    public function deleteFile(Request $request, $id)
    {
        $file = File::where('id', $id)->first();
        
        if (!$file) {
            return response()->json(
                [
                    "status"  => "error",
                    "message" => "This id correspond to any image in database",
                ], 500
            );
        }
        
        $file->delete();
        
        return response()->json(
            [
                "status"  => "success",
                "message" => "File successfully deleted",
            ]
        );
    }
    
}
