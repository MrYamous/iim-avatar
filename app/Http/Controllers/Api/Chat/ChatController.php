<?php

namespace App\Http\Controllers\Api\Chat;

use App\Events\Chat\PostConversationMessage;
use App\Http\Controllers\Controller;
use App\Http\Resources\Chat\ChatMessageResource;
use App\Http\Resources\Chat\ConversationResource;
use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use App\Models\User\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        // Return Private Chat For Current User
        return ConversationResource::collection(auth()->user()->getConversations->load('getMessages', "getMessages.getUser", "getMessages.getUser.getAvatar"));
    }

    /**
     * @param Request $request
     * @param $conversation
     * @return ConversationResource|\Illuminate\Http\JsonResponse
     */
    public function storeMessage(Request $request, $conversation)
    {
        $request->validate([
            "message" => "required|string"
        ]);

        if ($conversation === 'global') {
            $conversation = Conversation::where('id', 1)
                ->with("getMessages", "getMessages.getUser", "getMessages.getUser.getAvatar")
                ->first();
        } else {
            $conversation = Conversation::find($conversation);
            if (!$conversation) return response()->json(["message" => "No conversation found for id $conversation"], 500);
        }

        $message = Message::create([
            'message'            => $request->get('message'),
            'fk_user_id'         => auth()->user()->id,
            'fk_conversation_id' => $conversation->id,
        ]);

        $conversation->load('getMembers', 'getMessages', "getMessages.getUser", "getMessages.getUser.getAvatar");

        broadcast(new PostConversationMessage($message, $conversation))->toOthers();

        return new ConversationResource($conversation);
    }

    public function createConversation(Request $request)
    {
        $request->validate([
            'name'    => 'string',
            'members' => 'array|required'
        ]);
        $conv = new Conversation();
        $conv->name = $request->has('name') ? $request->get('name') : null;
        $conv->save();
        $a = [];
        foreach ($request->get('members') as $item) {
            $u = User::find($item['id']);
            if (!$u) {
                $conv->delete();

                return response()->json([
                    'message' => "No user found for id " . $item['id']
                ], 500);
            }
            $a[] = $u->id;
        }
        $conv->getMembers()->attach($a);
        $conv->load('getMembers', 'getMessages');

        return new ConversationResource($conv);
    }

    public function show($conversation)
    {
        if ($conversation === 'global') {
            $conversation = Conversation::where('id', 1)
                ->first();
        } else {
            $conversation = Conversation::orderBy('updated_at', 'DESC')
                ->where('id', $conversation)
                ->first();
            if (!$conversation) return response()->json(["error" => "Aucune conversation n'a été trouvé chat : $conversation"], 500);
        }
        $conversation->load("getMessages", "getMessages.getUser", "getMessages.getUser.getAvatar");
        return ChatMessageResource::collection($conversation ? $conversation->getMessages : []);
    }

}
