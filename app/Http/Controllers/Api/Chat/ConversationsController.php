<?php

namespace App\Http\Controllers\Api\Chat;

use App\Http\Controllers\Controller;
use App\Models\Chat\Conversation;

class ConversationsController extends Controller
{

    public function index()
    {
        return response()->json(Conversation::all());
    }

}
