<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserClanResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getClan(Request $request)
    {
        return new UserClanResource(auth()->user()->getClan->load('getUsers', 'getUsers.getAvatar'));
    }
}
