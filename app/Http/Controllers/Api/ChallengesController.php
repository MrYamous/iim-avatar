<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ChallengesResource;
use App\Http\Resources\Team\TeamResource;
use App\Models\Challenge;
use App\Models\Team;
use App\Models\User\User;
use Carbon\Carbon;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChallengesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $type
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request, $type)
    {
        $challenges = collect();
        if ($type === "all") {
            $challenges = Challenge::disponibleChallenge(auth()->user())->with("getTeams", "getClans", "getOwner", "getUsers", 'getUsers.getAvatar', "getImages")->get();
        } elseif ($type === "running") {
            $challenges = Challenge::runningChallenge(auth()->user())->with("getTeams", "getClans", "getOwner", "getUsers", 'getUsers.getAvatar', "getImages")->get();
        } elseif ($type === "done") {
            $challenges = Challenge::doneChallenge(auth()->user())->with("getTeams", "getClans", "getOwner", "getUsers", 'getUsers.getAvatar', "getImages")->get();
        } elseif ($type === "follow") {
            $challenges = auth()->user()->getFollowChallenges->load("getTeams", "getClans", "getOwner", "getUsers", 'getUsers.getAvatar', "getImages");
        }
        
        return ChallengesResource::collection($challenges);
    }
    
    public function updateState(Request $request, $id)
    {
        $c = Challenge::find($id);
        if (!$c) {
            return response()->json([
                "message" => "Challenge with id: $id not found"
            ], 500);
        }
        
        $state = $request->state;
        $chos = $c->array_states[ $state ];
        DB::table("challenges_has_users")->where('fk_challenge_id', $c->id)->update([
            "state" => $chos
        ]);
        
        return new ChallengesResource($c);
        
    }
    
    // Add type(level difficulty) in search function when fixed
    public function search(Request $request)
    {
        if ($request->has('challenges')) {
            $search = $request->get('challenges');
            $challenges = Challenge::where('title', 'LIKE', "%" . $search . "%")->orWhere('slug', 'LIKE', "%" . $search . "%");
            
            return ChallengesResource::collection($challenges);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
    
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return ChallengesResource
     */
    public function store(Request $request)
    {
        $challenge = new Challenge();
        $challenge->title = $request->title;
        $challenge->description = $request->description;
        //        $challenge->objectifs = $request->objectifs;
        //        $challenge->reglementation = $request->reglementation;
        //        $challenge->rendu = $request->rendu;
        $challenge->point_contribution = $request->point_contribution;
        $challenge->point_win = $request->point_win;
        $challenge->states = json_encode($request->states);
        $challenge->validated = 0;
        $challenge->start_date = Carbon::createFromFormat('Y-m-d', $request->start_date);
        $challenge->end_date = Carbon::createFromFormat('Y-m-d', $request->end_date);
        $challenge->save();
        
        return new ChallengesResource($challenge);
    }
    
    
    public function getFollow(Request $request)
    {
        return ChallengesResource::collection(auth()->user()->getFollowChallenges);
    }
    
    public function setFollow(Request $request, $challenge)
    {
        
        $t = Challenge::find($challenge);
        if (!$t) {
            return response()->json([
                'message' => 'no team for id $team'
            ]);
        }
        
        if (auth()->user()->getFollowChallenges->contains($t)) {
            return response()->json([
                "error" => "Already here"
            ], 500);
        }
        
        auth()->user()->getFollowChallenges()->attach($t->id);
        
        return ChallengesResource::collection(auth()->user()->getFollowChallenges);
    }
    
    public function getChallengeOfUser(Request $request)
    {
        return ChallengesResource::collection(auth()->user()->getAllMyChallenges());
    }
    
    
    public function joinChallenge(Request $request, $challenge)
    {
        $chal = Challenge::find($challenge);
        
        if (!$chal) {
            return response()->json([
                "message" => "Challenge with id: $challenge not found"
            ], 500);
        }
        
        if ($chal->type === "single" && $request->has("team")) {
            return response()->json([
                "message" => "Vous ne pouvez pas participer a un challenge individuel en équipe"
            ], 500);
        }
        
        if ($chal->type === "team" && $request->has("single")) {
            return response()->json([
                "message" => "vous ne pouvez pas participer a un challenge en équipe si vous êtes seul"
            ]);
        }
        // CE QUON VEUT CEST QUAND LA TEAM A REJOIND LE CHALLENGE SET LENTREY DE team_has_challenge AU STATE[0] DU CHALLENGE
        if ($request->has("team")) {
            $team = Team::find($request->team);
            $chal->joinChallenge(false, $team);
            $state = DB::table("team_has_challenge")->where('fk_challenge_id', $chal->id)->where('fk_team_id', $team->id)->first();
            $state->state = $chal->array_states[0];
        } else {
            $chal->joinChallenge(auth()->user(), false);
            $state = DB::table("challenges_has_users")->where("fk_challenge_id", $chal->id)->where("fk_user_id", auth()->user()->id)->first();
            $state->state = $chal->array_states[0];
        }
        
        
        $challenges = Challenge::disponibleChallenge(auth()->user())->with("getTeams", "getClans", "getOwner", "getUsers", 'getUsers.getAvatar', "getImages")->get();
        
        return ChallengesResource::collection($challenges);
    }
    
    
    public function show(Request $request, $id)
    {
        return new ChallengesResource(Challenge::with(Challenge::relation)->find($id));
    }
    
    public function updateStateChallenge(Request $request, $challenge)
    {
        $chal = Challenge::find($challenge);
        
        if ($request->has("team")) {
            $team = Team::find($request->team);
            DB::table("team_has_challenge")->where("fk_challenge_id", $chal->id)->where("fk_team_id", $team->id)
                ->update(["state" => $request->state]);;
        } else {
            $state = DB::table("challenges_has_users")->where("fk_challenge_id", $chal->id)->where("fk_user_id", auth()->user()->id)
                ->update(["state" => $request->state]);
            dd($state);
        }
        
    }
}
