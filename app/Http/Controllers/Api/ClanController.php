<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Clan\ClanResource;
use App\Models\Clan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClanController extends Controller
{
    public function index(Request $request)
    {
        return ClanResource::collection(Clan::all());
    }
}
