<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserEditResource;
use App\Models\Axe;
use App\Models\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{

    public function edit()
    {
        $user = Auth::user();
        $user->load('getClan', 'getItems', 'getItems.getFile', 'getAvatar');

        return new UserEditResource($user);
    }

    public function update(Request $request)
    {
        $request->validate([
            "first_name" => "string|required|nullable",
            "last_name"  => "string|required|nullable",
            "email"      => "string|required|nullable",
            "axe"        => "string|required|nullable",
            "promotion"  => "string|required|nullable",
            "file"       => "string|required|nullable"
        ]);

        $axe = Axe::find($request->get('axe'));
        $promotion = Promotion::find($request->get('promotion'));

        $user = Auth::user();
        if ($user) $user->update([
            'first_name'      => $request->has('first_name') ? $request->get('first_name') : $user->first_name,
            'last_name'       => $request->has('last_name') ? $request->get('last_name') : $user->last_name,
            'email'           => $request->has('email') ? $request->get('email') : $user->email,
            'description'     => $request->has('description') ? $request->get('description') : $user->description,
            'fk_avatar_id'    => $request->has('file') ? $request->get('file') : $user->file,
            'fk_axe_id'       => $axe ? $axe->id : $axe->fk_axe_id,
            'fk_promotion_id' => $promotion ? $promotion->id : $axe->fk_promotion_id,
        ]);

        $user->load('getItems', 'getAvatar', 'getClan');

        return response()->json([
            'status' => 'succes',
            'data'   => $user
        ]);
    }

}
