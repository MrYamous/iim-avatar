<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Clan\ClanResource;
use App\Http\Resources\LeaderboardResource;
use App\Http\Resources\RankingResource;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use App\Models\Clan;
use App\Models\TypeChallenge;
use App\Models\User\Role;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function getDashboard(Request $request)
    {
        return response()->json([
            "data" => [
                "user"   => new UserResource(auth()->user()->load("getClan", "getAvatar", "getItems")),
                "teams"  => TeamResource::collection(auth()->user()->getTeams->load('getCreator')),
                "houses" => ClanResource::collection(Clan::with("getImages", "getLogo")->get())
            ]
        ]);
    }
    
    public function getScore(Request $request, $slug)
    {
        $c = TypeChallenge::where('slug', $slug)->first();
        
        if (!$c) {
            return response()->json([
                "message" => "Le niveau n'existe pas"
            ], 500);
        }
        
        return response()->json([
            "status" => "success",
            "data"   => $c
        ]);
    }
    
    
    public function loginUser(Request $request)
    {
        if (!$request->password || !$request->email) {
            return response()->json(["status" => "error", "message" => "You should pass credentials"], 400);
        }
        $user = User::where('email', $request->email)->with("getClan", 'getAvatar')->first();
        if (!$user) {
            return response()->json(["status" => "error", "message" => "No user found in db"], 404);
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
            $argLogin = [
                "username" => $request->email,
                "password" => $request->password
            ];
            $client = DB::table('oauth_clients')->where('id', 1)->first();
            if (!$client->password_client) {
                DB::table('oauth_clients')->where('id', 1)->update([
                    "personal_access_client" => 0,
                    "password_client"        => 1
                ]);
            }
            //$role = join(" ", $user->roles->pluck('name')->toArray());
            
            //$scope = join(" ", $user->perms->toArray());
            //$scope = $scope . " " . $role;
            $data = array_merge($argLogin, [
                'grant_type'    => 'password',
                //'scope'         => $scope,
                'client_id'     => $client->id,
                'client_secret' => $client->secret,
            ]);
            $auth_token = json_decode(app()->handle(Request::create('oauth/token', 'POST', $data, [], [], ['HTTP_Accept' => 'application/json']))->getContent(), true);
            
            if (isset($auth_token["error"])) {
                return response()->json(["status" => "error", "message" => $auth_token["error"]], 500);
            }
            
            if (!$user) $user = User::where('id', (int) explodeAuthToken($auth_token)['sub'])->first();
            
            auth()->user()->updateLastLogin();
            
            return response()->json([
                    "status" => "success",
                    "data"   => [
                        "user"         => new UserResource($user),
                        "roles"        => Auth::user()->roles->pluck('name'),
                        "access_token" => $auth_token["access_token"]
                    ]
                ]
            );
        } else {
            return response()->json(["status" => "error", "message" => "Wrong credentials"], 401);
        }
    }
    
    
    public function saveAvatar(Request $request)
    {
        if ($request->has('file')) {
            $o = $this->uploadImage($request->file, str_random(23) . "-" . str_random(14));
            Auth::user()->fk_avatar_id = $o->id;
        }
        Auth::user()->save();
        $user = Auth::user();
        $user->load('getItems', "getAvatar", "getClan", "getChallenges");
        
        return response()->json([
            "status" => "success",
            "data"   => $user
        ]);
    }
    
    public function getUser()
    {
        $user = Auth::user();
        $user->load('getClan', 'getItems', 'getItems.getFile', 'getAvatar', 'getAxe', 'getPromotion');
        
        return new UserResource($user);
    }
    
    private function returnAccessToken(Request $request)
    {
        return Auth::user()->createToken("login")->accessToken;
    }
    
    public function registerUser(Request $request)
    {
        $request->validate([
            'last_name'  => 'required',
            'first_name' => 'required',
            'email'      => 'unique:users|required',
            'password'   => 'required',
        ]);
        
        $user = User::where('email', $request->get('email'))->first();
        if ($user) {
            return response()->json([
                "error" => "no"
            ], 500);
        }
        $user = new User();
        $user->last_name = $request->get('last_name');
        $user->first_name = $request->get('first_name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->save();
        $user->attachRole(Role::where('name', "student")->first());
        $user->save();
        
        return new UserResource(User::find($user->id));
    }
    
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        $user->load('getClan', 'getItems', 'getItems.getFile', 'getAvatar');
        
        return new UserResource($user);
    }
    
    public function search(Request $request)
    {
        if ($request->has('users')) {
            $search = $request->get('users');
            $users = User::orWhere('last_name', 'LIKE', "%" . $search . "%")
                ->orWhere('first_name', 'LIKE', "%" . $search . "%")
                ->orWhere('email', 'LIKE', "%" . $search . "%")->get();
            
            return UserResource::collection($users);
        }
    }
    
    public function clanRanking(Request $request)
    {
        return LeaderboardResource::collection(Clan::with('getImages', 'getLogo', "getUsers", "getUsers.getAvatar")->get()->sortBy('leaderboard'));
    }
    
    public function userRanking()
    {
        $user = User::all()->load('getClan', 'getAvatar')->sortByDesc(User::POINT);
        
        return RankingResource::collection($user);
    }
    
    
    //    public function logoutUser() {
    //
    //        Auth::logout();
    //
    //        return response()->json(['message' => 'Successfully logged out']);
    //
    //    }
}
