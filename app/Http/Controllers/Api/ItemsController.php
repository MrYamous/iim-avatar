<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Item\ItemResource;
use App\Http\Resources\UserResource;
use App\Models\Item;
use App\Models\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{
    public function index(Request $request)
    {
        return ItemResource::collection(Item::with('getFile')->get());
    }

    public function getAllItemsUser(Request $request, $id)
    {
        $user = User::with("getItems", 'getItems.getFile', 'getAvatar')->find($id);

        return ItemResource::collection($user->getItems);
    }

    public function search(Request $request)
    {
        if ($request->has('items')) {
            $search = $request->get('items');
            $items = Item::where('name', 'LIKE', "%" . $search . "%")->orWhere('slug', 'LIKE', "%" . $search . "%");

            return ItemResource::collection($items);
        }
    }

    public function buyItem(Request $request, $id)
    {
        $item = Item::where('id', $id)->first();


        if (!$item) {
            return response()->json([
                "status" => "error",
                "data"   => "Item non existant"
            ], 500);
        }
        if ($item->buy) {
            return response()->json([
                "message" => "Vous possédez déjà cet item"
            ], 500);
        }
        if (Auth::user()->gold < $item->cost) {
            return response()->json([
                "status" => "error",
                "data"   => "Vous n'avez pas assez d'argent pour acheter cette item, le prix pour acheter cette item est de : " . $item->cost . " gold"
            ], 500);
        }

        Auth::user()->gold = Auth::user()->gold - $item->cost;
        Auth::user()->save();
        $item->getUsers()->attach(Auth::user()->id);
        $item->load('getFile');
        $user = Auth::user();
        $user->load('getItems', "getAvatar", "getClan", "getChallenges");

        return new ItemResource($item);
    }

}
