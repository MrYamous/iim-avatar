<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Clan;
use App\Models\Quiz\Answer;
use App\Models\Quiz\Question;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller
{
    
    public function index()
    {
        $questions = Question::with('getAnswers')->paginate(3);
        
        return response()->json([
            'data' => [
                'pagination' => collect($questions)->forget('data'),
                'questions'  => $questions->getCollection()
            ]
        ]);
    }
    
    public function saveAnswers(Request $request)
    {
        $arrayOfIdAnswerIChoose = [];
        $arrayOfClanIChoose = [];
        $arrayOfClan = [];
        foreach ($request->all() as $index => $item) {
            /// id = id de la question
            /// answers => array contenant tout les id des reponse que j'ai selectionner
            foreach ($item["answers"] as $answer) {
                $a = Answer::with("getClan")->find($answer);
                if (!$a) {
                    return response()->json([
                        "message" => "No answer found for id : $answer"
                    ], 500);
                }
                if (!in_array($a->getClan->id, $arrayOfClanIChoose)) {
                    ///ICI SI LID DU CLAN NEST PAS DANS LE ARRAY OF CLAN I CHOSSE
                    $arrayOfClanIChoose[] = $a->getClan->id;
                    $arrayOfClan[ $a->getClan->id ] = $a->less;
                } else {
                    $arrayOfClan[ $a->getClan->id ] = $arrayOfClan[ $a->getClan->id ] + $a->less;
                }
                if (!$a) {
                    return response()->json([
                        "message" => "No answer found for id : $answer"
                    ], 500);
                }
                $arrayOfIdAnswerIChoose[] = $a->id;
            }
        }
        ///TODO RESSORTIR LA VALEUR LA PLUS HAUTE
        $idOfClan = array_keys($arrayOfClan, max($arrayOfClan));
        
        $c = Clan::find(collect($idOfClan)->first());
        if (auth()->user()->getAnswers->isNotEmpty()) {
            auth()->user()->getAnswers()->sync($arrayOfIdAnswerIChoose);
        } else {
            auth()->user()->getAnswers()->attach($arrayOfIdAnswerIChoose);
        }
        auth()->user()->fk_clan_id = $c->id;
        auth()->user()->save();
        
        return response()->json([
            'message' => 'Vous faites partie du clan ' . $c->title,
            'data'    => $request->all()
        ], 200);
    }
    
}
