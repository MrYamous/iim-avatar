<?php

namespace App\Http\Controllers\Api\Teams;

use App\Events\sendInvitationToTeam;
use App\Http\Controllers\Controller;
use App\Http\Resources\Team\TeamResource;
use App\Models\Chat\Conversation;
use App\Models\Team;
use App\Models\User\User;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return TeamResource::collection(auth()->user()->getTeams->load(Team::relation));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $slug
     * @return TeamResource
     */
    public function show(Request $request, $slug)
    {
        $team = Team::where('slug', $slug)->first();

        if (!$team) return response()->json(["message" => "Team with slug '$slug' not found"]);
        if (!$team->isInvited(auth()->user()) && !collect($team->getConfirmMembers)->keyBy('id')->has(auth()->user()->id)) return response()->json(["message" => "You have not be invited ton join this team."]);

        return new TeamResource($team->load(Team::relation));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return TeamResource
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"    => 'string|required',
            "members" => "array|required"
        ]);

        if (Team::where('slug', str_slug($request->name))->first()) return response()->json([
            "message" => "A team with name : '" . $request->name . "' already exist"
        ]);

        $team = Team::create([
            'name'          => $request->get('name'),
            'slug'          => str_slug($request->get('name')),
            'fk_creator_id' => auth()->user()->id,
        ]);

        $memberInvited = [];
        foreach ($request->get('members') as $member) {
            $user = User::find($member);
            $memberInvited[] = $user->id;
            event(new sendInvitationToTeam($team, $user));
        }

        $team->getInvitedMembers()->attach($memberInvited);
        $team->getConfirmMembers()->attach(auth()->user()->id);

        $conversation = Conversation::create(['name' => $team->name]);
        $team->update(['fk_conversation_id' => $conversation->id]);
        $conversation->getMembers()->attach(auth()->user());

        return new TeamResource($team);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return TeamResource|\Illuminate\Http\JsonResponse
     */
    public function edit($slug)
    {
        $team = Team::where('slug', $slug)->with(Team::relation)->first();

        if (!$team) return response()->json(["message" => "Team with id : $id not found"], 400);
        if ($team->getCreator->id !== auth()->user()->id) return response()->json(["message" => "You are not the creator !"], 403);

        return new TeamResource($team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $slug
     * @return TeamResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $slug)
    {
        $request->validate([
            "name"    => 'string|required',
            "members" => "array|required"
        ]);

        $team = Team::where('slug', $slug)->first();
        if (!$team) return response()->json(["message" => "Aucune équipe n'a été trouvé !"]);

        $team->update([
            'name' => $request->has('name') ? $request->get('name') : $team->name,
            'slug' => $request->has('name') ? str_slug($request->get('name')) : $team->slug,
        ]);

        $memberInvited = [];
        foreach ($request->get('members') as $member) {
            $user = User::find($member);
            if ($user) {
                if (!$team->getConfirmMembers->contains($user->id)) {
                    $memberInvited[] = $user->id;
                }
                if (!$team->getInvitedMembers->contains($user->id)) {
                    event(new sendInvitationToTeam($team, $user));
                }
            }
        }
        $memberConfirmed = [];
        foreach ($team->getConfirmMembers as $member) {
            if (!collect($request->get('members'))->contains($member->id)) {
                $memberConfirmed[] = $member->id;
            }
        }

        $team->getInvitedMembers()->sync($memberInvited);
        $team->getConfirmMembers()->sync($memberConfirmed);
        $team->getConfirmMembers()->attach(auth()->user()->id);

        $team->getConversation()->update(['name' => $team->name]);
        $team->getConversation->getMembers()->sync($team->getConfirmMembers);

        return new TeamResource($team);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return TeamResource|\Illuminate\Http\JsonResponse
     */
    public function destroy($slug)
    {
        $team = Team::where('slug', $slug)->first();

        if (!$team) return response()->json(["message" => "Aucune équipe n'a été trouvé !"]);

        $team->getInvitedMembers()->sync([]);
        $team->getConfirmMembers()->sync([]);
        $team->getConversation->getMessage()->delete();
        $team->getConversation()->delete();
        $team->delete();

        return new TeamResource($team);
    }

    public function getFollow(Request $request)
    {
        return TeamResource::collection(auth()->user()->getFollowTeams);
    }

    public function getTeamWhereImCreator(Request $request)
    {
        return TeamResource::collection(auth()->user()->getTeamWhereImCreator());

    }

    public function setFollow(Request $request, $team)
    {

        $t = Team::find($team);
        if (!$t) {
            return response()->json([
                'message' => 'no team for id $team'
            ]);
        }
        if (auth()->user()->getFollowTeams->contains($t)) {
            return response()->json([
                "error" => "Already here"
            ], 500);
        }


        auth()->user()->getFollowTeams()->attach($t->id);

        return TeamResource::collection(auth()->user()->getFollowTeams);
    }

    public function joinTeam(Request $request, $team)
    {
        $team = Team::find($team);

        if (!$team) return response()->json(["message" => "Team with id : $team not found"], 500);
        if (!$team->isInvited(auth()->user())) return response()->json(["message" => "You have not be invited ton join this team."], 500);

        $accept = $team->joinTeam(auth()->user());

        if (!$accept) return response()->json(["message" => "You already in this team."], 500);
        if ($team->getConversation) $team->getConversation->getMembers()->attach(auth()->user());

        return TeamResource::collection(auth()->user()->filterTeam()->load(Team::relation))
            ->additional(["count" => auth()->user()->filterTeam()->count()]);
    }

    public function leaveTeam(Request $request, $team)
    {
        $team = Team::find($team);

        if (!$team) return response()->json(["message" => "Team with id : $team not found"]);

        $team->getConfirmMembers()->detach(auth()->user()->id);
        $team->getInvitedMembers()->detach(auth()->user()->id);

        return new TeamResource($team->load(Team::relation));
    }

    public function search(Request $request)
    {
        $request->validate([
            "teams" => "array|required"
        ]);

        $search = $request->get('teams');
        $teams = Team::where('name', 'LIKE', "%" . $search . "%")->orWhere('slug', 'LIKE', "%" . $search . "%")->get();

        return TeamResource::collection($teams);
    }

    public function listInvitedTeam(Request $request)
    {
        return TeamResource::collection(auth()->user()->filterTeam()->load(Team::relation))
            ->additional(["count" => auth()->user()->filterTeam()->count()]);
    }

}
