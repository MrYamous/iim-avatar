<?php

namespace App\Http\Controllers\App;

use App\Models\Clan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaderBoardController extends Controller
{
    const PATH_VIEW = "app.entities.leaderboard.";
    
    public function showLeaderBoard(Request $request)
    {
        $clans = Clan::with("getUsers", "getLogo", "getImages")->get();
        $c = collect();
        foreach ($clans as $clan) {
            $ar = $clan->getAttributes();
            $ar["logo"] = $clan->getLogo;
            $ar["images"] = $clan->getImages;
            $ar["users"] = $clan->getUsers;
            $ar["score"] = $clan->score;
            $c->push($ar);
        }
        $clans = $c;
        $clans = $clans->sortByDesc("score");
        $o = [];
        $i = 0;
        foreach ($clans as $clan) {
            $o[$i] = $clan;
            $i++;
        }
        $c = collect();
        $c->push($o[4]);
        $c->push($o[1]);
        $c->push($o[0]);
        $c->push($o[2]);
        $c->push($o[3]);
        
        return view(self::PATH_VIEW . 'leaderboard')->with([
            "leaderboards" => $c
        ]);
    }
}
