<?php

namespace App\Http\Controllers\App;

use App\Models\Challenge;
use App\Models\Render;
use App\Models\UsersLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChallengesController extends Controller
{
    const PATH_VIEW = "app.entities.challenges.";
    
    public function showChallenge(Request $request)
    {
        return view(self::PATH_VIEW . 'challenges')->with([
            "challenges" => Challenge::orderBy('id', 'DESC')->paginate(8)
        ]);
    }
    
    
    public function submitChallenge(Request $request, $slug)
    {
        $ch = Challenge::where('slug', $slug)->first();
        if (!$ch) {
            return redirect()->back()->with("error", "Challenge non existant");
        }
        if ($ch->nb_max_student !== null) {
            if ($ch->nb_max_student === $ch->getUsers->count()) {
                return redirect()->back()->with("error", "Le nombre maximum de joueurs a été atteint sur ce challenge");
            }
        }
        foreach ($ch->getUsers as $getUser) {
            if ($getUser->id === Auth::user()->id){
                return redirect()->back()->with("error", "Vous particpez déjà à ce challenge");
            }
        }
        //TODO Make a matchmaking for team challenge
        
        $ch->getUsers()->attach([Auth::user()->id]);
        
        return redirect()->back()->with("succes", "Votre inscription au challenge a bien été prise en compte")->with("challenge", $ch->id);
    }
    
    
    public function renderChallenger(Request $request, $slug)
    {
        $ch = Challenge::where('slug', $slug)->first();
        if (!$ch) {
            return redirect()->back()->with("error", "Ce challenge n'existe pas");
        }
        $pointForContrib = $ch->point_contribution;
        Auth::user()->points = Auth::user()->points + $pointForContrib;
        Auth::user()->gold = Auth::user()->gold + $pointForContrib;
        Auth::user()->save();
        $points = Auth::user()->points;
        $level = UsersLevel::where("start", "<=", $points)->where("end", ">=", $points)->first();
        if ($level) {
            Auth::user()->level = $level->level;
        }
        
        Auth::user()->save();
        //        $users = UsersLevel::where("start", "=<", $points)->where("end", ">=", $points)->get();
        
        $ch->getRenderedUser()->attach([Auth::user()->id]);
        
        $re = new Render();
        $name = false;
        $id = false;
        $ext = $request->file("file")->getClientOriginalExtension();
        $img = null;
        if ($request->has('name')) {
            $name = $request->name;
        }
        if (Auth::check()) {
            $id = Auth::user()->id;
        }
        
        if ($ext === "png" || $ext === "jpg" || $ext === "jpeg" || $ext === "webp" || $ext === "gif") {
            $file = $this->uploadImage($request->file, $name);
        } else {
            $file = $this->saveFile($request, $request->file("file"), $ext, $name, $id);
        }
        if (is_array($file) && $file[0] === "error") {
            return redirect()->back()->with("$file[0]", "$file[1]");
        }
        
        //$file = $this->uploadImage($request->file, $request->name);
        
        $re->fk_file_id = $file->id;
        $re->name = $request->name;
        $re->slug = str_slug($request->name);
        $re->fk_user_id = Auth::user()->id;
        $re->save();
        
        
        $ch->getRender()->attach($re->id);
        
        //TODO ADD POINT TO USER BECAUSE HE RENDER SOMETHING
        
        return redirect()->back()->with("succes", "Votre rendu à ce challenge a bien été pris en compte")->with("challenge", $ch->id);;
        
    }
}
