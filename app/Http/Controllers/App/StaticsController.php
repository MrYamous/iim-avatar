<?php

namespace App\Http\Controllers\App;

use App\Mail\InscriptionMail;
use App\Models\Axe;
use App\Models\Clan;
use App\Models\File;
use App\Models\Post;
use App\Models\Promotion;
use App\Models\User\Role;
use App\Models\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class StaticsController extends Controller
{
    
    const PATH_VIEW = "app.statics.";
    
    public function redirect(Request $request)
    {
        if ($request->url() === "http://admin.nemesia.glrd.me") {
            return redirect()->to("/admin/login");
        } elseif ($request->url() === "http://api.nemesia.glrd.me") {
            return redirect()->to("/api");
        } else {
            return redirect()->to("/admin/login");
        }
    }
    
    public function home(Request $request)
    {
        
        return view(self::PATH_VIEW . "home")->with([
            "clans" => Clan::all(),
            "posts" => Post::orderBy("id", "DESC")->where("publish", true)->get()
        ]);
    }
    
    public function knowEntrie(Request $request)
    {
        $request->validate([
            "email" => "required|string"
        ]);
        
        $s = User::where('email', $request->email)->first();
        if (!$s) {
            return redirect("/questions");
        } else {
            
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                return redirect("/");
            }
            
            return redirect("/");
        }
    }
    
    public function searchFile(Request $request, $name)
    {
        $file = File::where('name', $name)->first();
        dd($file->getUser);
        
        return view(self::PATH_VIEW . "home");
    }
    
    public function showQuestion(Request $request)
    {
        
        return view(self::PATH_VIEW . "quiz");
    }
    
    public function submitQuestion(Request $request)
    {
        $request->validate([
            "array" => "required|array",
        ]);
        if (!Auth::check()) {
            return redirect()->back()->with("error", "Vous n'êtes pas connecté !");
        }
        $loki = 0;
        $ella = 0;
        $Thor = 0;
        $Skadi = 0;
        $Freya = 0;
        foreach ($request->array as $item) {
            if (json_decode($item)) {
                $arrayReponse = json_decode($item);
                foreach ($arrayReponse as $res) {
                    switch ($res) {
                        case $res === "Loki":
                            $loki = $loki + 1;
                            break;
                        
                        case $res === "Ella":
                            $ella = $ella + 1;
                            break;
                        
                        case $res === "Skadi":
                            $Skadi = $Skadi + 1;
                            break;
                        
                        case $res === "Freya":
                            $Freya = $Freya + 1;
                            break;
                        
                        case $res === "Thor":
                            $Thor = $Thor + 1;
                            break;
                        
                    }
                    
                }
            }
        }
        $arr = ["loki" => $loki, "ella" => $ella, "thor" => $Thor, "skadi" => $Skadi, "freya" => $Freya];
        $max = max($arr);
        
        $slug = array_search($max, $arr);
        $clan = Clan::where('slug', $slug)->first();
        
        //Create new user and attach clan
        $user = Auth::user();
        $user->fk_clan_id = $clan->id;
        $user->save();
        
        //try {
        //    Mail::to($user->email)->send(new InscriptionMail($request, $user));
        //} catch (\Exception $e) {
        //    return redirect()->back()->with("error", "Error : $e");
        //}
        
        return redirect("/success/" . $clan->title)->with("success", "Vous avez rejoint la famille " . $clan->title);
    }
    
    public function successForClan(Request $request, $slug)
    {
        $clan = Clan::where('slug', $slug)->first();
        
        if (!$clan) {
            return redirect()->back()->with("error", "Famille non connue");
        }
        
        return view(self::PATH_VIEW . "success")->with([
            "clan" => $clan
        ]);
    }
    
    public function showLoginModal(Request $request)
    {
        return view(self::PATH_VIEW . "login")->with([
            "axes"       => Axe::all(),
            "promotions" => Promotion::all()
        ]);
    }
    
    public function setPassword(Request $request, $token)
    {
        $user = User::where('remember_token', $token)->first();
        
        
        return view('app.entities.auth.passwords.reset')->with([
            "token" => $user->remember_token
        ]);
        
    }
    
    public function updatePassword(Request $request)
    {
        $user = User::where('remember_token', $request->token)->first();
        if (!$user) {
            return redirect("/")->with("error", "Utilisateur non connu");
        }
        
        if ($user->email !== $request->email) {
            return redirect("/")->with("error", "Les mails ne correspondent pas");
        }
        
        if ($request->password_confirmation !== $request->password) {
            return redirect("/")->with("error", "Les mots de passe ne correspondent pas");
        }
        
        $user->password = bcrypt($request->password);
        $user->save();
        
        return redirect("/")->with("success", "Mot de passe redéfini avec succès !");
    }
    
    public function showRegister()
    {
        return view("app.entities.auth.register")->with([
            "axes"       => Axe::all(),
            "promotions" => Promotion::all()
        ]);
    }
    
    public function showLogin()
    {
        return view("app.entities.auth.login");
    }
    
    public function showPost(Request $request, $slug)
    {
        $post = Post::where('slug', $slug)->first();
        if (!$post) {
            return redirect("/")->with("error", "Cet article n'existe pas");
        }
        
        return view(self::PATH_VIEW . "post")->with([
            "post" => $post
        ]);
    }
}
