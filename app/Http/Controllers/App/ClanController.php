<?php

namespace App\Http\Controllers\App;

use App\Models\Clan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClanController extends Controller
{
    const PATH_VIEW = "app.entities.clans.";

    public function showHouses()
    {
        return view(self::PATH_VIEW . "clans")->with([
            "clans" => Clan::orderBy('id', 'DESC')->get(),
        ]);
    }

    public function showJsonHouse($slug)
    {
        $clan = Clan::where('slug', $slug)->first();

        if (!$clan) {
            return response()->json([
                'status' => 'fail',
                'data' => null
            ]);
        }

        return response()->json([
            'status' => 'ok',
            'data' => [
                'id' => $clan->id,
                'slug' => $clan->slug,
                'title' => $clan->title,
                'levels' => $clan->level,
                'description' => $clan->description
            ]
        ]);
    }

    public function showUserClan(Request $request, $slug)
    {
        $clan = Clan::where('slug', $slug)->first();
        if (!$clan) {
            return redirect()->back()->with("error", "Cette famille n'existe pas");
        }
        if (Auth::user()->getClan) {
            if (Auth::user()->getClan->id !== $clan->id) {
                return redirect()->back()->with("error", "Vous n'êtes pas membre de cette famille");
            }
        }else{
            return redirect()->back()->with("error", "Vous n'êtes pas membre d'une famille");
        }

        return view(self::PATH_VIEW . "clan")->with([
            "user" => Auth::user(),
            "userClan" => Auth::user()->getClan,
        ]);
    }
}
