<?php

namespace App\Http\Controllers\App;

use App\Models\Axe;
use App\Models\Clan;
use App\Models\Item;
use App\Models\Promotion;
use App\Models\Render;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    const PATH_VIEW = "app.entities.account.";

    public function showAccount(Request $request)
    {
        return view(self::PATH_VIEW . 'account')->with([
            "user" => Auth::user(),
            "items" => Item::orderBy('id', 'DESC')->get()
        ]);
    }

    public function buyItem(Request $request, $id)
    {
        $item = Item::where('id', $id)->first();
        if (!$item) {
            return redirect()->back()->with("error", "Cet item n'existe pas");
        }
        if (Auth::user()->gold < $item->cost) {
            return redirect()->back()->with("error", "Vous n'avez pas assez d'argent pour acheter cet item");
        }
        Auth::user()->gold = Auth::user()->gold - $item->cost;
        Auth::user()->save();
        $item->getUsers()->attach(Auth::user()->id);
        return redirect()->back()->with("success", "Item acheté !")->with("vitrine", true);

    }

    public function submitToClan(Request $request, $id)
    {
        $clan = Clan::where('id', $id)->first();
        if (!$clan) {
            return redirect()->back()->with("error", "Cette famille n'existe pas");
        }

        Auth::user()->fk_clan_id = $clan->id;
        Auth::user()->save();

        return redirect()->back()->with("success", "Vous êtes maintenant membre de la famille $clan->title !");

    }

    public function showEdit(Request $request)
    {
        return view(self::PATH_VIEW . "edit")->with([
            "axes" => Axe::all(),
            "promotions" => Promotion::all()
        ]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        if ($request->has("avatar") && $request->hasFile("avatar")) {
            //Store new avatar of user
            $avatar = $this->uploadImage($request->file("avatar"), "avatar-" . str_slug($request->first_name . $request->last_name));
            $user->fk_avatar_id = $avatar->id;
        }
        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if ($request->has("first_name")) {
            $user->first_name = $request->first_name;
        }

        if ($request->has("last_name")) {
            $user->last_name = $request->last_name;
        }
        if ($request->has("email")) {
            $user->email = $request->email;
        }

        if ($request->has("axe")) {
            $user->fk_axe_id = $request->axe;
        }
        if ($request->has("promotion")) {
            $user->fk_promotion_id = $request->promotion;
        }

        $user->save();

        return redirect("/account")->with("success", "Vos modifications ont bien été prises en compte");
    }


}