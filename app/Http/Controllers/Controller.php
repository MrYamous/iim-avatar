<?php

namespace App\Http\Controllers;

use App\Traits\FileTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, FileTrait;
    
    public function sendPostCurl($url, $data, $header = [])
    {
        
        $ch = curl_init($url);
        //$data = json_encode($data);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        // execute!
        $response = curl_exec($ch);
        
        // close the connection, release resources used
        curl_close($ch);
        
        // do anything you want with your response
        return $response;
    }
    
    public function sendGetCUrl($url, $bearer = null)
    {
        
        $curl = curl_init();
        if (!$bearer) {
            
            curl_setopt_array(
                $curl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL            => $url,
                ]
            );
        } else {
            $authorization = "Authorization: Bearer " . $bearer;
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, "true");
            curl_setopt($curl, CURLOPT_URL, $url);
        }
        $response = curl_exec($curl);
        curl_close($curl);
        
        return $response;
    }
    
}
