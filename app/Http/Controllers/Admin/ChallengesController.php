<?php

namespace App\Http\Controllers\Admin;

use App\Models\Challenge;
use App\Models\Clan;
use App\Models\Constrainte;
use App\Models\File;
use App\Models\Ssk;
use App\Models\SskScore;
use App\Models\Team;
use App\Models\TypeChallenge;
use App\Models\User\User;
use App\Models\UsersLevel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use function Symfony\Component\Console\Tests\Command\createClosure;

class ChallengesController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.challenges.";

    public function getChallengesValides(Request $request)
    {
        $challenges_validated = Challenge::where("validated", 1)->get();

        return view(self::PATH_VIEW . "index_challenges_valides")->with([
            "challenges_validated" => $challenges_validated,
        ]);
    }

    public function getChallengesNonValides(Request $request)
    {
        $challenges_no_validated = Challenge::where("validated", 0)->get();

        return view(self::PATH_VIEW . "index_challenges_non_valides")->with([
            "challenges_no_validated" => $challenges_no_validated,
        ]);
    }

    public function create(Request $request)
    {
        $levels = TypeChallenge::orderBy('id', 'DESC')->get();
        $contraints = Constrainte::orderBy('id', 'DESC')->get();
        $ssks = Ssk::orderBy('id', 'DESC')->get();
        $clans = Clan::orderBy('id', 'DESC')->get();

        return view(self::PATH_VIEW . "create")->with([
            "levels" => $levels,
            "contraints" => $contraints,
            "ssks" => $ssks,
            "clans" => $clans
        ]);
    }

    public function show(Request $request, $slug)
    {
        $c = Challenge::where('slug', $slug)->with('getClans', 'getImages', "getConstraintes", "getSsks", "getOwner", "getLevel", "getSsks.getScore", 'getUsers', 'getTeams')->first();
        if (!$c) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }


        return view(self::PATH_VIEW . "show")->with([
            "challenges" => $c
        ]);


    }

    public function showChallengesNotValidated(Request $request, $slug)
    {
        $challenges = Challenge::where("slug", $slug)->first();

        return view(self::PATH_VIEW . "showChallengesNotValidated")->with([
            "challenges" => $challenges,
        ]);

    }


    public function showUpdate(Request $request, $slug)
    {
        $challenges = Challenge::where("slug", $slug)->first();
        $challenges->validated = 1;
        $challenges->save();

        return redirect()->to("admin/challenges")->with(["success" => "Le challenge a été validé"]);
    }


    public function edit(Request $request, $slug)
    {
        $c = Challenge::where('slug', $slug)->with('getClans', 'getImages', "getConstraintes", "getSsks", "getOwner", "getLevel", "getSsks.getScore")->first();
        $levels = TypeChallenge::orderBy('id', 'DESC')->get();
        $contraints = Constrainte::orderBy('id', 'DESC')->get();
        $ssks = Ssk::orderBy('id', 'DESC')->get();
        $clans = Clan::orderBy('id', 'DESC')->get();
        if (!$c) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }

        return view(self::PATH_VIEW . "edit")->with([
            "challenge" => $c,
            "levels" => $levels,
            "contraints" => $contraints,
            "ssks" => $ssks,
            "clans" => $clans
        ]);


    }


    public function update(Request $request, $slug)
    {
        $request->validate([
            "title" => "required|string",
            "description" => "required|string",
            "reglementation" => "required|string",
            "objectifs" => "required|string",
            "rendu" => "required|string",
            "max_user" => "required"
        ]);

        $chal = Challenge::where("slug", $slug)->first();
        if (!$chal) {
            return redirect()->back()->with("error", "Ce challenge n'existe pas");
        }
        if ($request->title !== $chal->title) {
            $chal->title = $request->title;
            $chal->slug = str_slug($request->title);
        }
        $chal->description = $request->description;
        $start_date = $request->begin_date;
        $end_date = $request->end_date;
        $chal->start_date = $start_date;
        $chal->end_date = $end_date;
        $chal->reglementation = $request->reglementation;
        $chal->objectifs = $request->objectifs;
        $chal->rendu = $request->rendu;
        $chal->end_date = $end_date;
        $chal->states = json_encode(explode(",", $request->get('states')));
        if ($request->has('point_win') && $request->point_win) {
            $chal->point_win = $request->point_win;
        }
        if ($request->has('point_contribution') && $request->point_contribution) {
            $chal->point_contribution = $request->point_contribution;
        }
        if ($request->has('max_user_by_team') && $request->max_user_by_team) {
            $chal->nb_max_student_by_team = $request->max_user_by_team;
        }
        if ($request->has('max_user') && is_array($request->max_user)) {
            foreach ($request->max_user as $max) {
                if ($max !== null) {
                    $chal->nb_max_student = $max;
                }
            }
        }
        $chal->fk_owner_id = Auth::user()->id;
        $chal->save();

        if ($request->has('level')) {
            $l = TypeChallenge::where('slug', $request->level)->first();
            if (!$l) {
                return redirect()->back()->with("error", "error level" . $request->level);
            }
            $chal->fk_type_id = $l->id;
        }

        if ($request->has('ssks')) {
            $arrayIdSsk = [];
            foreach ($request->ssks as $i => $item) {
                $k = Ssk::where('slug', $item['name'])->first();

                $l = new SskScore();
                $l->fk_ssk_id = $k->id;
                $l->fk_challenge_id = $chal->id;
                $l->fk_user_id = Auth::user()->id;
                $l->score = $item['score'];
                $l->save();
                if (!$k) {
                    return redirect()->back()->with("error", "error ssk " . $item['name']);
                }
                $arrayIdSsk[$i] = $k->id;
            }
            foreach ($arrayIdSsk as $id) {
                $chal->getSsks()->sync($id);
            }
        }

        if ($request->has('clans')) {
            $arrayIdClan = [];
            foreach ($request->clans as $i => $slug) {
                $k = Clan::where('slug', $slug)->first();
                if (!$k) {
                    return redirect()->back()->with("error", "error clans" . $slug);
                }
                $arrayIdClan[$i] = $k->id;
            }
            foreach ($arrayIdClan as $id) {
                $chal->getClans()->sync($id);
            }
        }

        if ($request->has('contraintes')) {
            $arrayIdConstraint = [];
            foreach ($request->contraintes as $i => $slug) {
                $k = Constrainte::where('slug', $slug)->first();
                if (!$k) {
                    return redirect()->back()->with("error", "error contrainte " . $slug);
                }
                $arrayIdConstraint[$i] = $k->id;
            }
            foreach ($arrayIdConstraint as $id) {
                $chal->getConstraintes()->sync($id);
            }
        }

        if ($request->has('files')) {
            $arrayIdFile = [];
            foreach ($request->file("files") as $i => $file) {
                $o = $this->uploadImage($file, str_slug($request->title) . "-" . str_random(3));
                $chal->getImages()->sync($o->id);
            }
        }
        $chal->save();

        return $chal->validated === 1 ? (redirect("admin/challenges")->with("success", "Challenge modifié avec succes !")) : (redirect("admin/challenges/unvalidated")->with("success", "Challenge modifié avec succes !"));

    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required|string",
            "description" => "required|string",
            "max_user" => "required",
            "states" => "string|required",
            "rendu" => "string|required",
            "reglementation" => "required|string",
            "objectifs" => "required|string",
        ]);
        $chal = new Challenge();
        $chal->title = $request->title;
        $chal->slug = str_slug($request->title);
        $chal->description = $request->description;
        $chal->reglementation = $request->reglementation;
        $chal->objectifs = $request->objectifs;
        $chal->rendu = $request->rendu;
        $chal->states = json_encode(explode(",", $request->get('states')));
        $chal->validated = 1;
        if ($request->has('team_choice')) {
            $chal->type = $request->get('team_choice') ? Challenge::TYPE_TEAM : Challenge::TYPE_SINGLE;
        }
        $start_date = Carbon::createFromFormat('d/m/Y', $request->begin_date);
        $end_date = Carbon::createFromFormat('d/m/Y', $request->end_date);
        $chal->start_date = $start_date;
        $chal->end_date = $end_date;
        if ($request->has('point_win') && $request->point_win) {
            $chal->point_win = $request->point_win;
        }
        if ($request->has('point_contribution') && $request->point_contribution) {
            $chal->point_contribution = $request->point_contribution;
        }
        if ($request->has('max_user_by_team') && $request->max_user_by_team) {
            $chal->nb_max_student_by_team = $request->max_user_by_team;
        }
        if ($request->has('max_user') && is_array($request->max_user)) {
            foreach ($request->max_user as $max) {
                if ($max !== null) {
                    $chal->nb_max_student = $max;
                }
            }
        }
        $chal->fk_owner_id = Auth::user()->id;
        $chal->save();

        if ($request->has('level')) {
            $l = TypeChallenge::where('slug', $request->level)->first();
            if (!$l) {
                return redirect()->back()->with("error", "error level" . $request->level);
            }
            $chal->fk_type_id = $l->id;
        }

        if ($request->has('ssks')) {
            $arrayIdSsk = [];
            foreach ($request->ssks as $i => $item) {
                $k = Ssk::where('slug', $item['name'])->first();

                $l = new SskScore();
                $l->fk_ssk_id = $k->id;
                $l->fk_challenge_id = $chal->id;
                $l->fk_user_id = Auth::user()->id;
                $l->score = $item['score'];
                $l->save();
                if (!$k) {
                    return redirect()->back()->with("error", "error ssk " . $item['name']);
                }
                $arrayIdSsk[$i] = $k->id;
            }
            foreach ($arrayIdSsk as $id) {
                $chal->getSsks()->attach($id);
            }
        }

        if ($request->has('clans')) {
            $arrayIdClan = [];
            foreach ($request->clans as $i => $slug) {
                $k = Clan::where('slug', $slug)->first();
                if (!$k) {
                    return redirect()->back()->with("error", "error clans" . $slug);
                }
                $arrayIdClan[$i] = $k->id;
            }
            foreach ($arrayIdClan as $id) {
                $chal->getClans()->attach($id);
            }
        }

        if ($request->has('contraintes')) {
            $arrayIdConstraint = [];
            foreach ($request->contraintes as $i => $slug) {
                $k = Constrainte::where('slug', $slug)->first();
                if (!$k) {
                    return redirect()->back()->with("error", "error contrainte " . $slug);
                }
                $arrayIdConstraint[$i] = $k->id;
            }
            foreach ($arrayIdConstraint as $id) {
                $chal->getConstraintes()->attach($id);
            }
        }

        if ($request->has('files')) {
            $arrayIdFile = [];
            foreach ($request->file("files") as $i => $file) {
                $o = $this->uploadImage($file, str_slug($request->title) . "-" . str_random(3));
                $chal->getImages()->attach($o->id);
            }
        }
        $chal->save();

        return redirect("admin/challenges")->with("success", "Challenge crée avec succes !");
    }


    public function delete(Request $request, $id)
    {

        $c = Challenge::where('id', $id)->first();

        if (!$c) {
            return redirect()->back()->with("error", "Aucun challenge trouvé");
        }

        $c->delete();

        return redirect()->back()->with("success", "Supprimé");
    }

    public function setWinner(Request $request, $slug, $id)
    {
        //Set winner to this tcahllenge

        $chal = Challenge::where('slug', $slug)->first();

        if (!$chal){
            return redirect()->back()->with([
                "error" => "Aucun challenge trouve"
            ]);
        }


        if ($chal->type === "single") {
            $user = User::find($id);
            if ($chal->getWinners->contains($user)) {
                return redirect()->back()->with([
                    "error" => "already"
                ]);
            }
            $chal->getWinners()->attach([$user->id]);
            $user->points = $user->points + $chal->point_win;
            $user->gold = $user->gold + $chal->point_win;
            $user_level = UsersLevel::where("start", "<=", $user->points)->where("end", ">=", $user->points)->first();

            if ($user_level) {
                $user->levels = $user_level->level;
            }
            $user->save();

            return redirect()->back()->with([
                "success" => "utilisateur set en tant que winner"
            ]);

        }


        if ($chal->type === "team") {
            $team = Team::with("getConfirmMembers")->find($id);

            if ($chal->getWinners->contains($team)) {
                return redirect()->back()->with([
                    "error" => "already"
                ]);
            }

            $chal->getWinners()->attach($team->id);
            foreach ($team->getConfirmMembers as $u) {
                $u->points = $u->points + $chal->point_win;
                $u->gold = $u->gold + $chal->point_win;
                $user_level = UsersLevel::where("start", "<=", $u->points)->where("end", ">=", $u->points)->first();

                if ($user_level) {
                    $u->levels = $user_level->level;
                }
                $u->save();
            }
            return redirect()->back()->with([
                "success" => "Team set comme winner"
            ]);
        }

    }


}
