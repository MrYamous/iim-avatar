<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CoursesController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.courses.";

    /**
     * Display a listing of the resource. Cour
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Cour::orderBy('id', 'DESC')->get();

        return view(self::PATH_VIEW . "index", ['courses' => $courses]);

    }

    public function show($slug){
        $cour = Cour::where('slug',$slug)->first();

        dd($cour);
    }

    public function destroy($id){
        Cour::find($id)->delete();

        return Redirect::back();
    }
}
