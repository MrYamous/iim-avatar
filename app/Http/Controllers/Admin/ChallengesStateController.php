<?php

namespace App\Http\Controllers\Admin;

use App\Models\ChallengeState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChallengesStateController extends Controller
{

    const PATH_VIEW = "admin.entities.cms.challenges_state.";

    public function index()
    {
        $c = ChallengeState::all();
        return view(self::PATH_VIEW . "index")->with([
            "c" => $c
        ]);
    }

    public function create(Request $request)
    {
        return view(self::PATH_VIEW . "create");

    }

    public function store(Request $request)
    {
        $request->validate([
            "name"
        ]);

        $c = new ChallengeState();
        $c->name = $request->name;
        $c->save();

        return redirect('/admin/challenges-state')->with("success", "ajouté");
    }

    public function show($id)
    {
        $c = ChallengeState::find($id);
        return view(self::PATH_VIEW . "show")->with([
            "id" => $id,
            "c" => $c
        ]);
    }

    public function edit(Request $request, $id)
    {
        $c = ChallengeState::find($id);
        return view(self::PATH_VIEW . "edit")->with([
            "c" => $c
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "name"
        ]);

        $c = ChallengeState::find($id);
        $c->name = $request->name;
        $c->save();

        return redirect("/admin/challenges-state")->with("success", "Modifié");

    }

    public function delete($id)
    {
        $c = ChallengeState::find($id);

        if (!$c){
            return redirect()->back()-with("error", "Aucun state trouvé");
        }

        $c->delete();

        return redirect()->back()->with("success", "Supprimé");

    }
}
