<?php

namespace App\Http\Controllers\Admin;

use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class NotesController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.notes.";

    public function index(){
        $notes = Note::all();

        return View(self::PATH_VIEW.'index',['notes' => $notes]);
    }

    public function destroy($id){
        $user = Note::find($id);

        if ($user !== null){
            $user->delete();
        }

        return Redirect::back();
    }
}
