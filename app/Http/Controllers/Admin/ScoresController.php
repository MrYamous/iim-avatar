<?php

namespace App\Http\Controllers\Admin;

use App\Models\Score;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ScoresController extends Controller
{

    const PATH_VIEW = "admin.entities.cms.scores.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scores = Score::all();

        return view(self::PATH_VIEW . "index", ['scores' => $scores] );
    }
    public function destroy($id)
    {
        Score::find($id)->delete();

        return Redirect::back();
    }
}
