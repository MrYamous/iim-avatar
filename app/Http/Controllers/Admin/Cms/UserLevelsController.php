<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\UsersLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserLevelsController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.users.levels.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(self::PATH_VIEW . "index")->with([
            "levels" => UsersLevel::orderBy('id', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }


    public function show(Request $request, $id)
    {
        return view(self::PATH_VIEW . "show")->with([
            "clan" => UsersLevel::where('id', id)->first()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "level" => "required",
            "start" => "required",
            "end" => "required"
        ]);
        $level = new UsersLevel();
        $level->level = $request->level;
        $level->start = $request->start;
        $level->end = $request->end;
        $level->save();

        return redirect("/admin/levels/users")->with("success", "Niveau de joueur ajouté");
    }

    public function delete(Request $request, $id)
    {
        $c = UsersLevel::where('id', $id)->first();

        if (!$c) {
            return redirect()->back()->with("error", "Aucune niveau de joueur trouvé");
        }

        $c->delete();

        return redirect()->back()->with("success", "Le niveau de joueur à bien été supprimé");
    }
}
