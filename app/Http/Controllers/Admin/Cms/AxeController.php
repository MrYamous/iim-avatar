<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Axe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AxeController extends Controller
{

    const PATH_VIEW = "admin.entities.cms.axe.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $axes = Axe::orderBy('id', 'DESC')->get();

        return view(self::PATH_VIEW . "index")->with([
            "axes" => $axes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
        ]);

        $axe = new Axe();
        $axe->name = $request->name;
        $axe->save();

        return redirect("admin/axe")->with("success", "Axe créé avec succès");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $axe = Axe::where('id', $id)->first();

        return view(self::PATH_VIEW . "show")->with([
            "axe" => $axe,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $axe = Axe::where('id', $id)->first();

        return view(self::PATH_VIEW . "edit")->with([
            "axe" => $axe,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "name" => "required"
        ]);
        $axe = Axe::where('id', $id);
        $axe->name = $request->name;
        $axe->save();

        return response()->json([
           'status' => 'success',
           'data' => $axe
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $axe = Axe::find($id);

        if ($axe !== null){
            $axe->delete();
        }
        return redirect()->back()->with("success", "Axe supprimé");
    }
}
