<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Block;
use App\Models\BlockCategory;
use App\Models\Page;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlocksController extends Controller
{
    
    const CONTROLLER = 'Admin\Cms\BlocksController@';
    
    public function show(Request $request, $id)
    {
        $page = Page::where('id', $id)->first();
        $block = $page->getBlocks;
        
        return view('admin.entities.cms.pages.blocks.show')->with([
            'page' => $page
        ]);
    }
    
    public function createBlock(Request $request, $id)
    {
        $page = Page::where('id', $id)->first();
        
        $block = new Block();
        $block->key = str_slug($request->name);
        $block->type = $request->type;
        $block->fk_category_id = $request->category;
        $block->fk_page_id = $page->id;
        if ($request->type === "text") {
            $block->value = $request->get('content');
        } elseif ($request->type === "image" && $request->has("image") && $request->hasFile("image")) {
            $file = $this->uploadImage($request->file("image"), str_slug($request->name) . "-page-$id");
            $block->value = $file->file;
        }
        $block->created_at = Carbon::now();
        $block->save();
        
        return redirect()->action('Admin\Cms\BlocksController@show', $id)->with('success', 'Votre block a bien été crée');
    }
    
    public function createFolder(Request $request, $id)
    {
        $page = Page::where('id', $id)->first();
        
        $category = new BlockCategory();
        $category->name = $request->name;
        $category->fk_page_id = $page->id;
        $category->slug = str_slug($request->name);
        $category->save();
        
        return redirect()->back()->with('success', 'Dossier crée');
    }
    
    public function showBlocks(Request $request, $id, $slug)
    {
        $page = Page::where('id', $id)->first();
        $category = $page->getCategories;
        
        return view('admin.entities.cms.pages.blocks.showBlock')->with([
            'page'     => $page,
            'category' => $category,
        ]);
    }
    
    public function showBlocksDetails(Request $request, $id, $slug, $blockId)
    {
        $page = Page::where('id', $id)->first();
        $category = $page->getCategories;
        $block = Block::where('id', $blockId)->first();
        
        return view('admin.entities.cms.pages.blocks.showBlockDetails')->with([
            'page'     => $page,
            'category' => $category,
            'block'    => $block
        ]);
    }
    
    public function updateBlock(Request $request, $id)
    {
        $block = Block::where('id', $id)->first();
        
        $block->key = $request->key;
        if ($block->type === "text") {
            $block->value = $request->value;
        } elseif ($block->type === "image" && $request->has("image") && $request->hasFile("image")) {
            $file = $this->uploadImage($request->file("image"), str_slug($request->name) . "-page-$id");
            $block->value = $file->file;
        }
        
        
        $block->save();
        
        return redirect()->back();
        
    }
    
    public function deleteBlock(Request $request, $id)
    {
        $block = Block::where('id', $id)->first();
        $block->delete();
        
        return redirect()->to("/admin/block/pages/" . $request->page)->with("success", "Le block à été supprimée");
        
    }
}
