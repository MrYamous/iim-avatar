<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Clan;
use App\Models\Item;
use App\Models\Render;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.items.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ss = Item::orderBy('id', 'DESC')->get();

        return view(self::PATH_VIEW . "index")->with([
            "items" => $ss
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }


    public function show(Request $request, $slug)
    {

        $item = Item::where('slug', $slug)->with('getUsers', 'getFile')->first();
        return view(self::PATH_VIEW . "show")->with([
            "item" => $item
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"
        ]);
        $item = new Item();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->slug = str_slug($request->name);
        $item->cost = $request->cost;

        if ($request->has('file')) {
            $o = $this->uploadImage($request->file, "$request->name" . "-" . str_random(14));
            $item->fk_file_id = $o->id;
        }
        $item->save();

        return redirect("/admin/items")->with("success", "ajouté");
    }

    public function delete($id)
    {
        $item = Item::where('id', $id)->first();

        if (!$item) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }

        $item->delete();

        return redirect()->back()->with("success", "Supprimé");
    }
}
