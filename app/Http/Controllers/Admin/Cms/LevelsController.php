<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\TypeChallenge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPUnit\Util\Type;

class LevelsController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.levels.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(self::PATH_VIEW . "index")->with([
            "levels" => TypeChallenge::orderBy('id', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"
        ]);

        $type = new TypeChallenge();
        $type->name = $request->name;
        $type->slug = str_slug($request->name);
        $type->point_win = $request->point_win;
        $type->point_contribution = $request->point_contribution;
        $type->description = $request->description;
        $type->save();


        return redirect("/admin/level")->with("success", "ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $c = TypeChallenge::where('id', $id)->first();

        if (!$c) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }

        $c->delete();

        return redirect()->back()->with("success", "Supprimé");
    }
}
