<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\User\Permission;
use App\Models\User\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    const PATH_VIEW = 'admin.entities.cms.users.roles.';
    const CONTROLLER = 'Admin\Cms\RolesController';

    public function index()
    {
        return view(self::PATH_VIEW . 'index')->with([
            'roles' => Role::all()
        ]);
    }

    public function create()
    {

        return view(self::PATH_VIEW . 'create')->with([
            'permissions' => Permission::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'display_name' => 'required',
        ]);

        $role = Role::create([
            'display_name' => $request->display_name,
            'description' => $request->description,
            'name' => str_slug($request->display_name)
        ]);

        if ($request->has('permissions') && !empty($request->get('permissions'))) {
            $role->attachPermissions($request->permissions);
            $role->syncChanges();
        }

        return redirect()->action(self::CONTROLLER . '@index')->with('success', "add");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $role = Role::where('id', $id)->first();
        return view(self::PATH_VIEW . 'edit')->with([
            'role' => $role,
            'permissions' => Permission::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'display_name' => 'required',
        ]);

        $role = Role::where('id', $id)->first();

        $role->update([
            'display_name' => request()->display_name,
            'description' => request()->description,
        ]);

        if (request()->has('permissions') && !empty(request()->get('permissions'))) {
            // Delete all permissions attached to the current role
            $role->detachPermissions($role->permissions);
            $role->attachPermissions(request()->permissions);
            $role->syncChanges();
        }

        return redirect()->action(self::CONTROLLER . '@index')->with('success', "ok");
    }


    public function delete(Request $request, $id)
    {
        $rol = Role::where('id', $id)->first();
        if (!$rol) {
            return redirect()->back()->with("error", "Aucun role trouvé");
        }

        $rol->delete();

        return redirect()->action(self::CONTROLLER . '@index')->with('success', "supprimé");
    }
}
