<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Clan;
use App\Models\Render;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RenderController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.render.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ss = Render::orderBy('id', 'DESC')->get();

        return view(self::PATH_VIEW . "index")->with([
            "renders" => $ss
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }


    public function show(Request $request, $slug)
    {
        $cl = Clan::where('slug', $slug)->with('getUsers', 'getImages')->first();
        return view(self::PATH_VIEW . "show")->with([
            "clan" => $cl
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"
        ]);
        $clan = new Clan();
        $clan->title = $request->name;
        $clan->slug = str_slug($request->name);
        $clan->description = $request->description;
        $clan->save();

        if ($request->has('files')) {
            $arrayIdFile = [];
            foreach ($request->file("files") as $i => $file) {
                $o = $this->uploadImage($file, "$clan->slug" . $i . "-" . str_random(14));
                $clan->getImages()->attach($o->id);
            }
        }

        if ($request->has('file_logo')) {
            $logo = $this->uploadImage($request->file_logo, "$clan->slug" . "-logo-" . str_random(14));
            $clan->fk_logo_id = $logo->id;
        }
        $clan->save();

        return redirect("/admin/clans")->with("success", "ajouté");
    }

    public function delete($id)
    {
        $c = Clan::where('id', $id)->first();

        if (!$c) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }

        $c->delete();

        return redirect()->back()->with("success", "Supprimé");
    }
}
