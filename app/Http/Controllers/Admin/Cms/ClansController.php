<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Clan;
use App\Models\Render;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClansController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.clans.";
    const URL_COLOR = "http://www.thecolorapi.com/id?hex=";
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ss = Clan::orderBy('id', 'DESC')->get();
        
        return view(self::PATH_VIEW . "index")->with([
            "clans" => $ss
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }
    
    
    public function show(Request $request, $slug)
    {
        $cl = Clan::where('slug', $slug)->with('getUsers', 'getImages')->first();
        
        return view(self::PATH_VIEW . "show")->with([
            "clan" => $cl
        ]);
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            "name"
        ]);
        
        $clan = new Clan();
        $clan->title = $request->name;
        $clan->slug = str_slug($request->name);
        $clan->description = $request->description;
        $clan->hexa_color = $request->hexa_color;
        $reponse = $this->getRgbColor($this->getUrlForRgbColor($request));
        $clan->rgb_color = $reponse->rgb->value;
        $clan->save();
        
        if ($request->has('files')) {
            $arrayIdFile = [];
            foreach ($request->file("files") as $i => $file) {
                $o = $this->uploadImage($file, "$clan->slug" . $i . "-" . str_random(14));
                $clan->getImages()->attach($o->id);
            }
        }
        
        if ($request->has('file_logo')) {
            $logo = $this->uploadImage($request->file_logo, "$clan->slug" . "-logo-" . str_random(14));
            $clan->fk_logo_id = $logo->id;
        }
        $clan->save();
        
        return redirect("/admin/clans")->with("success", "ajouté");
    }
    
    public function delete($id)
    {
        $c = Clan::where('id', $id)->first();
        
        if (!$c) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }
        
        $c->delete();
        
        return redirect()->back()->with("success", "Supprimé");
    }
    
    public function edit(Request $request, $slug)
    {
        $cl = Clan::where('slug', $slug)->with('getUsers', 'getImages', 'getLogo')->first();
        
        return view(self::PATH_VIEW . "edit")->with([
            "clan" => $cl
        ]);
    }
    
    public function update(Request $request, $slug)
    {
        
        
        $cl = Clan::where('slug', $slug)->with('getUsers', 'getImages')->first();
        
        $cl->title = $request->name;
        $cl->slug = str_slug($request->name);
        $cl->description = $request->description;
        if ($cl->hexa_color !== $request->hexa_color) {
            $cl->hexa_color = $request->hexa_color;
            $reponse = $this->getRgbColor($this->getUrlForRgbColor($request));
            $cl->rgb_color = $reponse->rgb->value;
        }
        
        
        $cl->save();
        if ($request->has("file_logo")) {
            
            $logo = $this->uploadImage($request->file('file_logo'), str_slug($request->name) . "-logo");
            $cl->fk_logo_id = $logo->id;
        }
        $arrFile = [];
        if ($request->has("files")) {
            foreach ($request->file('files') as $i => $file) {
                
                $fileInstance = $this->uploadImage($file, str_slug($request->name) . "$i" . "-logo");
                $arrFile[] = $fileInstance->id;
            }
            $cl->getImages()->attach($arrFile);
        }
        
        if ($request->has('file_logo')) {
            $logo = $this->uploadImage($request->file_logo, "$cl->slug" . "-logo-" . str_random(14));
            $cl->fk_logo_id = $logo->id;
        }
        $cl->save();
        
        return redirect()->action("Admin\Cms\ClansController@index")->with("success", "Clan modifié");
        
    }
    
    private function getRgbColor($url)
    {
        return json_decode($this->sendGetCUrl($url, false));
    }
    
    private function getUrlForRgbColor(Request $request)
    {
        return self::URL_COLOR . str_replace("#", "", $request->hexa_color);
    }
}
