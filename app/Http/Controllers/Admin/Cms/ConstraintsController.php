<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Constrainte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConstraintsController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.constraints.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(self::PATH_VIEW . "index")->with([
            "constraints" => Constrainte::orderBy('id', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::PATH_VIEW . "create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"
        ]);
        $constraint = new Constrainte();
        $constraint->name = $request->name;
        $constraint->slug = str_slug($request->name);
        $constraint->description = $request->description;
        $constraint->save();


        return redirect("/admin/constraints")->with("success", "ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $c = Constrainte::where('id', $id)->first();

        if (!$c) {
            return redirect()->back()->with("error", "Aucune challenge trouvé");
        }

        $c->delete();

        return redirect()->back()->with("success", "Supprimé");
    }
}
