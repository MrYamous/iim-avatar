<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Http\Controllers\Controller;
use App\Models\Clan;
use App\Models\Quiz\Answer;
use App\Models\Quiz\Question;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.quiz.";
    
    public function index()
    {
        return view(self::PATH_VIEW . 'index')->with([
            "questions" => Question::orderBy('id', 'DESC')->get()
        ]);
    }
    
    public function create()
    {
        $clans = Clan::all();
        
        return view(self::PATH_VIEW . 'create')->with([
            'clans' => $clans
        ]);
    }
    
    
    public function edit(Request $request, $id)
    {
        $question = Question::with('getAnswers', 'getFile')->find($id);
        
        return view(self::PATH_VIEW . 'edit')->with([
            'clans'    => Clan::all(),
            'question' => $question,
        ]);
    }
    
    public function update(Request $request, $id)
    {
        
        $request->validate([
            "question" => "required|string"
        ]);
        
        $question = Question::find($id);
        $question->title = $request->question;
        $question->content = $request->get('content');
        if ($request->hasFile("file")) {
            $o = $this->uploadImage($request->file, "$request->title" . "-" . str_random(14));
            $question->fk_file_id = $o->id;
        }
        $question->save();
        
        
        $a = [];
        foreach ($request->get('reponses') as $reponse) {
            $r = new Answer();
            $r->title = $reponse['reponse'];
            $r->fk_clan_id = $reponse['clans'];
            $r->less = $reponse['less'];
            $r->save();
            $a[] = $r->id;
        }
        $question->getAnswers()->sync($a);
        
        
        return redirect()->route("admin.quiz.index")->with("success", "Question modifiée");
        
        
    }
    
    public function store(Request $request)
    {
        $request->validate([
            "question" => "required|string"
        ]);
        $question = new Question();
        $question->title = $request->question;
        $question->content = $request->get('content');
        
        if ($request->hasFile("file")) {
            $o = $this->uploadImage($request->file, "$request->title" . "-" . str_random(14));
            $question->fk_file_id = $o->id;
        }
        $question->save();
        $a = [];
        foreach ($request->get('reponses') as $reponse) {
            $r = new Answer();
            $r->title = $reponse['reponse'];
            $r->fk_clan_id = $reponse['clans'];
            $r->less = $reponse['less'];
            $r->save();
            $a[] = $r->id;
        }
        $question->getAnswers()->attach($a);
        
        return redirect()->route("admin.quiz.index")->with("success", "Question crée");
    }
    
    
    public function show(Request $request, $id)
    {
        return view(self::PATH_VIEW . "show")->with([
            "question" => Question::with('getAnswers', "getFile")->find($id)
        ]);
    }
    
    
    public function delete(Request $request, $id)
    {
        $question = Question::find($id);
        
        if ($question)
            $question->delete();
        
        return redirect()->route("admin.quiz.index")->with("success", "Question suprimée");
        
        
    }
    
}
