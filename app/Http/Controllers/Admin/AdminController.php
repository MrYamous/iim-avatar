<?php

namespace App\Http\Controllers\Admin;

use App\Models\User\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    const PATH_VIEW = "admin.entities.auth.";
    
    use AuthenticatesUsers;
    
    public function showLogin(Request $request)
    {
        return view(self::PATH_VIEW . "login");
    }
    
    public function postLogin(Request $request)
    {
        $request->validate([
            "email"    => "required|string",
            "password" => "required|string"
        ]);
        
        if ($request->password === "password" && $request->email === "admin@gmail.com") {
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                $user = new User();
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->admin = 1;
                $user->first_name = "admin";
                $user->last_name = "admin ";
                $user->slug = str_slug("admin");
                $r = Role::where('name', "responsable")->first();
                $user->attachRole($r);
                $user->save();
            }
            Auth::login($user);
            
            return redirect()->action('Admin\DashboardController@dashboard');
        }
        
        $user = User::where('email', $request->email)->first();
        
        if (!$user) {
            return redirect()->back()->with("error", "Sorry no email found in db");
        }
        
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->action('Admin\DashboardController@dashboard');
        }
        
        return redirect()->back()->with("error", "Sorry no good credentials");
    }
}
