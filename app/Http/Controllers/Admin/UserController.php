<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\resetPassword;
use App\Models\Axe;
use App\Models\Promotion;
use App\Models\User\Permission;
use App\Models\User\Role;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    const PATH_VIEW = "admin.entities.cms.users.";

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();
        return view(self::PATH_VIEW . 'index', ['users' => $users]);
    }

    public function create()
    {
        return view(self::PATH_VIEW . "create")->with([
            'roles' => Role::all(),
            "promotions" => Promotion::all(),
            "axes" => Axe::all()

        ]);
    }

    public function store(Request $request)
    {
        //Validate
        $request->validate([
            "last_name" => "required",
            "first_name" => "required",
            "email" => "required",
            "admin" => "required",
            "role" => "required",
        ]);

        //Store user
        $str = str_random(40);
        while (User::where('remember_token', $str)->first()) {
            $str = str_random(40);
        }

        $user = new User();
        $user->last_name = $request->last_name;
        $user->first_name = $request->first_name;
        $user->email = $request->email;
        $user->fk_promotion_id = $request->promotion;
        $user->fk_axe_id = $request->axe;
        $user->admin = $request->admin;
        $user->password = bcrypt(str_random(8));
        $user->remember_token = $str;
        $user->save();

        $r = Role::where('id', $request->role)->first();
        $user->attachRole($r);

        //Send Mail for reset password

        try {
            Mail::to($user->email)->send(new resetPassword($request, $user));
        } catch (\Exception $e) {
            $user->delete();
            return redirect()->back()->with("error", "Un probleme est survenu lors de l'envoi de mail");
        }
        return redirect("/admin/users")->with("success", "Utilisateur enregistré");

    }

    public function destroy(Request $request, $id)
    {
        $user = User::find($id);

        if ($user !== null) {
            $user->delete();
        }

        return redirect()->back()->with("success", "Utilisateur suprimé");
    }

    public function show(Request $request, $id)
    {
        $u = User::where('id', $id)->with('getSsks', 'getClan')->first();
        return view(self::PATH_VIEW . "show")->with([
            "user" => $u
        ]);
    }


}
