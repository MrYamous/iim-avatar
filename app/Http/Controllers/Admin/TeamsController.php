<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamsController extends Controller
{

    const PATH_VIEW = "admin.entities.cms.teams.";

    public function index()
    {
        return view(self::PATH_VIEW . 'index')->with([
            'teams' => Team::orderBy('id', 'DESC')->get()
        ]);
    }

    public function show(Request $request, $id)
    {
        return view(self::PATH_VIEW . 'show')->with([
            "teams" => Team::with('getCreator', 'getConfirmMembers', 'getInvitedMembers', 'getChallenges')->find($id)
        ]);
    }

    public function delete(Request $request, $id)
    {
        $team = Team::find($id);

        if ($team) $team->delete();

        return redirect()->route("admin.teams.index")->with("success", "Equipe supprimée");
    }

}
