<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Chat\Conversation;
use App\Models\User\Role;
use App\Models\User\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/questions';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function validateDevinciEmail(array $data)
    {
        if ($data["email"]) {
            $findme = '@devinci.fr';
            $pos = strpos($data["email"], $findme);
            if (!$pos) {
                return ["error", "Vous devez utiliser une adresse mail devinci."];
            }
        }
    }

    public function register(Request $request)
    {
        $res = $this->validateDevinciEmail($request->all());
        if (is_array($res) && $res[0] === "error") redirect()->back()->with("error", $res[1]);

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    public function showRegistrationForm()
    {
        return view('app.entities.auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|string|email|max:255|unique:users',
            'password'   => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User\User
     */
    protected function create(array $data)
    {

        $users = User::where('slug', str_slug($data['last_name'] . $data["first_name"]))->get();
        if (!$users) {
            $slug = str_slug($data['last_name'] . $data["first_name"]);
        } else {
            $lenght = count($users);
            $slug = str_slug($data['last_name'] . $data["first_name"]) . "-" . ($lenght + 1);
        }

        $user = User::create([
            'first_name'      => $data['first_name'],
            'last_name'       => $data['last_name'],
            "slug"            => $slug,
            'fk_axe_id'       => $data['axe'],
            'fk_promotion_id' => $data['promotion'],
            'email'           => $data['email'],
            'password'        => Hash::make($data['password'])
        ]);

        $r = Role::where('name', "student")->first();
        $user->attachRole($r);

        $user->save();

        $conversation = Conversation::find(1);
        if($conversation) $conversation->getMembers()->attach($user);

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        Auth::login($user);
    }
}
