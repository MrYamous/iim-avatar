<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class HandleRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if ($request->user()) {
        //    auth()->user()->last_login = Carbon::now();
        //    auth()->user()->save();
        //}
        return $next($request);
    }
}
