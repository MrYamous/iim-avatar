<?php

namespace App\Http\Middleware;

use App\Models\User\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class Iim
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // THIS IS MY FIRST MIDDLEWARE LOAD
        if ($request->headers->get("authorization")) {
            if ($request->headers->get("authorization") === "Bearer user") {
                $user = User::where('email', 'glrd.remi@gmail.com')->first();
                if (!$user) {
                    return response()->json([
                        "error" => "No User"
                    ]);
                }
                $user->password = bcrypt('password');
                $user->save();
                Auth::login($user);
                //$argLogin = ["username" => "glrd.remi@gmail.com", "password" => 'password'];
                //$client = DB::table('oauth_clients')->where('id', 1)->first();
                //
                //if (!$client->password_client) {
                //    DB::table('oauth_clients')->where('id', 1)->update([
                //        "personal_access_client" => 0,
                //        "password_client"        => 1
                //    ]);
                //}
                //$data = array_merge($argLogin, [
                //    'grant_type'    => 'password',
                //    'scope'         => '*',
                //    'client_id'     => $client->id,
                //    'client_secret' => $client->secret,
                //]);
                //$response = app()->handle(Request::create('oauth/token', 'POST', $data, [], [], [
                //    'HTTP_Accept' => 'application/json',
                //]));
                //dd(json_decode($response->getContent(), true), $response->getContent());
                //$auth_token = json_decode($response->getContent(), true);
                //$access_token = $auth_token["access_token"];
                $access_token = Auth::user()->createToken('login')->accessToken;
                
                $request->headers->set("authorization", "Bearer " . $access_token);
                
                //dd($request->bearerToken());
                
                return $next($request);
            } else {
                return $next($request);
            }
        }
        
        return $next($request);
    }
}
