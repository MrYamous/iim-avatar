<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user() || !Auth::check()) {
            //return response()->json([
            //    'message' => 'Vous devez être connectée pour accéder a cette partie du site',
            //], 401);
            return redirect("/admin/login")->with("error", "Vous devez être connectée pour acceder a cette partie du site");
        }
        if ($request->user()->admin !== 1) {
            //return response()->json([
            //    'message' => 'Vous devez être administrateur pour accéder a cette partie du site',
            //], 403);
            return redirect()->back()->with("error", "Vous n'avez pas accès a cette partie du site");
        }
        if ($request->user()->admin === 1) return $next($request);
    }
}
