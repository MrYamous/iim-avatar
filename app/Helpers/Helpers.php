<?php


use App\Models\Block;
use App\Models\UsersLevel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

function truncateTable(Array $tables)
{
    foreach ($tables as $table) {
        DB::table($table)->truncate();
    }
    
    return true;
}

function displayState(array $array): string
{
    return implode($array, ",");
}

function getScoreByUserAndSsk($idSsk, $idUser)
{
    $r = DB::table('ssks_users')->where([["fk_user_id", $idUser], ["fk_ssk_users", $idSsk]])->first();
    if ($r) {
        return $r->score;
    }
    
    return "Aucun score";
}

function getTextTranslates($name)
{
    $text = \App\Models\Cms\Texts\Text::where('key', $name)->first();
    $text = json_decode($text->text);
    //$local = session('locale');
    $local = request()->segment(1);
    //$local = 'fr';
    //if($local === "FR"){
    return $text->$local ? $text->$local : 'missing text : ' . $name;
    //}else{
    //    return $text->en ? $text->en : var_dump('missing text : ' . $name);
    //}
    //dd($local);
    
}


function unique_random($table, $col, $chars = 16)
{
    
    $unique = false;
    
    // Store tested results in array to not test them again
    $tested = [];
    
    do {
        
        // Generate random string of characters
        $random = str_random($chars);
        
        // Check if it's already testing
        // If so, don't query the database again
        if (in_array($random, $tested)) {
            continue;
        }
        
        // Check if it is unique in the database
        $count = DB::table($table)->where($col, '=', $random)->count();
        
        // Store the random character in the tested array
        // To keep track which ones are already tested
        $tested[] = $random;
        
        // String appears to be unique
        if ($count == 0) {
            // Set unique to true to break the loop
            $unique = true;
        }
        
        // If unique is still false at this point
        // it will just repeat all the steps until
        // it has generated a random string of characters
        
    } while (!$unique);
    
    
    return $random;
}


function active($route, $action)
{
    if ($route === $action) {
        return "active";
    }
    
    return "";
}

function createUser()
{
    $faker = Faker\Factory::create();
    $user = \App\Models\Users\User::create([
        //"name" => $faker->name,
        "email"    => $faker->email,
        "password" => bcrypt("password"),
    ]);
    $roleUser = \App\Models\Users\Role::where('name', 'user')->first();
    $user->attachRole($roleUser);
    
    return $user;
}


function getDayOfStartChallenge($startDate)
{
    $startDate = Carbon\Carbon::createFromFormat('Y-m-d', $startDate);
    $now = \Carbon\Carbon::now();
    $diff = $now->diff($startDate);
    if ($now > $startDate) {
        return true;
    }
    
    return $diff->days;
}

function canSubmitChallenge($startDate, $challenge)
{
    if (Auth::check()) {
        if (!Auth::user()->getClan) {
            return false;
        }
    }
    $startDate = Carbon::createFromFormat('Y-m-d', $startDate);
    $now = Carbon::now();
    $date = new DateTime($startDate);
    $now = new DateTime($now);
    $can = canSubmitChallengeByClan($now, $date, $challenge);
    if ($can) {
        return true;
        //dd($challenge);
        //Know if challenge is single or in team
    }
    
    return false;
}

function canSubmitChallengeByClan($now, $date, $challenge)
{
    if ($now > $date || $now->format("d/m/Y") == $date->format("d/m/Y")) {
        if (Auth::check()) {
            if ($challenge->all_clans) {
                return true;
            }
            if ($challenge->getClans !== null || $challenge->getClans->isNotEmpty()) {
                return true;
            }
            if (Auth::user()->getClan) {
                foreach ($challenge->getClans as $getClan) {
                    if (Auth::user()->getClan->id === $getClan->id) {
                        return true;
                    }
                }
            }
            
        }
    }
    
    return false;
}


function getClanLevel(\App\Models\Clan $clan)
{
    $arrayOfPointUser = [];
    foreach ($clan->getUsers as $i => $getUser) {
        $arrayOfPointUser[ $i ] = $getUser->points;
    }
    $totalPoint = array_sum($arrayOfPointUser);
    
    $level = UsersLevel::where("start", "<=", $totalPoint)->where("end", ">=", $totalPoint)->first();
    
    return $level ? $level->level : "Niveau trop élevée";
}

function getClanPoint(\App\Models\Clan $clan)
{
    $arrayOfPointUser = [];
    foreach ($clan->getUsers as $i => $getUser) {
        $arrayOfPointUser[ $i ] = $getUser->points;
    }
    
    return array_sum($arrayOfPointUser);
}

function getPlaceHolditImage()
{
    return "https://via.placeholder.com/150";
}


/**
 * @param $name
 */
function getTexts($name)
{
    $text = Block::where('key', $name)->first();
    
    return $text ? $text->value : var_dump('missing text : ' . $name);
}


function userHasAlreadySendRenderForThisChallenge(\App\Models\User\User $user, \App\Models\Challenge $challenge)
{
    foreach ($challenge->getRender as $render) {
        if ($user->id === $render->fk_user_id) {
            return true;
        }
    }
    
    return false;
}

function challengeIsStart(\App\Models\Challenge $challenge)
{
    //Start date of challenge is equal or less than now
    
    $start_date = Carbon::createFromFormat('Y-m-d', $challenge->start_date);
    $now = Carbon::now();
    $start_date = new DateTime($start_date);
    $now = new DateTime($now);
    
    if ($start_date->format("d/m/Y") === $now->format("d/m/Y") || $start_date < $now) {
        return true;
    }
    
    return false;
}

function explodeAuthToken(array $auth_token)
{
    $jwt = array_get($auth_token, 'access_token');
    if (!$jwt) {
        return null;
    }
    $tks = explode('.', $jwt);
    
    list($headb64, $bodyb64, $cryptob64) = $tks;
    $body = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64));
    $col = collect($body);
    
    return $col;
}


function challengeIsFinish(\App\Models\Challenge $challenge)
{
    $end_date = Carbon::createFromFormat('Y-m-d', $challenge->end_date);
    $now = Carbon::now();
    $end_date = new DateTime($end_date);
    $now = new DateTime($now);
    
    if ($end_date->format("d/m/Y") === $now->format("d/m/Y") || $end_date < $now) {
        return true;
    }
    
    return false;
}

function canAccessToThisChallengeByClan(\App\Models\Challenge $challenge, \App\Models\User\User $user)
{
    //Clan of user can participate to this challenge
    
    if ($challenge->all_clans) {
        return true;
    }
    
    if ($challenge->getClans !== null || $challenge->getClans->isNotEmpty()) {
        return true;
    }
    
    if ($user->getClan) {
        foreach ($challenge->getClans as $getClan) {
            if ($user->getClan->id === $getClan->id) {
                return true;
            }
        }
    }
    
    return false;
}


function userAlreadyParticpateToThisChallenge(\App\Models\User\User $user, \App\Models\Challenge $challenge)
{
    if ($challenge->getUsers->isNotEmpty()) {
        
        foreach ($challenge->getUsers as $u) {
            if ($u->id === $user->id) {
                return true;
            }
        }
    }
    
    return false;
}


function isWinnerUser(\App\Models\Challenge $challenge, \App\Models\User\User $user)
{
    foreach ($challenge->getWinners as $us) {
        if ($user->id === $us->id) {
            return true;
        }
    }
    
    return false;
}