<?php

namespace App\Observers;

use App\Models\Clan;

class ClanObserver
{
    public static function saving(Clan $clan){
        self::staticEditSLug($clan);
    }
    public static function updating(Clan $clan){
        self::staticEditSLug($clan);
    }
    public static function creating(Clan $clan){
        self::staticEditSLug($clan);
    }

    private static function staticEditSLug(Clan $clan){
        $clan->slug = str_slug($clan->title,"-",'fr');
    }
}
