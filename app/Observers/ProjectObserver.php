<?php

namespace App\Observers;

use App\Models\Project;

class ProjectObserver
{
    public static function saving(Project $project){
        self::staticEditSLug($project);
    }
    public static function updating(Project $project){
        self::staticEditSLug($project);
    }
    public static function creating(Project $project){
        self::staticEditSLug($project);
    }

    private static function staticEditSLug(Project $project){
        $project->slug = str_slug($project->title,"-",'fr');
    }
}
