<?php

namespace App\Observers;

use App\Models\Courr;

class CourObserver
{
    public static function saving(Cour $cour){
        self::staticEditSLug($cour);
    }
    public static function updating(Cour $cour){
        self::staticEditSLug($cour);
    }
    public static function creating(Cour $cour){
        self::staticEditSLug($cour);
    }

    private static function staticEditSLug(Cour $cour){
        $cour->slug = str_slug($cour->title,"-",'fr');
    }
}
