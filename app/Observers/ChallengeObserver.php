<?php

namespace App\Observers;

use App\Models\Challenge;

class ChallengeObserver
{
    public static function saving(Challenge $challenge)
    {
        self::staticEditSLug($challenge);
    }

    public static function updating(Challenge $challenge)
    {
        self::staticEditSLug($challenge);
    }

    public static function creating(Challenge $challenge)
    {
        self::staticEditSLug($challenge);
    }

    private static function staticEditSLug(Challenge $challenge)
    {
        $challenge->slug = str_slug($challenge->title, "-", 'fr');
    }
}
