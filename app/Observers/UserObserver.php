<?php

namespace App\Observers;

use App\Models\File;
use App\Models\User\User;

class UserObserver
{
    public static function saving(User $user)
    {
        //self::staticEditSLug($user);
    }
    
    public static function updating(User $user)
    {
        //self::staticEditSLug($user);
    }
    
    public static function creating(User $user)
    {
        //self::staticEditSLug($user);
    }
    
    private static function staticEditSLug(User $user)
    {
        $user->slug = str_slug($user->last_name, "-", 'fr') . '.' . str_slug($user->first_name, "-", 'fr');
    }
    
    public function created(User $user)
    {
        $file = new File();
        $file->content = "avatar";
        $file->file = "http://i.imgur.com/RfkIclQ.png";
        $file->fk_user_id = $user->id;
        $file->type = "png";
        $file->save();
        $user->fk_avatar_id = $file->id;
        $user->save();
    }
}
