<?php

namespace App\Events\Chat;

use App\Http\Resources\Chat\ChatMessageResource;
use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostConversationMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    private $message;
    private $conversation;
    
    /**
     * Create a new event instance.
     *
     * @param Message $message
     * @param Conversation $conversation
     */
    public function __construct(Message $message, Conversation $conversation)
    {
        $this->message = $message;
        $this->conversation = $conversation;
    }
    
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $id = $this->conversation->id;
        if ($id === 1) $id = 'global';
        
        return new PresenceChannel('chat-' . $id);
    }
    
    public function broadcastAs()
    {
        return 'new.message';
    }
    
    public function broadcastWith()
    {
        $this->message->load('getUser', 'getUser.getClan');
        
        return ['message' => new ChatMessageResource($this->message)];
    }
}
