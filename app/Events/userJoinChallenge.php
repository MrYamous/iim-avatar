<?php

namespace App\Events;

use App\Http\Resources\ChallengesResource;
use App\Models\Challenge;
use App\Models\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class userJoinChallenge
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private $challenge;
    private $user;

    /**
     * Create a new event instance.
     *
     * @param Challenge $challenge
     * @param User $user
     */
    public function __construct(Challenge $challenge, User $user)
    {
        $this->challenge = $challenge;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('app.user.' . $this->user->id);
    }

    public function broadcastAs()
    {
        return 'join.challenge';
    }

    public function broadcastWith()
    {
        return ['challenge' => new ChallengesResource($this->challenge)];
    }
}
