<?php

namespace App\Mail;

use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param Request $request
     * @param $user
     */
    public function __construct(Request $request, $user)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('valkyria@devinci.fr')
            ->subject('Bienvenue à Valkyria')
            ->markdown('app.entities.mails.inscription')
            ->with([
                'request' => $this->request,
                'user' => $this->user
            ]);
    }
}
