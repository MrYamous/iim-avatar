<?php

namespace App\Traits;

use App\Models\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait FileTrait
{
    function uploadImage($image, $name, $width = null, $height = null, $format = false)
    {
        $img = Image::make($image->getRealPath());
        if ($width || $height) {
            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }
        
        if ($format) {
            $filename = str_slug($name) . '.' . $format;
        } else {
            $filename = str_slug($name) . '.' . $image->getClientOriginalExtension();
        }
        
        // Check if image already exists in uploads repository and add -$i if it exists
        $path = $this->getFolderName()['uploads'] . $filename;
        $pathDb = $this->getFolderName()['db'] . $filename;
        $i = 1;
        while (file_exists($path)) {
            if ($format) {
                $filename = str_slug($name) . '-' . $i . '.' . $format;
            } else {
                $filename = str_slug($name) . '-' . $i . '.' . $image->getClientOriginalExtension();
            }
            $path = $this->getFolderName()['uploads'] . $filename;
            $pathDb = $this->getFolderName()['db'] . $filename;
            $i++;
        }
        $img->save($path);
        
        return $this->saveImageToDb($pathDb, $img->mime(), $name);
    }
    
    function getFolderName()
    {
        $currentDate = Carbon::now(); // Get current date
        $uploadsPath = '/uploads';
        $realPath = public_path() . $uploadsPath;
        
        // If year directory doesn't exist
        if (!is_dir($realPath . '/' . $currentDate->format('Y'))) {
            mkdir($realPath . '/' . $currentDate->format('Y'), 0777);
        }
        // If month directory doesn't exist
        if (!is_dir($realPath . '/' . $currentDate->format('Y') . '/' . $currentDate->format('m'))) {
            mkdir($realPath . '/' . $currentDate->format('Y') . '/' . $currentDate->format('m'), 0777);
        }
        
        return [
            'uploads' => $realPath . '/' . $currentDate->format('Y') . '/' . $currentDate->format('m') . '/',
            'db'      => $uploadsPath . '/' . $currentDate->format('Y') . '/' . $currentDate->format('m') . '/'
        ];
    }
    
    function saveImageToDb($filename, $type, $name)
    {
        $file = File::create([
            'file'       => $filename,
            'type'       => $type,
            'content'    => str_random(32),
            'fk_user_id' => Auth::check() ? Auth::user()->id : null
        ]);
        
        return $file;
    }
    
    
    function saveFile($request, UploadedFile $file, $type, $name = "", $idUser = false)
    {
        if (!$file->isValid()) {
            return [
                "error",
                "The file with name : '" . $file->getClientOriginalName() . " is not valid"
            ];
        }
        Auth::check() ? $idUser = Auth::user()->id : 0;
        if ($name === "") {
            $name = false;
        }
        if ($name) {
            $originalFilename = $name;
        } else {
            $originalFilename = $file->getClientOriginalName();
        }
        
        $path = '/files';
        $file = Storage::disk('uploads')->put($path, $file);
        $pathDb = Storage::url($file);
        $pathDb = str_replace("/storage", "/uploads", $pathDb);
        
        $fileInstace = new File();
        $fileInstace->file = $pathDb;
        $fileInstace->type = $type;
        $fileInstace->content = $originalFilename;
        //$fileInstace->slug = str_slug($originalFilename);
        $fileInstace->fk_user_id = $idUser;
        $fileInstace->save();
        
        return $fileInstace;
    }
    
    
}
