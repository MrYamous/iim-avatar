<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# RUN THE COMMAND WITH docker-compose -f dev.docker-compose.yml up -d
# GO TO 127.0.0.1:8081 for connect to phpmyadmin

## About Project

- Install 
    - `git clone git@gitlab.com:glrd78/iim-avatar.git`
    - `cd iim-avatar`
    - `cp .env.docker.example .env`
    - `composer install`
    - `php artisan key:generate`
    - `docker-compose -f dev.docker-compose.yml up -d`
    - `php artisan migrate:fresh`
    - `php artisan db:seed`

- Run serv   ``php artisan serve and go 127.0.0.1:8000``

- To view database go to phpmyadmin ``127.0.0.1:8081``

- If you are in MAC OSX you can connect with *sequel pro* by
    - Hote : `127.0.0.1`
    - Utilisateur : `root`
    - Mdp : `root`
    - Port : `3309`

- Run serv with specific port   ``php artisan serve --port 8010``      