/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
// require('notie');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

navBarCheckScrollTop(); // initial check
$(window).scroll(function (e) {
    navBarCheckScrollTop(); // check on scroll
});

function navBarCheckScrollTop() {
    if ($(window).scrollTop() >= 80) {
        $('#nav-wrapper').addClass('nav-bg-dark');
    } else {
        $('#nav-wrapper').removeClass('nav-bg-dark');
    }
}