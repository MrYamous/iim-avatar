@include('admin.templates.header')
@include('admin.templates.navbar')
@include('admin.templates.navtop')


@yield('content')

@include('admin.templates.footer')
