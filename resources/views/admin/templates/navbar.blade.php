<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-header">
        <a href="">
            {{--<img src="{{ asset('pages-assets/img/logo_white.png') }}" alt="logo" class="brand"--}}
            {{--data-src="{{ asset('pages-assets/img/logo_white.png') }}"--}}
            {{--data-src-retina="{{ asset('pages-assets/img/logo_white_2x.png') }}" width="78" height="22">--}}
        </a>
    </div>
    <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="m-t-30">
                <a href="{{ route('admin') }}">
                    <span class="title">Home</span>
                </a>
                <span class="bg-success icon-thumbnail">
                    <i class="pg-home"></i>
                </span>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title">Quiz</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail">
                    <i class="fa fa-user"></i>
                </span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.quiz.index') }}">Tous les questions</a>
                        <span class="icon-thumbnail">Cls</span>
                    </li>
                    <li>
                        <a href="{{ route('admin.quiz.create') }}">Ajouter une question</a>
                        <span class="icon-thumbnail">Cls</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title">Utilisateurs</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail">
                    <i class="fa fa-user"></i>
                </span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.users.index') }}">Tous les utilisateurs</a>
                        <span class="icon-thumbnail">utls</span>
                    </li>
                    
                    <li>
                        <a href="{{ action('Admin\Cms\RolesController@index') }}">Tous les roles</a>
                        <span class="icon-thumbnail">rls</span>
                    </li>
                    <li class="">
                        <a href="javascript:;" class="w-100"><span class="title">Niveau des joueurs</span>
                            <span class="arrow"></span></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ action('Admin\Cms\UserLevelsController@create')}}">Ajouter un niveau</a>
                                <span class="icon-thumbnail">Chlg</span>
                            </li>
                            <li>
                                <a href="{{ action('Admin\Cms\UserLevelsController@index') }}">Voir toutes les tranches
                                                                                               de niveau</a>
                                <span class="icon-thumbnail">Chlg</span>
                            </li>
                        </ul>
                    </li>
                    
                    {{--<li>--}}
                    {{--<a href="">Second Lien</a>--}}
                    {{--<span class="icon-thumbnail">rls</span>--}}
                    {{--</li>--}}
                </ul>
            </li>
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Pages</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\PagesController@create') }}">Ajouter une page</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\PagesController@index') }}">Toutes les pages</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Blog</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;" class="w-100"><span class="title">Post</span>--}}
            {{--<span class="arrow"></span></a>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\Blog\PostController@create') }}">Ajouter un article</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\Blog\PostController@index') }}">Tous les articles</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;" class="w-100"><span class="title">Categories</span>--}}
            {{--<span class="arrow"></span></a>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\Blog\CategoriesController@create') }}">Ajouter une--}}
            {{--categorie</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\Blog\CategoriesController@index') }}">Toutes les--}}
            {{--categories</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{----}}
            {{--</li>--}}
            
            <li>
                <a href="javascript:;">
                    <span class="title">Challenges</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail">
            <i class="fa fa-user"></i>
            </span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="javascript:;" class="w-100"><span class="title">Niveaux</span>
                            <span class="arrow"></span></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ route('admin.levels.create') }}">Ajouter un niveau</a>
                                <span class="icon-thumbnail">Chlg</span>
                            </li>
                            <li>
                                <a href="{{ route('admin.levels.index') }}">Voir tous les niveaux</a>
                                <span class="icon-thumbnail">Chlg</span>
                            </li>
                        </ul>
                    </li>
                    {{--<li class="">--}}
                    {{--<a href="javascript:;" class="w-100"><span class="title">Contraintes</span>--}}
                    {{--<span class="arrow"></span></a>--}}
                    {{--<ul class="sub-menu">--}}
                    {{--<li>--}}
                    {{--<a href="{{ route('constraints') }}">Ajouter une contrainte</a>--}}
                    {{--<span class="icon-thumbnail">Chlg</span>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="{{ route('admin.constraint.index') }}">Voir toutes les contraintes</a>--}}
                    {{--<span class="icon-thumbnail">Chlg</span>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    
                    <li class="">
                        <a href="javascript:;" class="w-100"><span class="title">Challenges Validés</span>
                            <span class="arrow"></span></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ route('admin.challenges.index_challenges_valides') }}">Tous les challenges
                                                                                                   validés</a>
                                <span class="icon-thumbnail">Chlg</span>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="javascript:;" class="w-100"><span class="title">Challenges non Validés</span>
                            <span class="arrow"></span></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ action('Admin\ChallengesController@getChallengesNonValides') }}">Tous les
                                                                                                             challenges
                                                                                                             non
                                                                                                             validés</a>
                                <span class="icon-thumbnail">Chlg</span>
                            </li>
                            {{--<li>--}}
                            {{--<a href="{{ action('Admin\ChallengesStateController@index') }}">Avancement des--}}
                            {{--challenges</a>--}}
                            {{--<span class="icon-thumbnail">Chlg</span>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.challenges.create') }}">Ajouter un challenge</a>
                        <span class="icon-thumbnail">Chlg</span>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title">Equipes</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail">
                    <i class="fa fa-user"></i>
                </span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.teams.index') }}">Toutes les équipes</a>
                        <span class="icon-thumbnail">Cls</span>
                    </li>
                </ul>
            </li>
    
            <li>
                <a href="javascript:;">
                    <span class="title">Familles</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail">
                    <i class="fa fa-user"></i>
                </span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.clans.create') }}">Ajouter une famille</a>
                        <span class="icon-thumbnail">Cls</span>
                    </li>
                    <li>
                        <a href="{{ route('admin.clans.index') }}">Toutes les familles</a>
                        <span class="icon-thumbnail">Cls</span>
                    </li>
                </ul>
            </li>
            
            {{--<li>--}}
                {{--<a href="javascript:;">--}}
                    {{--<span class="title">Rendus</span>--}}
                    {{--<span class="arrow"></span>--}}
                {{--</a>--}}
                {{--<span class="icon-thumbnail">--}}
                    {{--<i class="fa fa-user"></i>--}}
                {{--</span>--}}
                {{--<ul class="sub-menu">--}}
                    {{--<li>--}}
                        {{--<a href="{{ route('admin.render.index') }}">Tous les rendus</a>--}}
                        {{--<span class="icon-thumbnail">Rnd</span>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            
            <li>
                <a href="javascript:;">
                    <span class="title">Objets</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail">
                    <i class="fa fa-user"></i>
                </span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.items.index') }}">Tous les objets</a>
                        <span class="icon-thumbnail">Itms</span>
                    </li>
                </ul>
            </li>
            
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Soft Skills</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin.ssk.create') }}">Ajouter un soft skill</a>--}}
            {{--<span class="icon-thumbnail">Ssk</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="{{ route('admin.ssk.index') }}">Tout les soft skills</a>--}}
            {{--<span class="icon-thumbnail">Ssk</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Blog</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;" class="w-100"><span class="title">Post</span>--}}
            {{--<span class="arrow"></span></a>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="">Ajouter un article</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="') }}">Tout les articles</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;" class="w-100"><span class="title">Categorie</span>--}}
            {{--<span class="arrow"></span></a>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="') }}">Ajouter une categorie</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="') }}">Toutes les categories</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Salle</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="">Ajouter une salle</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="}}">Toutes les salles</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Réservations</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li>--}}
            {{--<a href="}}">Toutes les réservations</a>--}}
            {{--<span class="icon-thumbnail">pgs</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--//GROUP QUESTION--}}
            {{--<li>--}}
            {{--<a href="javascript:;">--}}
            {{--<span class="title">Multi Menu</span>--}}
            {{--<span class="arrow"></span>--}}
            {{--</a>--}}
            {{--<span class="icon-thumbnail">--}}
            {{--<i class="fa fa-user"></i>--}}
            {{--</span>--}}
            {{----}}
            {{--<ul class="sub-menu">--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;" class="w-100"><span class="title">Sub Menu</span>--}}
            {{--<span class="arrow"></span></a>--}}
            {{--<ul class="sub-menu" style="display: none;">--}}
            {{--<li>--}}
            {{--<a href="">Sub First Link</a>--}}
            {{--<span class="icon-thumbnail">Sm</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a href="">Sub Second Link</a>--}}
            {{--<span class="icon-thumbnail">Sm</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>
