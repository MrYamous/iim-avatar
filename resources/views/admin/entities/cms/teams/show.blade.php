@extends('admin')

@section('css')
    <link href="{{ asset('pages-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
@stop
@section('js')
    <script src="{{ asset('pages-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            // DATEPICKER - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/index.html
            $.fn.datepicker.dates['fr'] = {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                daysMin: ["D", "L", "M", "M", "J", "V", "S"],
                months: months,
                monthsShort: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juill", "Août", "Sept", "Oct", "Nov", "Déc"],
                today: "Aujourd'hui",
                clear: "Clear",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 1
            };
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy', // FORMAT - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format
                language: 'fr',
                todayHight: true
            });
            $('#time').timepicker({
                showMeridian: false
            }).on('show.timepicker', function (e) {
                var widget = $('.bootstrap-timepicker-widget');
                widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({});
            $('.js-example-basic-multiple').select2();
        });
    </script>
@stop

@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Tableau de bord</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.teams.index') }}">Equipes</a>
                </li>
                <li class="breadcrumb-item active">Voir une question</li>
            </ol>
            {{--            <form action="{{ action('Admin\ChallengesController@store') }}" method="POST" enctype="multipart/form-data">--}}
            {{--{{ csrf_field() }}--}}
            <div class="row">
                <div class="col-xl-7 col-lg-6 ">
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Voir une équipe
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} form-group-default required"
                                         aria-required="true">
                                        <label>Nom équipe</label>
                                        <p>
                                            {{ $teams->name }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('id') ? 'has-error' : '' }} form-group-default required"
                                         aria-required="true">
                                        <label>Equipe numéro:</label>
                                        <p>
                                            {{ $teams->id }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('complete') ? 'has-error' : '' }} form-group-default required"
                                         aria-required="true">
                                        <label>Equipe complete ?</label>
                                        <p>
                                            {{ $teams->complete === 0 ? 'Non' : 'Oui' }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} form-group-default required"
                                         aria-required="true">
                                        <label>Créateur de l'équipe</label>
                                        <p>
                                            {{ $teams->getCreator->name }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 ">
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Voir les challenges de l'équipe
                            </div>
                        </div>
                        <div class="card-block">
                            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                                <thead>
                                <tr>
                                    <th style="width:10%;">#</th>
                                    <th>Nom des Challenges</th>
                                    <th>Description des challenges</th>
                                    <th style="width:20%">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teams->getChallenges AS $key => $challenge)

                                    <tr>
                                        <td style="width:10%;" class="v-align-middle">
                                            {{ $key+1 }}
                                        </td>
                                        <td class="v-align-middle semi-bold">
                                            {{ $challenge->title }}
                                        </td>
                                        <td class="v-align-middle">
                                            {{ $challenge->description }}
                                        </td>
                                        <td style="width:20%" class="v-align-middle d-flex">
                                            <a href="{{ action('Admin\ChallengesController@show', $challenge->slug) }}"
                                               class="btn btn btn-primary mr-2">
                                                Voir
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 ">
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Voir les membres confirmés
                            </div>
                        </div>
                        <div class="card-block">
                            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                                <thead>
                                <tr>
                                    <th style="width:10%;">#</th>
                                    <th>Prénom</th>
                                    <th>Nom de famille</th>
                                    <th>email</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teams->getConfirmMembers AS $key => $user)

                                    <tr>
                                        <td style="width:10%;" class="v-align-middle">
                                            {{ $key+1 }}
                                        </td>
                                        <td class="v-align-middle semi-bold">
                                            {{ $user->first_name }}
                                        </td>
                                        <td class="v-align-middle semi-bold">
                                            {{ $user->last_name }}
                                        </td>
                                        <td class="v-align-middle">
                                            {{ $user->email }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 ">
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Voir les membres invités
                            </div>
                        </div>
                        <div class="card-block">
                            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                                <thead>
                                <tr>
                                    <th style="width:10%;">#</th>
                                    <th>Prénom</th>
                                    <th>Nom de famille</th>
                                    <th>email</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teams->getInvitedMembers AS $key => $user)

                                    <tr>
                                        <td style="width:10%;" class="v-align-middle">
                                            {{ $key+1 }}
                                        </td>
                                        <td class="v-align-middle semi-bold">
                                            {{ $user->first_name }}
                                        </td>
                                        <td class="v-align-middle semi-bold">
                                            {{ $user->last_name }}
                                        </td>
                                        <td class="v-align-middle">
                                            {{ $user->email }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

            {{--</form>--}}
        </div>
    </div>
@stop

