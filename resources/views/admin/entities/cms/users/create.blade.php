@extends('admin')

@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\Cms\RolesController@index') }}">Utilisateur</a>
                </li>
                <li class="breadcrumb-item active">Ajouter un utilisateur</li>
            </ol>
            <form action="{{ action('Admin\UserController@store') }}" method="post"
                  data-toggle="validator">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xl-7 col-lg-6 ">
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="card-title">
                                    Crée un utilisateur
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Nom de l'utilisateur</label>
                                            <input type="text" class="form-control" name="last_name"
                                                   value="{{ old('last_name') }}"
                                                   placeholder="Nom de l'utilisateur" required>
                                            @if ($errors->has('last_name'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Prénom de l'utilisateur</label>
                                            <input type="text" class="form-control" name="first_name"
                                                   value="{{ old('first_name') }}"
                                                   placeholder="Prénom de l'utilisateur" required>
                                            @if ($errors->has('first_name'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Email de l'utilisateur</label>
                                            <input type="text" class="form-control" name="email"
                                                   value="{{ old('email') }}"
                                                   placeholder="Email de l'utilisateur" required>
                                            @if ($errors->has('email'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('promotion') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Promotion de l'utilisateur</label>
                                            <select name="promotion" id="" class="form-control">
                                                @foreach($promotions AS $promotion)
                                                    <option value="{{ $promotion->id }}">{{ $promotion->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('axe') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Axe de l'utilisateur</label>
                                            <select name="axe" id="" class="form-control">
                                                @foreach($axes AS $axe)
                                                    <option value="{{ $axe->id }}">{{ $axe->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('admin') ? 'has-error' : '' }} form-group-default">
                                            <label class="">Admin ?</label>
                                            <select class="full-width form-control"
                                                    name="admin">
                                                <option value="1">Oui</option>
                                                <option value="0">Non</option>
                                            </select>
                                            @if ($errors->has('admin'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('admin') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="card-title">
                                    Roles
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix">
                                    <select name="role" id="" class="form-control">
                                        @foreach($roles AS $role)
                                            <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6">
                        <div class="card card-transparent">
                            <div class="card-header ">
                                <div class="card-title">
                                    Ajouter un utilisateur
                                </div>
                            </div>
                            <div class="card-block">
                                <h3>
                                    Ajouter un utilisateur
                                </h3>
                                <p>
                                    Un email sera automatiquement envoyé a l'utilisateur pour qu'il initialise sont mot de passe
                                </p>
                                <br>
                                <button type="submit" class="btn btn-primary btn-cons">
                                    Ajouter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop