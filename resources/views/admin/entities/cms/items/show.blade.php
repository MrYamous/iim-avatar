@extends('admin')

@section('css')
    <link href="{{ asset('pages-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
@stop
@section('js')
    <script src="{{ asset('pages-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            // DATEPICKER - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/index.html
            $.fn.datepicker.dates['fr'] = {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                daysMin: ["D", "L", "M", "M", "J", "V", "S"],
                months: months,
                monthsShort: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juill", "Août", "Sept", "Oct", "Nov", "Déc"],
                today: "Aujourd'hui",
                clear: "Clear",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 1
            };
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy', // FORMAT - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format
                language: 'fr',
                todayHight: true
            });
            $('#time').timepicker({
                showMeridian: false
            }).on('show.timepicker', function (e) {
                var widget = $('.bootstrap-timepicker-widget');
                widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({});
            $('.js-example-basic-multiple').select2();
        });
    </script>
@stop

@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Tableau de bord</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.items.index') }}">Item</a>
                </li>
                <li class="breadcrumb-item active">Voir un item</li>
            </ol>
            {{--            <form action="{{ action('Admin\ChallengesController@store') }}" method="POST" enctype="multipart/form-data">--}}
            {{--{{ csrf_field() }}--}}
            <div class="row">
                <div class="col-xl-7 col-lg-6 ">
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Voir un item
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} form-group-default required"
                                         aria-required="true">
                                        <label>Nom de l'item</label>
                                        <p>
                                            {{ $item->name }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }} form-group-default required"
                                         aria-required="true">
                                        <label>Description de l'item</label>
                                        <p>
                                            {{ $item->description }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="card card-white">
                                        <div class="card-header">
                                            <div class="card-title">
                                                Photo de l'item
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row clearfix">


                                                    <img src="{{ $item->getFile->file  }}" alt="" width="150px" height="150px">

                                                    <br>


                                                {{--@foreach($challenges->getImages as $img)--}}
                                                {{--<img src="{{ $img->file }}" alt="" width="150" height="150">--}}
                                                {{--<br>--}}
                                                {{--@endforeach--}}
                                                {{--<div class="form-group form-group-default">--}}
                                                {{--<label>Image mise en avant</label>--}}
                                                {{--<input type="file" name="file" class="form-control">--}}
                                                {{--@if($errors->count())--}}
                                                {{--<div class="help-block">--}}
                                                {{--<strong>1</strong>--}}
                                                {{--</div>--}}
                                                {{--@endif--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-default required"--}}
                                         {{--aria-required="true">--}}
                                        {{--<label>Image lié au clan</label>--}}
                                        {{--<div class="card card-default">--}}
                                            {{--<div class="card-header ">--}}
                                                {{--<div class="card-title">Banques d'images--}}
                                                {{--</div>--}}
                                                {{--<div class="card-controls">--}}
                                                    {{--<ul>--}}
                                                        {{--<li><a data-toggle="close" class="card-close" href="#"><i--}}
                                                                        {{--class="card-icon card-icon-close"></i></a>--}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="card-block">--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-group form-group-default required"--}}
                            {{--aria-required="true">--}}
                            {{--<label>Contrainte du challenge</label>--}}
                            {{--@foreach($challenges->getConstraintes as $i => $contraint)--}}
                            {{--<span>{{ $i + 1 . " : "}}{{ $contraint->name }}</span>--}}
                            {{--<br>--}}
                            {{--<br>--}}
                            {{--@endforeach--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-group form-group-default required"--}}
                            {{--aria-required="true">--}}
                            {{--<label>Nombre maximumn d'utilisateur participant au challenge</label>--}}
                            {{--<p>--}}
                            {{--{{ $challenges->nb_max_student }}--}}
                            {{--</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group form-group-default required"--}}
                                         {{--aria-required="true">--}}
                                        {{--<label>Date de début</label>--}}
                                        {{--<span>{{ $challenges->start_date }}</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group form-group-default required"--}}
                                         {{--aria-required="true">--}}
                                        {{--<label>Date de fin</label>--}}
                                        {{--<span>{{ $challenges->end_date }}</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    {{--<div class="card card-transparent">--}}
                    {{--<div class="card-header ">--}}
                    {{--<div class="card-title">--}}
                    {{--Validation--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="card-block">--}}
                    {{--<h3>--}}
                    {{--Ajouter un challenge--}}
                    {{--</h3>--}}
                    {{--<p>--}}
                    {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis debitis dicta,--}}
                    {{--eaque hic impedit ipsam non quia quo repellat sint? Exercitationem fuga illo minima--}}
                    {{--molestiae quaerat quidem repellat, similique veniam!--}}
                    {{--</p>--}}
                    {{--<br>--}}
                    {{--<button type="submit" class="btn btn-primary btn-cons">--}}
                    {{--Ajouter le challenge--}}
                    {{--</button>--}}
                    {{--</div>--}}
                </div>
            </div>
            {{--</form>--}}
        </div>
    </div>
@stop

