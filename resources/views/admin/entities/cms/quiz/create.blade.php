@extends('admin')

@section('css')
    <link href="{{ asset('pages-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
@stop
@section('js')
    <script src="{{ asset('pages-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            // DATEPICKER - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/index.html
            $.fn.datepicker.dates['fr'] = {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                daysMin: ["D", "L", "M", "M", "J", "V", "S"],
                months: months,
                monthsShort: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juill", "Août", "Sept", "Oct", "Nov", "Déc"],
                today: "Aujourd'hui",
                clear: "Clear",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 1
            };
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy', // FORMAT - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format
                language: 'fr',
                todayHight: true,
                startDate: new Date()
            });
            $('#time').timepicker({
                showMeridian: false
            }).on('show.timepicker', function (e) {
                var widget = $('.bootstrap-timepicker-widget');
                widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({});
            $('.js-example-basic-multiple').select2();
            $('.delete-resp').on('click', function () {
                $(this).parent().parent().remove();
                let loop = $('#cont').data('loop');
                let i = loop - 1;
                $('#cont').data('loop', i)
            });
            $(document).on('click', ".delete-resp", function () {
                $(this).parent().parent().remove();
                let loop = $('#cont').data('loop');
                let i = loop - 1;
                if (i === 2) {
                    $('#addQuestion').removeClass("d-none");
                }
                $('#cont').data('loop', i)
            });
            $('#addQuestion').on('click', function () {
                let loop = $('#cont').data('loop');
                let i = loop + 1;
                if (i === 3) {
                    $('#addQuestion').addClass("d-none");
                } else {
                    $('#addQuestion').removeClass("d-none");
                }
                if (i === 4) {
                    notie.alert(1, "Plus de réponse disponible", 2);
                    return true;
                }
                $('#cont').data('loop', i);
                console.log(i);
                $('#cont').append("<div class=\"resp\">\n" +
                    "                                                    <label for=\"\"\n" +
                    "                                                           class=\"my-2 w-100 d-flex align-items-center justify-content-between\">\n" +
                    "                                                        Réponse " + i + "\n" +
                    "                                                        <button class=\"btn btn-primary delete-resp\" type=\"button\">\n" +
                    "                                                            Supprimé la réponse\n" +
                    "                                                        </button>\n" +
                    "                                                    </label>\n" +
                    "                                                    <div class=\"form-group form-group-default {{ $errors->has('reponse') ? 'has-error' : '' }}required\">\n" +
                    "                                                        <label>Réponses</label>\n" +
                    "                                                        <input type=\"text\" class=\"form-control\" name=\"reponses[" + i + "][reponse]\"\n" +
                    "                                                               value=\"{{ old('reponse') }}\">\n" +
                    "                                                        @if ($errors->has('reponse'))\n" +
                    "                                                            <div class=\"help-block\">\n" +
                    "                                                                <strong>{{ $errors->first('reponse') }}</strong>\n" +
                    "                                                            </div>\n" +
                    "                                                        @endif\n" +
                    "                                                    </div>\n" +
                    "                                                    \n" +
                    "                                                    <div class=\"form-group {{ $errors->has('clan') ? 'has-error' : '' }} form-group-default required\"\n" +
                    "                                                         aria-required=\"true\">\n" +
                    "                                                        <label>Clan associé à la réponse </label>\n" +
                    "                                                        <select name=\"reponses[" + i + "][clans]\" id=\"\" class=\"form-control select2\">\n" +
                    "                                                            @foreach($clans as $clan)\n" +
                    "                                                                <option value=\"{{ $clan->id }}\">{{ $clan->title }}</option>\n" +
                    "                                                            @endforeach\n" +
                    "                                                        </select>\n" +
                    "                                                    </div>\n" +
                    "                                                </div>" +
                    "<div class=\"form-group form-group-default required\">\n" +
                    "                                                        <label>Poid de la réponse</label>\n" +
                    "                                                        <input type=\"number\" class=\"form-control\"\n" +
                    "                                                               name=\"reponses[" + i + "][less]\"\n" +
                    "                                                               value=\"{{ old('reponse') }}\">\n" +
                    "                                                    </div>")
            })
        });
    </script>
@stop


@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Tableau de bord</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.quiz.index') }}">Quiz</a>
                </li>
                <li class="breadcrumb-item active">Ajouter un quiz</li>
            </ol>
            <form action="{{ action('Admin\Cms\QuizController@store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xl-7 col-lg-6 ">
                        <div class="card card-white">
                            <div class="card-header">
                                <div class="card-title">
                                    Ajouter un quiz
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Titre de la question</label>
                                            <input type="text" class="form-control" name="question"
                                                   value="{{ old('question') }}"
                                                   placeholder="Titre de la question"
                                                   required>
                                            @if ($errors->has('question'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('question') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }} form-group-default required"
                                             aria-required="true">
                                            <label>Description de la question</label>
                                            <textarea type="text" class="editor form-control" name="content"
                                                      placeholder="Description de la page">{{ old('content') }}</textarea>
                                            @if ($errors->has('content'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('content') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group-default mb-2 required">
                                            <label>Zone réponse (3 réponses max)</label>
                                            <div class="is" id="cont" data-loop="1">
                                                <div class="resp">
                                                    <label for=""
                                                           class="my-2 w-100 d-flex align-items-center justify-content-between">
                                                        Réponse 1
                                                        <button class="btn btn-primary delete-resp" type="button">
                                                            Supprimé la réponse
                                                        </button>
                                                    </label>
                                                    <div class="form-group form-group-default {{ $errors->has('reponse') ? 'has-error' : '' }}required">
                                                        <label>Réponses</label>
                                                        <input type="text" class="form-control"
                                                               name="reponses[1][reponse]"
                                                               value="{{ old('reponse') }}">
                                                        @if ($errors->has('reponse'))
                                                            <div class="help-block">
                                                                <strong>{{ $errors->first('reponse') }}</strong>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    
                                                    <div class="form-group {{ $errors->has('clan') ? 'has-error' : '' }} form-group-default required"
                                                         aria-required="true">
                                                        <label>Clan associé à la réponse </label>
                                                        <select name="reponses[1][clans]" id=""
                                                                class="form-control select2">
                                                            @foreach($clans as $clan)
                                                                <option value="{{ $clan->id }}">{{ $clan->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="form-group form-group-default required">
                                                        <label>Poid de la réponse</label>
                                                        <input type="number" class="form-control"
                                                               name="reponses[1][less]"
                                                               value="{{ old('reponse') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button type="button" id="addQuestion" class="btn-primary btn mt-2 mb-3">
                                                Ajouter une
                                                réponse
                                            </button>
                                        </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6">
                        <div class="card card-white">
                            <div class="card-header">
                                <div class="card-title">
                                    Ajouter un quiz
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div
                                                class="form-group {{ $errors->has('file') ? 'has-error' : '' }} form-group-default required"
                                                aria-required="true">
                                            <label>Images de la question</label>
                                            <input type="file" class="form-control" name="file"
                                                   value="{{ old('file') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-transparent">
                            <div class="card-header ">
                                <div class="card-title">
                                    Validation
                                </div>
                            </div>
                            <div class="card-block">
                                <h3>
                                    Ajouter la question
                                </h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis debitis dicta,
                                    eaque hic impedit ipsam non quia quo repellat sint? Exercitationem fuga illo minima
                                    molestiae quaerat quidem repellat, similique veniam!
                                </p>
                                <br>
                                <button type="submit" class="btn btn-primary btn-cons">
                                    Ajouter une question
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

