@extends('admin')

@section('css')
    <link href="{{ asset('pages-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
@stop
@section('js')
    <script src="{{ asset('pages-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            // DATEPICKER - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/index.html
            $.fn.datepicker.dates['fr'] = {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                daysMin: ["D", "L", "M", "M", "J", "V", "S"],
                months: months,
                monthsShort: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juill", "Août", "Sept", "Oct", "Nov", "Déc"],
                today: "Aujourd'hui",
                clear: "Clear",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 1
            };
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy', // FORMAT - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format
                language: 'fr',
                todayHight: true,
                startDate: new Date()
            });
            $('#time').timepicker({
                showMeridian: false
            }).on('show.timepicker', function (e) {
                var widget = $('.bootstrap-timepicker-widget');
                widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({});
            $('.js-example-basic-multiple').select2();
            let val = $("input[name='team_choice']");
            // console.log(val);
            // let yes = $("input#yes");
            $('#without_team').removeClass("d-none");

            $("#yes").on('click', function () {
                $('#without_team input').val("");
                $('#with_team').removeClass("d-none");
                $('#without_team').addClass("d-none");
            });
            $("#no").on('click', function () {
                $('#with_team input').val("")
                $('#without_team').removeClass("d-none");
                $('#with_team').addClass("d-none");
            });

            let valOfLevelSelector = $('#levelSelector').val();
            console.log(valOfLevelSelector);
            let inputWin = $('input[name="point_win"]');
            let inputContrib = $('input[name="point_contribution"]');
            if (valOfLevelSelector !== null || valOfLevelSelector !== undefined) {
                $.ajax({
                    url: "/api/scores/" + valOfLevelSelector,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        inputContrib.val(result.data.point_contribution);
                        inputWin.val(result.data.point_win);
                    }
                });
            }
            $("#levelSelector").on('change', function () {
                valOfLevelSelector = $(this).val();
                $.ajax({
                    url: "/api/scores/" + valOfLevelSelector,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        inputContrib.val(result.data.point_contribution);
                        inputWin.val(result.data.point_win);
                    }
                });
            });
            // $("input[name='team_choice']").on('change', function () {
            //
            //     console.log(val.val());
            //     if ($(this).value() === ""){
            //
            //     }
            // });
        });
    </script>
@stop

@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Tableau de bord</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.challenges_state.index')}}">Challenge state</a>
                </li>
                <li class="breadcrumb-item active">Modifier le state du challenge</li>
            </ol>
            <form action="{{ action('Admin\ChallengesStateController@update', $c->id) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xl-7 col-lg-6 ">
                        <div class="card card-white">
                            {{--<div class="card-header">--}}
                            {{--<div class="card-title">--}}
                            {{--Ajouter un challenge--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} required"
                                             aria-required="true">
                                            <label>Nom du state du challenge</label>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{ $c->name }}"
                                                   placeholder="Titre du state du challenge"
                                                   required>
                                            @if ($errors->has('name'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card card-transparent">
                            <div class="card-header ">
                                <div class="card-title">
                                    Modifier ce state du challenge
                                </div>
                            </div>
                            <div class="card-block">
                                <h3>
                                    modifier le state du challenge
                                </h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis debitis dicta,
                                    eaque hic impedit ipsam non quia quo repellat sint? Exercitationem fuga illo minima
                                    molestiae quaerat quidem repellat, similique veniam!
                                </p>
                                <br>
                                <button type="submit" class="btn btn-primary btn-cons">
                                    Modifier le state du challenge
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6">
                        {{--<div class="card card-white">--}}
                        {{--<div class="card-header">--}}
                        {{--<div class="card-title">--}}
                        {{--Soft skill lié au challenge--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="card-block">--}}
                        {{--Soft Skill lié au challenge--}}
                        {{--<div class="row clearfix">--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="form-group form-group-default">--}}
                        {{--<label>--}}

                        {{--<select class="js-example-basic-single w-100" name="ssks[0][name]"--}}
                        {{--style="width: 100%">--}}
                        {{--@foreach($ssks as $ssk)--}}
                        {{--<option value="{{$ssk->slug}}">{{ $ssk->name }}</option>--}}

                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="form-group form-group-default">--}}
                        {{--<label>--}}
                        {{--Score du Soft Skill--}}
                        {{--<input type="text" class="form-control"--}}
                        {{--value="{{ $challenge->seo_description') }}" name="ssks[0][score]"--}}
                        {{--placeholder="Score du soft Skill">--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row clearfix">--}}
                        {{--<div class="col-md-12">--}}
                        {{--<button type="submit" class="btn btn-primary btn-cons">--}}
                        {{--//TODO ADD ROW OF SOFT SKILL WHEN PRESSED HERE--}}
                        {{--Ajouter un soft skill--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}



                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

