@extends('admin')

@section('css')
    <link href="{{ asset('pages-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
@stop
@section('js')
    <script src="{{ asset('pages-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            // DATEPICKER - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/index.html
            $.fn.datepicker.dates['fr'] = {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                daysMin: ["D", "L", "M", "M", "J", "V", "S"],
                months: months,
                monthsShort: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juill", "Août", "Sept", "Oct", "Nov", "Déc"],
                today: "Aujourd'hui",
                clear: "Clear",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 1
            };
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy', // FORMAT - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format
                language: 'fr',
                todayHight: true
            });
            $('#time').timepicker({
                showMeridian: false
            }).on('show.timepicker', function (e) {
                var widget = $('.bootstrap-timepicker-widget');
                widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({});
            $('.js-example-basic-multiple').select2();
        });
    </script>
@stop

@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Tableau de bord</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.challenges.index_challenges_valides') }}">Challenge</a>
                </li>
                <li class="breadcrumb-item active">Voir un challenge</li>
            </ol>
            {{--            <form action="{{ action('Admin\ChallengesController@store') }}" method="POST" enctype="multipart/form-data">--}}
            {{--{{ csrf_field() }}--}}
            <div class="row">
                <div class="col-xl-7 col-lg-6 ">
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Voir un challenge
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div
                                            class="form-group {{ $errors->has('title') ? 'has-error' : '' }} form-group-default required"
                                            aria-required="true">
                                        <label>Titre du challenge</label>
                                        <p>
                                            {{ $challenges->title }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div
                                            class="form-group {{ $errors->has('description') ? 'has-error' : '' }} form-group-default required"
                                            aria-required="true">
                                        <label>Description du challenge</label>
                                        <p>
                                            {{ $challenges->description }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div
                                        class="form-group {{ $errors->has('objectifs') ? 'has-error' : '' }} form-group-default required"
                                        aria-required="true">
                                        <label>Objectifs du challenge</label>
                                        <p>
                                            {{ $challenges->objectifs }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div
                                        class="form-group {{ $errors->has('reglementation') ? 'has-error' : '' }} form-group-default required"
                                        aria-required="true">
                                        <label>Reglementation du challenge</label>
                                        <p>
                                            {{ $challenges->reglementation }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div
                                        class="form-group {{ $errors->has('rendu') ? 'has-error' : '' }} form-group-default required"
                                        aria-required="true">
                                        <label>Rendu du challenge</label>
                                        <p>
                                            {{ $challenges->rendu }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default required"
                                         aria-required="true">
                                        <label>Niveau de dificulté du challenge</label>
                                        <p>
                                            {{ $challenges->getLevel ? $challenges->getLevel->name : ''}}
                                        </p>
                                        <div class="d-flex flex-column align-items-start justify-content-center">
                                            <div class="">
                                                <label class="font-weight-bold">Point gagné si réussite du
                                                                                challenge</label>
                                                <p>
                                                    {{ $challenges->point_win }}
                                                </p>
                                            </div>
                                            
                                            <div class="">
                                                <label class="font-weight-bold">Point gagné si contribution au
                                                                                challenge</label>
                                                <p>
                                                    {{ $challenges->point_contribution }}
                                                </p>
                                            </div>
                                            
                                            <div class="">
                                                <label class="font-weight-bold">Ce challenge est validé ?</label>
                                                <p>
                                                    @if($challenges->validated === 1)
                                                        oui
                                                    @else
                                                        non
                                                    @endif
                                                </p>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default required"
                                         aria-required="true">
                                        <label>Etapes du challenge</label>
                                        @foreach($challenges->array_states as $i => $state)
                                            <span>{{ $i + 1 . " . "}}{{ $state }}</span>
                                            <br>
                                            <br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required"
                                         aria-required="true">
                                        <label>Nombre maximumn d'utilisateur participant au challenge</label>
                                        <p>
                                            {{ $challenges->nb_max_student }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required"
                                         aria-required="true">
                                        <label>Nombre maximumn d'utilisateur part équipe</label>
                                        <p>
                                            {{ $challenges->nb_max_student_by_team }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required"
                                         aria-required="true">
                                        <label>Date de début</label>
                                        <span>{{ $challenges->start_date }}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default required"
                                         aria-required="true">
                                        <label>Date de fin</label>
                                        <span>{{ $challenges->end_date }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                    {{--<div class="card card-white">--}}
                    {{--<div class="card-header">--}}
                    {{--<div class="card-title">--}}
                    {{--Soft skill lié au challenge--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="card-block">--}}
                    {{--Soft Skill lié au challenge--}}
                    {{--<div class="row clearfix">--}}
                    {{--<div class="col-md-6">--}}
                    {{--<div class="form-group form-group-default">--}}
                    {{--<label>--}}
                    
                    {{--<select class="js-example-basic-single w-100" name="ssks[0][name]"--}}
                    {{--style="width: 100%">--}}
                    {{--@foreach($challenges->getSsks as $ssk)--}}
                    {{--<span>{{ $ssk->name }}</span>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--</label>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                    {{--<div class="form-group form-group-default">--}}
                    {{--Score--}}
                    {{--<label>--}}
                    {{--</label>--}}
                    {{--@foreach($challenges->getSsks as $ssk)--}}
                    {{--@foreach($ssk->getScore as $sc)--}}
                    {{--@if($sc->fk_challenge_id === $challenges->id)--}}
                    {{--<span class="font-weight-bold">{{ $sc->score }}</span>--}}
                    {{--@endif--}}
                    {{--@endforeach--}}
                    {{--@endforeach--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Photo de référence du challenge
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                @foreach($challenges->getImages as $img)
                                    <img src="{{ $img->file }}" alt="" width="150" height="150">
                                    <br>
                                @endforeach
                                {{--<div class="form-group form-group-default">--}}
                                {{--<label>Image mise en avant</label>--}}
                                {{--<input type="file" name="file" class="form-control">--}}
                                {{--@if($errors->count())--}}
                                {{--<div class="help-block">--}}
                                {{--<strong>1</strong>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Choissisez les clans qui peuvent participer
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                <div
                                        class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }} form-group-default">
                                    <label>
                                        Clans accepté
                                    </label>
                                    <p>
                                        @foreach($challenges->getClans as $clan)
                                            {{ $clan->title }}
                                            <br>
                                        @endforeach
                                    </p>
                                </div>
                                {{--<div class="form-group form-group-default">--}}
                                {{--<label>Image mise en avant</label>--}}
                                {{--<input type="file" name="file" class="form-control">--}}
                                {{--@if($errors->count())--}}
                                {{--<div class="help-block">--}}
                                {{--<strong>1</strong>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="card card-white">
                        <div class="card-header">
                            <div class="card-title">
                                Les utilisateurs inscrit a ce challenge
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                <div
                                        class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }} form-group-default">
                                    <p>
                                        @if($challenges->getUsers->isEmpty())
                                            Aucun utilisateur inscrit
                                        @else
                                            @foreach($challenges->getUsers as $user)
                                                {{ $user->first_name }}
                                                <br>
                                            @endforeach
                                        @endif
                                    </p>
                                </div>
                                {{--<div class="form-group form-group-default">--}}
                                {{--<label>Image mise en avant</label>--}}
                                {{--<input type="file" name="file" class="form-control">--}}
                                {{--@if($errors->count())--}}
                                {{--<div class="help-block">--}}
                                {{--<strong>1</strong>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>


                    <div class="card card-white">
                        @if($challenges->type === "single")
                        <div class="card-header">
                            <div class="card-title">
                                Liste des utilisateurs ayant remporté le challenge
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row clearfix">
                                <div
                                    class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }} form-group-default">
                                    <p>
                                        @if($challenges->getWinners->isEmpty())
                                            Aucun utilisateur n'a remporté ce challenge
                                        @else
                                            @foreach($challenges->getWinners as $winner)
                                                {{ $winner->first_name }}
                                                <br>
                                            @endforeach
                                        @endif
                                    </p>
                                </div>
                                {{--<div class="form-group form-group-default">--}}
                                {{--<label>Image mise en avant</label>--}}
                                {{--<input type="file" name="file" class="form-control">--}}
                                {{--@if($errors->count())--}}
                                {{--<div class="help-block">--}}
                                {{--<strong>1</strong>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                            </div>
                        </div>

                            @else($challenges->type === "team")
                            <div class="card-header">
                                <div class="card-title">
                                    Liste des équipes ayant remporté le challenge
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div
                                        class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }} form-group-default">
                                        <p>
                                            @if($challenges->getWinners->isEmpty())
                                                Aucune équipe n'a remporté ce challenge
                                            @else
                                                @foreach($challenges->getWinners as $winner)
                                                    {{ $winner->name }}
                                                    <br>
                                                @endforeach
                                            @endif
                                        </p>
                                    </div>
                                    {{--<div class="form-group form-group-default">--}}
                                    {{--<label>Image mise en avant</label>--}}
                                    {{--<input type="file" name="file" class="form-control">--}}
                                    {{--@if($errors->count())--}}
                                    {{--<div class="help-block">--}}
                                    {{--<strong>1</strong>--}}
                                    {{--</div>--}}
                                    {{--@endif--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            @endif
                    </div>


                </div>
            </div>
            {{--</form>--}}
            <div class="row">
                <div class="card">
                    <div class="card-header ">
                        <div class="pull-right">
                            <div class="col-xs-12">
                                <input type="text" id="search-table" class="form-control pull-right"
                                       placeholder="Search">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card-block">
                        @if($challenges->type === "single")
                            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                                <thead>
                                <tr>
                                    <th style="width:10%;">#</th>

                                    <th>Utilisateurs</th>

                                    <th style="width:20%">Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($challenges->getUsers AS $key => $item)
                                    <tr>
                                        <td style="width:10%;" class="v-align-middle">
                                            {{ $key+1 }}
                                        </td>

                                        <td class="v-align-middle semi-bold">
                                            {{ $item->first_name . ' ' . $item->last_name }}
                                        </td>


                                        <td style="width:20%" class="v-align-middle d-flex">
                                            <a href="{{action('Admin\ChallengesController@setWinner', [$challenges->slug, $item->id])}}"
                                               class="btn btn btn-primary mr-2">
                                                Set this user as winner
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endif

                            @if($challenges->type === "team")
                                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                                    <thead>
                                    <tr>
                                        <th style="width:10%;">#</th>

                                        <th>Equipes</th>

                                        <th style="width:20%">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>



                                    @foreach($challenges->getTeams AS $key => $item)
                                        <tr>
                                            <td style="width:10%;" class="v-align-middle">
                                                {{ $key+1 }}
                                            </td>

                                            <td class="v-align-middle semi-bold">
                                                {{ $item->name}}
                                            </td>


                                            <td style="width:20%" class="v-align-middle d-flex">
                                                <a href="{{action('Admin\ChallengesController@setWinner', [$challenges->slug, $item->id])}}"
                                                   class="btn btn btn-primary mr-2">
                                                    Set this team as winner
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                        {{--<div class="card card-transparent">--}}
                        {{--<div class="card-header ">--}}
                        {{--<div class="card-title">--}}
                        {{--Validation--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="card-block">--}}
                        {{--<h3>--}}
                        {{--Ajouter un challenge--}}
                        {{--</h3>--}}
                        {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis debitis dicta,--}}
                        {{--eaque hic impedit ipsam non quia quo repellat sint? Exercitationem fuga illo minima--}}
                        {{--molestiae quaerat quidem repellat, similique veniam!--}}
                        {{--</p>--}}
                        {{--<br>--}}
                        {{--<button type="submit" class="btn btn-primary btn-cons">--}}
                        {{--Ajouter le challenge--}}
                        {{--</button>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

