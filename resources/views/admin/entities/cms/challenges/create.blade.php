@extends('admin')

@section('css')
    <link href="{{ asset('pages-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link href="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet"
          type="text/css"
          media="screen">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-tag/bootstrap-tagsinput.css">
    ¬
@stop
@section('js')
    <script src="{{ asset('pages-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('pages-assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
    <script src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            // DATEPICKER - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/index.html
            $.fn.datepicker.dates['fr'] = {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                daysMin: ["D", "L", "M", "M", "J", "V", "S"],
                months: months,
                monthsShort: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juill", "Août", "Sept", "Oct", "Nov", "Déc"],
                today: "Aujourd'hui",
                clear: "Clear",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 1
            };
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy', // FORMAT - Documentation : http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format
                language: 'fr',
                todayHight: true,
                startDate: new Date()
            });
            $('#time').timepicker({
                showMeridian: false
            }).on('show.timepicker', function (e) {
                var widget = $('.bootstrap-timepicker-widget');
                widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({});
            $('.js-example-basic-multiple').select2();
            let val = $("input[name='team_choice']");
            // console.log(val);
            // let yes = $("input#yes");
            $('#without_team').removeClass("d-none");

            $("#yes").on('click', function () {
                $('#without_team input').val("");
                $('#with_team').removeClass("d-none");
                $('#without_team').addClass("d-none");
                $('#tem').addClass("d-none");
            });
            $("#no").on('click', function () {
                $('#with_team input').val("");
                $('#tem').removeClass("d-none");
                $('#without_team').removeClass("d-none");
                $('#with_team').addClass("d-none");
            });

            let valOfLevelSelector = $('#levelSelector').val();
            console.log(valOfLevelSelector);
            let inputWin = $('input[name="point_win"]');
            let inputContrib = $('input[name="point_contribution"]');
            if (valOfLevelSelector !== null || valOfLevelSelector !== undefined) {
                $.ajax({
                    url: "/api/scores/" + valOfLevelSelector,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        inputContrib.val(result.data.point_contribution);
                        inputWin.val(result.data.point_win);
                    }
                });
            }
            $("#levelSelector").on('change', function () {
                valOfLevelSelector = $(this).val();
                $.ajax({
                    url: "/api/scores/" + valOfLevelSelector,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        inputContrib.val(result.data.point_contribution);
                        inputWin.val(result.data.point_win);
                    }
                });
            });
            // $("input[name='team_choice']").on('change', function () {
            //
            //     console.log(val.val());
            //     if ($(this).value() === ""){
            //
            //     }
            // });
            $('#tagsinput').tagsinput({
                typeahead: {
                    source: ['Commencement', 'Rendu']
                }
            });
        });
    </script>
@stop

@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid container-fixed-lg">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ action('Admin\DashboardController@dashboard') }}">Tableau de bord</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.challenges.index_challenges_valides') }}">Challenge</a>
                </li>
                <li class="breadcrumb-item active">Ajouter un challenge</li>
            </ol>
            <form action="{{ action('Admin\ChallengesController@store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xl-7 col-lg-6 ">
                        <div class="card card-white">
                            {{--<div class="card-header">--}}
                            {{--<div class="card-title">--}}
                            {{--Ajouter un challenge--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} required"
                                             aria-required="true">
                                            <label>Titre du challenge</label>
                                            <input type="text" class="form-control" name="title"
                                                   value="{{ old('title') }}"
                                                   placeholder="Titre de la page"
                                                   required>
                                            @if ($errors->has('title'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }} required"
                                             aria-required="true">
                                            <label>Description du challenge</label>
                                            <textarea type="text" class="editor form-control" name="description"
                                                      placeholder="Description de la page">{{ old('description') }}</textarea>
                                            @if ($errors->has('description'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('objectifs') ? 'has-error' : '' }} required"
                                             aria-required="true">
                                            <label>Objecifs du challenge</label>
                                            <textarea type="text" class="editor form-control" name="objectifs"
                                                      placeholder="Objectifs du challenge">{{ old('objectifs') }}</textarea>
                                            @if ($errors->has('objectifs'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('objectifs') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('reglementation') ? 'has-error' : '' }} required"
                                             aria-required="true">
                                            <label>Reglementation du challenge</label>
                                            <textarea type="text" class="editor form-control" name="reglementation"
                                                      placeholder="Reglementation du challenge">{{ old('reglementation') }}</textarea>
                                            @if ($errors->has('reglementation'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('reglementation') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('rendu') ? 'has-error' : '' }} required"
                                             aria-required="true">
                                            <label>Rendu du challenge</label>
                                            <textarea type="text" class="editor form-control" name="rendu"
                                                      placeholder="Rendu de la page">{{ old('rendu') }}</textarea>
                                            @if ($errors->has('rendu'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('rendu') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group required"
                                             aria-required="true">
                                            <label>Niveau de difficulté du challenge</label>
                                            <select class="js-example-basic-single w-100" name="level"
                                                    style="width: 100%" id="levelSelector">
                                                @foreach($levels as $level)
                                                    <option value="{{ $level->slug }}">{{ $level->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="row mx-0 mt-2">
                                                <div class="col-md-6">
                                                    <label for="">Points gagnés si le challenge est gagné</label>
                                                    <input value="" name="point_win"
                                                           class="form-control"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="">Points gagnés pour participation au challenge</label>
                                                    <input value=""
                                                           name="point_contribution"
                                                           class="form-control"/>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default required"
                                             aria-required="true">
                                            <label>Date de début</label>
                                            <div class=" datepicker input-group date">
                                                <input type="text" class="form-control" name="begin_date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default required"
                                             aria-required="true">
                                            <label>Date de fin</label>
                                            <div class="datepicker input-group date">
                                                <input type="text" class="form-control" name="end_date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-transparent">
                            <div class="card-header ">
                                <div class="card-title">
                                    Validation
                                </div>
                            </div>
                            <div class="card-block">
                                <h3>
                                    Ajouter un challenge
                                </h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis debitis dicta,
                                    eaque hic impedit ipsam non quia quo repellat sint? Exercitationem fuga illo minima
                                    molestiae quaerat quidem repellat, similique veniam!
                                </p>
                                <br>
                                <button type="submit" class="btn btn-primary btn-cons">
                                    Ajouter le challenge
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6">
                        {{--<div class="card card-white">--}}
                        {{--<div class="card-header">--}}
                        {{--<div class="card-title">--}}
                        {{--Soft skill lié au challenge--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="card-block">--}}
                        {{--Soft Skill lié au challenge--}}
                        {{--<div class="row clearfix">--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="form-group form-group-default">--}}
                        {{--<label>--}}
                        
                        {{--<select class="js-example-basic-single w-100" name="ssks[0][name]"--}}
                        {{--style="width: 100%">--}}
                        {{--@foreach($ssks as $ssk)--}}
                        {{--<option value="{{$ssk->slug}}">{{ $ssk->name }}</option>--}}
                        
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="form-group form-group-default">--}}
                        {{--<label>--}}
                        {{--Score du Soft Skill--}}
                        {{--<input type="text" class="form-control"--}}
                        {{--value="{{ old('seo_description') }}" name="ssks[0][score]"--}}
                        {{--placeholder="Score du soft Skill">--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row clearfix">--}}
                        {{--<div class="col-md-12">--}}
                        {{--<button type="submit" class="btn btn-primary btn-cons">--}}
                        {{--//TODO ADD ROW OF SOFT SKILL WHEN PRESSED HERE--}}
                        {{--Ajouter un soft skill--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="card card-white">
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label>Ce challenge est un challenge d'équipe ?</label>
                                            <div class="radio radio-success">
                                                <input type="radio" checked="checked" value="no" name="team_choice"
                                                       id="no">
                                                <label for="no">Non</label>
                                                <input type="radio" value="yes" data-value="yes" name="team_choice"
                                                       id="yes">
                                                <label for="yes">Oui</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block d-none" id="with_team">
                                <div class="row clearfix ">
                                    <div class="col-md-12">
                                        <div class="form-group form-group-default required"
                                             aria-required="true">
                                            <label>Nombre maximumn d'équipe pouvant participer au challenge</label>
                                            <input type="number" class="form-control" name="max_user[]"
                                                   value="{{ old('max') }}"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block d-none" id="without_team">
                                <div class="row clearfix ">
                                    <div class="col-md-12">
                                        <div class="form-group form-group-default required"
                                             aria-required="true">
                                            <label>Nombre maximumn d'utilisateur participant au challenge</label>
                                            <input type="number" class="form-control" name="max_user[]"
                                                   value="{{ old('max') }}"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-white">
                            <div class="card-header">
                                <div class="card-title">
                                    Photo de référence du challenge
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix">
                                    <div class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }} form-group-default">
                                        <label>
                                            {{--Photo de référence--}}
                                        </label>
                                        <input type="file" class="form-control" name="files[]" value=""
                                               multiple="multiple">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card card-white">
                            <div class="card-header">
                                <div class="card-title">
                                    Choissisez les clans qui peuvent participer
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix mx-0">
                                    <div class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }}">
                                        <label>
                                            Clans accepté
                                        </label>
                                        <select class="js-example-basic-multiple w-100" name="clans[]"
                                                style="width: 100%" multiple="multiple">
                                            @foreach($clans as $clan)
                                                <option value="{{ $clan->slug }}">{{ $clan->title }}</option>
                                            @endforeach
                                        </select>
                                        <label for="">
                                            Accepté tout les clans
                                            <input type="radio" value="yes" name="all_clans">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card card-white">
                            <div class="card-header">
                                <div class="card-title">
                                    Choissisez les étapes de votre challenge
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="row clearfix mx-0">
                                    <div class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }}">
                                        <input id="#tagsinput" name="states" type="text" value="Commencement,Rendu"
                                               data-role="tagsinput"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

