@extends('admin')

@section('js')
    <script>
        $(function () {

            console.log("okdzazaaz");
            $("#selectCreateType").on("change", function () {

                console.log("val", $(this).val());
                if ($(this).val() === "text") {
                    $('#textSelected').removeClass("d-none");
                    $('#imageSelected').addClass("d-none");
                } else if ($(this).val() === "image") {
                    $('#imageSelected').removeClass("d-none");
                    $('#textSelected').addClass("d-none");
                }
            })
        })
    </script>
@endsection

@section('content')
    <style>
        .page-container .page-content-wrapper .content {
            padding-bottom: 0;
        }
        
        .copyright {
            display: none;
        }
        
        .no-uppercase {
            text-transform: none !important;
        }
    </style>
    <div class="content full-height">
        <nav class="secondary-sidebar">
            <div class="m-b-30 m-l-30 m-r-30 hidden-sm-down">
                <button class="btn btn-complete btn-block btn-compose no-uppercase" data-toggle="modal"
                        data-target="#createBlockModal">
                    Ajouter un bloc
                </button>
            </div>
            <p class="menu-title">Tous les dossiers de cette page</p>
            <ul class="main-menu">
                
                <li>
                    <a href="#" data-toggle="modal" data-target="#createFolderModal">
                        <span class="title">
                            Ajouter un nouveau dossier
                        </span>
                    </a>
                </li>
    
                <li class="" id="folders">
                    {{-- Avoid waiting --}}
                    @foreach($page->getCategories AS $category)
                        <a href="#" class="singleFolder"
                           data-id="{{ $category->id }}">
                    <span class="title">
                    <i class="pg-folder"></i>
                        {{ $category->name }}
                    </span>
                            <span class="badge pull-right">5</span>
                        </a>
                    @endforeach
                </li>
            </ul>
            {{--<p class="menu-title m-t-20 all-caps">Les autres pages</p>--}}
            {{--<ul class="sub-menu no-padding">--}}
            {{--@foreach($pages AS $children)--}}
            {{--<li>--}}
            {{--<a href="{{ action('Admin\Cms\BlocksController@index', $children->id) }}">--}}
            {{--<span class="title">{{ $children->title }}</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
        </nav>
        
        <div class="inner-content full-height">
            <div class="split-view">
                
                <div style="" id="list" class="split-list">
                    <div data-email="list" class="boreded no-top-border list-view">
                        <h2 class="list-view-fake-header">
                            Dossier: <strong id="currentFolder">{{$category->name}}</strong>
                            {{-- TODO --}}
                        </h2>
                        <div style="position: absolute; right:10px; z-index:999">
                            <a href="{{ action('Admin\Cms\BlocksController@show', $page->id) }}">
                                <i class="fa fa-times"></i>
                            </a>
                            </form>
                        </div>
                        @include('admin.entities.cms.pages.partials.listBlock', [$blocks = $category->getBlocks])
                    </div>
                </div>
                
                <div id="details" class="split-details" style="">
                    <div class="email-content-wrapper">
                        <div class="email-content">
                            <div class="email-content-header">
                                <div class="thumbnail-wrapper d48 circular">
                                </div>
                                <div class="sender inline m-l-10">
                                    
                                    <p class="datetime no-margin">Dernière mise à jour : {{ $block->created_at }}
                                    </p>
                                </div>
                                <form action="{{ action('Admin\Cms\BlocksController@updateBlock', $block->id)}}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="d-flex flex-column mt-3">
                                        <div class="form-group form-group-default">
                                            <label for="">La clé</label>
                                            <input class="form-control" type="text" name="key"
                                                   value="{{ $block->key }}">
                                        </div>
                                        @if($block->type === "text")
                                        <div class="form-group form-group-default">
                                            <label for="">La valeur</label>
                                            <textarea name="value" id="" class="form-control" cols="30"
                                                      rows="10">{!! $block->value  !!}</textarea>
                                        </div>
                                        @else
                                            <div class="form-group form-group-default">
                                                <label for="">La valeur</label>
                                                <img src="{{ $block->value }}" alt="" class="w-100 h-100">
                                            </div>
                                            <div class="mt-2 ">
                                                <div class="form-group form-group-default">
                                                    <label>Votre nouvelle image</label>
                                                    <input type="file" name="image">
                                                </div>
                                            </div>
                                        
                                        @endif
                                        <div class="">
                                            <button type="submit" class="btn btn-success">Modifier le block</button>
                                            
                                        </div>
                                    </div>
                                </form>
                                <form action="{{action('Admin\Cms\BlocksController@deleteBlock', $block->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="slug" value="{{ $category->slug }}">
                                    <input type="hidden" name="page" value="{{ $page->id }}">
                                    <button type="submit" class="ml-5 mt-2 btn btn-danger">Supprimer le block</button>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.entities.cms.pages.partials.modal')
@stop