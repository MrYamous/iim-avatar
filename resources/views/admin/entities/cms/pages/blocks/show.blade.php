@extends('admin')

@section('js')
    <script>
        $(function () {

            console.log("okdzazaaz");
            $("#selectCreateType").on("change", function () {

                console.log("val", $(this).val());
                if ($(this).val() === "text") {
                    $('#textSelected').removeClass("d-none");
                    $('#imageSelected').addClass("d-none");
                } else if ($(this).val() === "image") {
                    $('#imageSelected').removeClass("d-none");
                    $('#textSelected').addClass("d-none");
                }
            })
        })
    </script>
@endsection

@section('content')
    <style>
        .page-container .page-content-wrapper .content {
            padding-bottom: 0;
        }
        
        .copyright {
            display: none;
        }
        
        .no-uppercase {
            text-transform: none !important;
        }
    </style>
    <div class="content full-height">
        <nav class="secondary-sidebar">
            <div class="m-b-30 m-l-30 m-r-30 hidden-sm-down">
                <button class="btn btn-complete btn-block btn-compose no-uppercase" data-toggle="modal"
                        data-target="#createBlockModal">
                    Ajouter un bloc
                </button>
            </div>
            <p class="menu-title">Tous les dossiers de cette page</p>
            <ul class="main-menu">
                
                <li>
                    <a href="#" data-toggle="modal" data-target="#createFolderModal">
                        <span class="title">
                            Ajouter un nouveau dossier
                        </span>
                    </a>
                </li>
                
                <li class="" id="folders">
                    {{-- Avoid waiting --}}
                    @foreach($page->getCategories AS $category)
                        <a href="{{ action('Admin\Cms\BlocksController@showBlocks', [$page->id, $category->slug]) }}" class="singleFolder"
                           data-id="{{ $category->id }}">
                    <span class="title">
                    <i class="pg-folder"></i>
                        {{ $category->name }}
                    </span>
                            <span class="badge pull-right">5</span>
                        </a>
                    @endforeach
                </li>
            </ul>
            {{--<p class="menu-title m-t-20 all-caps">Les autres pages</p>--}}
            {{--<ul class="sub-menu no-padding">--}}
                {{--@foreach($pages AS $children)--}}
                {{--<li>--}}
                {{--<a href="{{ action('Admin\Cms\BlocksController@index', $children->id) }}">--}}
                {{--<span class="title">{{ $children->title }}</span>--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        </nav>
        
        <div class="inner-content full-height">
            <div class="split-view">
                
                <div style="display: none;" id="list" class="split-list">
                    <div data-email="list" class="boreded no-top-border list-view">
                        <h2 class="list-view-fake-header">
                            Dossier: <strong id="currentFolder">{{ $page->title }}</strong>
                            {{-- TODO --}}
                        </h2>
                        <div style="position: absolute; right:10px; z-index:999">
                            <form action="#">
                                {{ csrf_field() }}
                                <input type="hidden" name="folder" required>
                                <button>
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </div>
                        <div class="list-view-wrapper">
                            <div class="list-view-group-container">
                                <ul class="no-padding" id="blocks">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="details" class="split-details" style="display: none;">
                    <div class="email-content-wrapper">
                        <div class="actions-wrapper menuclipper bg-master-lightest">
                            <ul class="actions menuclipper-menu no-margin p-l-20">
                                <li class="no-padding">
                                    <a href="#" class="text-danger">
                                        Supprimer ce bloc
                                    </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="email-content">
                            <div class="email-content-header">
                                <div class="thumbnail-wrapper d48 circular">
                                    <i class="pg-text_style"></i>
                                </div>
                                <div class="sender inline m-l-10">
                                    <p class="name no-margin bold" id="currentBlockKey"></p>
                                    <p class="datetime no-margin">Dernière mise à jour :
                                        <span id="currentBlockTime"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="email-content-body m-t-20">
                            </div>
                            <div class="b-a b-grey m-t-30">
                                {{--<form action="{{ action('Admin\Cms\BlocksController@updateBlock', $page->id) }}"--}}
                                {{--id="currentBlockText" style="display: none" method="post">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<input type="hidden" name="block" id="currentBlockIdValue">--}}
                                {{--<textarea class="form-control" placeholder="Valeur" name="value"--}}
                                {{--id="currentBlockTextValue"></textarea>--}}
                                {{--</form>--}}
                                {{--<form action="{{ action('Admin\Cms\BlocksController@updateBlock', $page->id) }}"--}}
                                {{--id="currentBlockImage" style="display: none" method="post"--}}
                                {{--enctype="multipart/form-data">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<input type="hidden" name="block" id="currentBlockIdValueForImage">--}}
                                {{--<input type="file" name="image" id="currentBlockImageValue">--}}
                                {{--<button type="submit" class="btn btn-primary">Envoyer</button>--}}
                                {{--</form>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.entities.cms.pages.partials.modal')
@stop