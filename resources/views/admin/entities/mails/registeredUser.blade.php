<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
<style>
    body, body *:not(html):not(style):not(br):not(tr):not(code) {
        font-family: 'Raleway', sans-serif;
        box-sizing: border-box;
    }

    @media only screen and (max-width: 600px) {
        .inner-body {
            width: 100% !important;
        }

        .footer {
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }
</style>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="content" width="100%" cellpadding="0" cellspacing="0">
                <tr style="text-align: center;">
                    <td>
                        <img src="{{ asset("/images/logo.png") }}"
                             alt="" width="400px" style="margin-top: 40px">
                        <div style="margin-top: 30px;"></div>
                    </td>
                </tr>
                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0">
                        <table class="inner-body" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-cell" style="text-align:left;color: #324e6d;">
                                    <div style="font-size: 16px; padding-top:30px;">
                                        Bienvenue {{ $request->first_name }}  {{ $request->last_name}},
                                        <br>
                                        <br>
                                        Un compte a été crée pour vous par un administrateur de Valkyria !
                                        <br>
                                        <br>
                                        <br>
                                        Vous devez definir votre mot de passe en cliquant <a
                                                href="{{ action('App\StaticsController@setPassword', $user->remember_token) }}">ici</a>
                                        <br>
                                        <br>
                                        @if($user->admin !== 0)
                                            Votre compte est un compte administrateurs, vous pourrez vous connecter à la plateforme
                                            une fois votre mot de passe defini via ce
                                            <a href="{{ url('/admin') }}">lien</a>
                                        @endif

                                    </div>
                                    <div style="margin-top: 40px;text-align: center;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>