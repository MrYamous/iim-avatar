<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer("fk_file_id")->references('id')->on('files')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('challenges', function (Blueprint $table) {
            $table->integer("fk_owner_id")->nullable()->references('id')->on('users');
            $table->integer("nb_max_student")->nullable();
            $table->integer("fk_type_id")->nullable()->references('id')->on('type_challenges');
            $table->integer('honneur_max')->nullable();
//            $table->dropColumn("teacher_id");
        });

        Schema::create("cms_levels", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name")->nullable();
            $table->string("slug")->nullable();
            $table->string("description")->nullable();
            $table->timestamps();
        });

        //***Create Constrainte of challenge
        Schema::create("cms_constraint", function (Blueprint $table) {
            $table->increments("id");
            $table->longText("name")->nullable();
            $table->string("slug")->nullable();
            $table->longText("description")->nullable();
            $table->timestamps();
        });

        //***Create Type of challenge
        Schema::create("type_challenges", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name")->nullable();
            $table->string("slug")->nullable();
            $table->string("description")->nullable();
            $table->timestamps();
        });

        //***Create Soft Skill
        Schema::create("cms_ssk", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name")->nullable();
            $table->string("slug")->nullable();
            $table->string("description")->nullable();
            $table->timestamps();
        });

        //***Create Challenge has X Soft Skill
        Schema::create("challenges_has_ssk", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->nullable();
            $table->integer("fk_ssk_id")->nullable();
            $table->timestamps();
        });

        //***Create Challenge has X Constrainte
        Schema::create("challenges_has_constraint", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->nullable();
            $table->integer("fk_constraint_id")->nullable();
            $table->timestamps();
        });

        //***Create Challenge has X clan
        Schema::create("challenges_has_clans", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->nullable();
            $table->integer("fk_clan_id")->nullable();
            $table->timestamps();
        });

        //***Create Challenge has X Files
        Schema::create("challenges_has_files", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->nullable();
            $table->integer("fk_file_id")->nullable();
            $table->timestamps();
        });

        //***Create Challenge has X User
        Schema::create("challenges_has_users", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->nullable();
            $table->integer("fk_user_id")->nullable();
            $table->timestamps();
        });
    
        //***Create Challenge has X Winner
        Schema::create('challenges_has_winners', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->nullable();
            $table->integer("fk_user_id")->nullable();
        });

        Schema::create("files", function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('content');
            $table->string('type');
            $table->integer('fk_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("cms_levels");
        Schema::dropIfExists("cms_cours");
        Schema::dropIfExists("files");

        Schema::dropIfExists("users_has_challenges");
        Schema::dropIfExists("challenges_has_files");
        Schema::dropIfExists("challenges_has_clans");
        Schema::dropIfExists("challenges_has_constraint");
        Schema::dropIfExists("challenges_has_ssk");
        Schema::dropIfExists("cms_ssk");
        Schema::dropIfExists("type_challenges");
        Schema::dropIfExists("cms_constraint");
        Schema::dropIfExists("cms_levels");
        Schema::dropIfExists("challenges_has_winners");
        Schema::dropIfExists("challenges_has_users");
    }
}
