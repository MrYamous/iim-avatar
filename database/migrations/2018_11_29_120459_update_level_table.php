<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_challenges', function (Blueprint $table) {
            $table->integer("point_win")->nullable();
            $table->integer("point_contribution")->nullable();
        });

        Schema::table("challenges", function (Blueprint $table) {
            $table->integer("nb_max_student_by_team")->nullable()->after('slug');
            $table->integer("point_win")->nullable()->after('slug');
            $table->integer("point_contribution")->nullable()->after('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_challenges', function (Blueprint $table) {
            $table->dropColumn("point_win");
            $table->dropColumn("point_contribution");
        });

        Schema::table('challenges', function (Blueprint $table) {
            $table->dropColumn("point_win");
            $table->dropColumn("point_contribution");
            $table->dropColumn("nb_max_student_by_team");
        });
    }
}
