<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChallengesTableForObjectifsAndReglementation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("challenges", function (Blueprint $table) {
            $table->text("reglementation");
            $table->text("objectifs");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("challenges", function (Blueprint $table) {
            $table->dropColumn("reglementation");
            $table->dropColumn("objectifs");
        });
    }
}
