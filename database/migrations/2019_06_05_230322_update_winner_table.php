<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWinnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("challenges_has_winners", function (Blueprint $table) {
            $table->dropColumn("fk_user_id");
            $table->integer("fk_winner_id");
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("challenges_has_winners", function (Blueprint $table) {
            $table->integer("fk_user_id");
            $table->dropColumn("fk_winner_id");
        });
    }
}
