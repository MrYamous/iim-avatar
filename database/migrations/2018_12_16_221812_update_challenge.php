<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChallenge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("challenges_has_renderer_members", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_challenge_id")->unsigned()->references('id')->on('challenges')->nullable();
            $table->integer("fk_user_id")->unsigned()->references('id')->on('users')->nullable();
            $table->integer("fk_render_id")->unsigned()->references('id')->on('renders')->nullable();
            $table->timestamps();
        });


        Schema::create("renders", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name")->nullable();
            $table->string("slug")->nullable();
            $table->string("description")->nullable();
            $table->integer("fk_file_id")->unsigned()->references('id')->on('files')->nullable();
            $table->integer("fk_user_id")->unsigned()->references('id')->on('users')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("challenges_has_renderer_members");
        Schema::dropIfExists("renders");

    }
}
