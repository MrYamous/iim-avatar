<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTeamConversation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cms_conversations", function (Blueprint $table) {
            $table->integer("team")->default(0);
            $table->integer("fk_team_id")->nullable()->unsigned()->reference('id')->on('cms_teams');
        });
        Schema::dropIfExists("cms_teams_chats");
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cms_conversations", function (Blueprint $table) {
            $table->dropColumn("team");
            $table->dropColumn("fk_team_id");
        });
    }
}
