<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTeamHasChallengeTableReplaceFkByState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("team_has_challenge", function (Blueprint $table) {
            $table->dropColumn("fk_state_id");
            $table->string("state")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("team_has_challenge", function (Blueprint $table) {
           $table->dropColumn("state");
        });
    }
}
