<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablesForChatFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_teams', function (Blueprint $table) {
            $table->integer('fk_conversation_id')->nullable()->unsigned()->reference('id')->on('cms_conversations')->after('complete');
        });
        Schema::table('cms_conversations', function (Blueprint $table) {
            $table->dropColumn('team');
            $table->dropColumn('fk_team_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_teams', function (Blueprint $table) {
            $table->dropColumn('fk_conversation_id');
        });
        Schema::table('cms_conversations', function (Blueprint $table) {
            $table->integer("team")->default(0);
            $table->integer("fk_team_id")->nullable()->unsigned()->reference('id')->on('cms_teams');
        });
    }
}
