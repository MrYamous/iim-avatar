<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->text("name")->nullable();
            $table->string("slug")->nullable();
            $table->integer("complete")->default(0);
            $table->integer("fk_creator_id")->unsigned()->reference('id')->on('users');
            $table->timestamps();
        });
        
        Schema::create('team_has_user', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('fk_team_id');
            $table->integer("fk_user_id");
            $table->timestamps();
        });
        
        Schema::create('team_has_invited_user', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('fk_team_id');
            $table->integer("fk_user_id");
            $table->timestamps();
        });
        
        Schema::create("team_has_challenge", function (Blueprint $table) {
            $table->increments("id");
            $table->integer('fk_team_id');
            $table->integer("fk_challenge_id");
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_teams');
        Schema::dropIfExists('team_has_user');
        Schema::dropIfExists('team_has_invited_user');
        Schema::dropIfExists('team_has_challenge');
    }
}
