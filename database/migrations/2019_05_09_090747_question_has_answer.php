<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuestionHasAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // QUESTIONS TABLE
        Schema::create('cms_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->integer('fk_file_id')->nullable()->unsigned()->reference('id')->on('files');
            $table->timestamps();
        });

        // ANSWER TABLE
        Schema::create('cms_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->integer('fk_clan_id');
            $table->timestamps();
        });

        // QUESTION HAS ANSWER
        Schema::create('question_has_answer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_answer_id')->unsigned()->reference('id')->on('cms_answers');
            $table->integer('fk_question_id')->unsigned()->reference('id')->on('cms_questions');
            $table->timestamps();
        });

        // USER HAS RESPONSE TABLE
        Schema::create('user_has_answer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_user_id')->unsigned()->reference('id')->on('cms_users');
            $table->integer('fk_answer_id')->unsigned()->reference('id')->on('cms_answers');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_has_answer');
        Schema::dropIfExists('user_has_answer');
        Schema::dropIfExists('cms_answers');
        Schema::dropIfExists('cms_questions');
    }
}
