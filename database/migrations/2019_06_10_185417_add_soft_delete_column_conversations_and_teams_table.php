<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteColumnConversationsAndTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cms_conversations", function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table("cms_teams", function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table("cms_messages", function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cms_conversations", function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });
        Schema::table("cms_teams", function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });
        Schema::table("cms_messages", function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });
    }
}
