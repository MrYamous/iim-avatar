<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemsShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->integer("fk_file_id")->nullable();
            $table->integer("cost");
            $table->timestamps();
        });

        Schema::create("items_has_users", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("fk_items_id");
            $table->integer("fk_user_id");
            $table->timestamps();
        });

        Schema::table("users", function (Blueprint $table) {
            $table->integer("admin")->nullable()->default(0);
        });

        Schema::table("challenges", function (Blueprint $table) {
            $table->integer("all_clans")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_items');
        Schema::dropIfExists('items_has_users');
        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("admin");
        });
        Schema::table("challenges", function (Blueprint $table) {
            $table->dropColumn("all_clans");
        });
    }
}
