<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->nullable();
            $table->timestamps();
        });
        
        Schema::create("cms_messages", function (Blueprint $table) {
            $table->increments("id");
            $table->longText("message");
            $table->integer('fk_conversation_id')->unsigned()->reference('id')->on('cms_conversations');
            $table->integer('fk_user_id')->unsigned()->reference('id')->on('users');
            $table->timestamps();
        });
        
        Schema::create("user_has_conversation", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_user_id")->unsigned()->reference('id')->on('users');
            $table->integer("fk_conversation_id")->unsigned()->reference('id')->on('cms_conversations');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_conversations');
        Schema::dropIfExists("cms_messages");
        Schema::dropIfExists("user_has_conversation");
    }
}
