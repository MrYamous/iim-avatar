<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllMigration2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clans', function (Blueprint $table) {
            $table->integer("levels")->nullable();
            $table->integer("fk_logo_id")->nullable()->unsigned()->references('id')->on('files');

        });

        Schema::create("clans_has_file", function(Blueprint $table){
            $table->increments("id");
            $table->integer("fk_file_id")->nullable();
            $table->integer("fk_clan_id")->nullable();
        });

        Schema::create("clans_has_user", function(Blueprint $table){
            $table->increments("id");
            $table->integer("fk_user_id")->nullable();
            $table->integer("fk_clan_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project', function (Blueprint $table) {
            //
        });
    }
}
