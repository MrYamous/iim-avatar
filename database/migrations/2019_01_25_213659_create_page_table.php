<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('title');
            $table->string('url')->index();
            $table->unsignedInteger('fk_image_id')->nullable();
            $table->longText('content');
            $table->longText('seo_title')->nullable();
            $table->longText('seo_description')->nullable();
            $table->longText('og_title')->nullable();
            $table->longText('og_description')->nullable();
            $table->unsignedInteger('og_image')->nullable();
            $table->unsignedInteger('parent_page')->nullable();
            $table->timestamps();
            // Foreign keys
            //$table->foreign('fk_image_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('cascade');
        });
        
        Schema::create('cms_blocks_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->unsignedInteger('fk_page_id');
            $table->timestamps();
            // Foreign key
            //$table->foreign('fk_page_id')->references('id')->on('cms_pages');
        });
        
        Schema::create('cms_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['text', 'image']);
            $table->string('key')->index();
            $table->longText('value');
            $table->unsignedInteger('fk_page_id');
            $table->unsignedInteger('fk_category_id')->nullable();
            $table->timestamps();
            // Primary key
            //$table->foreign('fk_page_id')->references('id')->on('cms_pages')->onUpdate('cascade')->onDelete('cascade');
            //$table->foreign('fk_category_id')->references('id')->on('cms_blocks_categories');
        });
        
        Schema::create('cms_post', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('subtitle');
            $table->text('excerpt');
            $table->longText('content');
            $table->integer('publish');
            $table->integer('fk_file_id')->on('id')->reference('files')->nullable();
            $table->timestamps();
        });
        
        Schema::create('cms_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->timestamps();
        });
        
        Schema::create('category_post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_post_id')->on('id')->reference('cms_post')->nullable();
            $table->integer('fk_category_id')->on('id')->reference('cms_category')->nullable();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_blocks_categories');
        Schema::dropIfExists('cms_blocks');
        Schema::dropIfExists('cms_pages');
        Schema::dropIfExists('cms_blog');
        Schema::dropIfExists('cms_post');
        Schema::dropIfExists('cms_category');
        Schema::dropIfExists('category_post');
    }
}
