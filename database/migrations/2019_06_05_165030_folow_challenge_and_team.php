<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FolowChallengeAndTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_has_follow_challenge", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_user_id")->unsigned()->referece('id')->on('users');
            $table->integer("fk_challenge_id")->unsigned()->referece('id')->on('challenges');
            $table->timestamps();
        });
        
        Schema::create("user_has_follow_teams", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_user_id")->unsigned()->referece('id')->on('users');
            $table->integer("fk_team_id")->unsigned()->referece('id')->on('cms_teams');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("user_has_follow_challenge");
        Schema::dropIfExists("user_has_follow_teams");
    }
}
