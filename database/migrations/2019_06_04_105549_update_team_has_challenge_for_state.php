<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTeamHasChallengeForState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_has_challenge', function (Blueprint $table) {
            $table->integer('fk_state_id')->unsigned()->reference('id')->on('cms_challenge_state')->nullable();
        });
        
        Schema::create('cms_challenge_state', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_has_challenge');
        Schema::dropIfExists('cms_challenge_state');
    }
}
