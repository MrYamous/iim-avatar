<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Challenge has each users
//        Schema::create('challenges_has_users', function (Blueprint $table) {
//            $table->increments("id");
//            $table->integer("fk_challenge_id");
//            $table->integer("fk_user_id");
//        });

        //Score of each user
//        Schema::table('scores', function (Blueprint $table) {
//            $table->dropColumn("description");
//            $table->dropColumn("challenge_id");
//            $table->dropColumn("user_id");
//            $table->integer("fk_user_id");
//        });

        Schema::dropIfExists('scores');

        Schema::create("ssks_users", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("fk_user_id");
            $table->integer("fk_ssk_users");
            $table->integer("score");
        });

        Schema::table("users", function (Blueprint $table) {
            $table->integer("fk_clan_id")->references('id')->on('clans')->nullable();
            $table->dropColumn("avatar");
            $table->integer("fk_avatar_id")->references('id')->on('files')->nullable();
            $table->integer('fk_axe_id')->nullable();
            $table->integer('fk_promotion_id')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("ssks_users");
        Schema::dropIfExists("challenges_has_users");
        Schema::dropIfExists("clans_has_user");
        Schema::dropIfExists("clans_has_file");
    }
}
