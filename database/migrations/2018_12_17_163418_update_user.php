<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer("levels")->nullable()->default(0);
            $table->integer("gold")->nullable()->default(0);
            $table->integer("points")->nullable()->default(0);
        });


        Schema::create("cms_users_level", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("level");
            $table->integer("start");
            $table->integer("end");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("levels");
            $table->dropColumn("gold");
            $table->dropColumn("points");
        });
        Schema::dropIfExists("cms_users_level");
    }
}
