<?php

use App\Models\Chat\Conversation;
use App\Models\User\Role;
use App\Models\User\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateUsersTable();

        if ($this->command->confirm('Create simply Hugo, Rémi, Jean user entries?')) {
            $this->createSpecificUsers([
                ['first_name' => 'Rémi', 'last_name' => 'Guillard', 'email' => 'glrd.remi@gmail.com'],
                ['first_name' => 'Hugo', 'last_name' => 'Doe', 'email' => 'hugo@gmail.com'],
                ['first_name' => 'Jean', 'last_name' => 'Saunie', 'email' => 'jean.saunie@gmail.com'],
                ['first_name' => 'Arthur', 'last_name' => 'Kenfack', 'email' => 'arthur@nemesia.fr'],
                ['first_name' => 'Virak', 'last_name' => 'Mey', 'email' => 'virak@nemesia.fr']
            ]);
        } else {
            $this->createRandomUsers();
        }
    }

    private function createSpecificUsers($users)
    {
        foreach ($users as $user) {
            $user = User::create([
                'first_name' => $user['first_name'],
                'last_name'  => $user['last_name'],
                'email'      => $user['email'],
                'slug'       => str_slug($user['first_name']),
                'fk_clan_id' => rand(1, 3),
                'password'   => bcrypt('password'),
                'admin'      => 1,
            ]);
            $role = Role::where('name', "responsable")->first();
            if ($role && !$user->hasRole('responsable')) $user->attachRole($role);

            $conversation = Conversation::find(1);
            if ($conversation) $conversation->getMembers()->attach($user);
        }
        $this->command->info('UsersSeeder completed!');
    }

    private function createRandomUsers()
    {
        for ($i = 0; $i < 20; $i++) {
            $faker = Faker\Factory::create('fr_FR');
            $user = User::create([
                'first_name' => $faker->firstName . $i,
                'last_name'  => $faker->lastName,
                'email'      => $faker->email,
                'slug'       => str_slug($faker->firstName . $i),
                'fk_clan_id' => rand(1, 5),
                'password'   => bcrypt('password')
            ]);

            $role = Role::where('name', "student")->first();
            if ($role && !$user->hasRole('student')) $user->attachRole($role);

            $conversation = Conversation::find(1);
            if ($conversation) $conversation->getMembers()->attach($user);
        }
    }

    private function truncateUsersTable()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
