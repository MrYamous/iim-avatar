<?php

use App\Models\Promotion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class PromotionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncatePromotionsTable();

        if ($this->command->confirm('Create default promotions?')) {
            for ($i = 1; $i <= 5; $i++) {
                Promotion::create([
                    "name" => 'Année ' . $i,
                    "slug" => str_slug('Année ' . $i)
                ]);
            }
            $this->command->info('PromotionsSeeder completed!');
        }
    }

    private function truncatePromotionsTable()
    {
        Schema::disableForeignKeyConstraints();
        Promotion::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
