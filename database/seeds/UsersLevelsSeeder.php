<?php

use App\Models\UsersLevel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->truncateUsersLevelsTable();

        if ($this->command->confirm("Create default levels?")) {
            $levels = [
                ['level' => 1, 'start' => 0, 'end' => 10],
                ['level' => 2, 'start' => 11, 'end' => 20],
                ['level' => 3, 'start' => 21, 'end' => 30],
                ['level' => 4, 'start' => 31, 'end' => 40],
            ];

            foreach ($levels as $level) {
                UsersLevel::create([
                    'level' => $level['level'],
                    'start' => $level['start'],
                    'end' => $level['end'],
                ]);
            }
            $this->command->info('UsersLevelsSeeder completed!');
        }
    }

    private function truncateUsersLevelsTable()
    {
        Schema::disableForeignKeyConstraints();
        UsersLevel::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
