<?php

use App\Models\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncatePagesTable();

        if ($this->command->confirm('Create default pages?')) {
            $pages = [
                "Home",
                "Clans",
                "Challenges",
                "Classements",
                "Profil",
                "Connexion",
                "Inscription",
                "Quiz",
                "Navbar",
            ];
            foreach ($pages as $page) {
                Page::create([
                    "title" => $page,
                    "url" => url("/" . str_slug($page)),
                    "content" => $page
                ]);
            }
            $this->command->info('PagesSeeder completed!');
        }
    }

    private function truncatePagesTable()
    {
        Schema::disableForeignKeyConstraints();
        Page::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
