<?php

use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable(["clans", "clans_has_file", "clans_has_user", "challenges_has_clans"]);
        if ($this->command->confirm("Create default houses ?")) {
            $desc = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aut commodi exercitationem ipsa molestiae omnis quas qui sapiente similique, unde veniam voluptatem! Aspernatur consequuntur dolor, earum minus nam similique ut.";
            $clans = [
                ["name" => "Le Clan de Phoenix", 'description' => $desc],
                ["name" => "L'Ordre du Kraken", 'description' => $desc],
                ["name" => "La Garde de Cerbère", 'description' => $desc]
            ];
            $file = new \App\Models\File();
            $file->content = "maison";
            $file->file = "http://i.imgur.com/YxFha5Q.png";
            $file->type = "png";
            $file->fk_user_id = 1;
            $file->save();
            
            foreach ($clans as $clan) {
                $c = new \App\Models\Clan();
                $titl = $clan['name'];
                $c->title = $titl;
                $c->slug = str_slug($titl);
                $c->description = $clan['description'];
                $c->fk_logo_id = $file->id;
                $c->save();
                $c->getImages()->attach($file->id);
            }
        }
    }
}
