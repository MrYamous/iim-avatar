<?php

use App\Models\Item;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ShopItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateShopItemsTable();
        
        if ($this->command->confirm("Create default shop items?")) {
            $file = new \App\Models\File();
            $file->file = "https://www.breakflip.com/uploads/WoW/Aluqat/Progress_Antorus/Antorus_Taeshalach.jpg";
            $file->content = str_random(34);
            $file->type = "png";
            
            $file->fk_user_id = 1;
            $file->save();
            for ($i = 0; $i < 20; $i++) {
                $name = "Lance du tonnere $i";
                Item::create([
                    'name'        => $name,
                    'slug'        => str_slug($name),
                    'cost'        => rand(1, 40),
                    'description' => "Un item génial !",
                    'fk_file_id'  => $file->id
                ]);
            }
            $this->command->info('ShopItemsSeeder completed!');
        }
    }
    
    private function truncateShopItemsTable()
    {
        Schema::disableForeignKeyConstraints();
        Item::truncate();
        Schema::enableForeignKeyConstraints();
    }
    
}