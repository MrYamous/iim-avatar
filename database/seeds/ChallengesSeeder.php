<?php

use App\Models\Challenge;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ChallengesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateChallengeTables();

        if ($this->command->confirm("Create default challenges ?")) {
            for ($i = 1; $i <= 10; $i++) {
                $faker = Faker\Factory::create('fr_FR');
                Challenge::create([
                    'title'              => $faker->sentence(rand(3, 10)),
                    'description'        => $faker->sentence(rand(3, 10)),
                    'point_contribution' => rand(20, 60),
                    'point_win'          => rand(51, 600),
                    'start_date'         => Carbon::createFromFormat('d/m/Y', Carbon::now()->subDay(rand(3, 20))->format('d/m/Y')),
                    'end_date'           => Carbon::createFromFormat('d/m/Y', Carbon::now()->addDay(rand(3, 20))->format('d/m/Y')),
                    'rendu'              => $faker->sentence(),
                    'reglementation'     => $faker->sentence(),
                    'objectifs'          => $faker->sentence(),
                    'validated'          => rand(0, 1),
                    'states'             => json_encode(["Début", "Fin"]),
                ]);
            }
        }
    }

    private function truncateChallengeTables()
    {
        truncateTable([
            'challenges',
            'challenges_has_clans',
            'challenges_has_constraint',
            'challenges_has_files',
            'challenges_has_renderer_members',
            'challenges_has_ssk',
            'challenges_has_users',
            'challenges_has_winners',
            'challenges_has_files',
            'team_has_challenge',
        ]);
    }

}
