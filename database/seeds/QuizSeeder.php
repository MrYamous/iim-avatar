<?php

use App\Models\Quiz\Answer;
use App\Models\Quiz\Question;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class QuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateQuizTables();

        if ($this->command->confirm("Create default questions and answers?")) {
            for ($i = 0; $i < 10; $i++) {
                $faker = \Faker\Factory::create();
                $question = Question::create([
                    'title' => $faker->sentence(8),
                    'content' => $faker->paragraph(2)
                ]);

                $arrayOfIdAnswer = [];
                for ($j = 0; $j < 3; $j++) {
                    $answer = Answer::create([
                        'title' => $faker->sentence(6) . $j,
                        'fk_clan_id' => rand(1, 3)
                    ]);
                    $arrayOfIdAnswer[] = $answer->id;
                }
                $question->getAnswers()->attach($arrayOfIdAnswer);
            }
            $this->command->info('QuizSeeder completed!');
        }
    }

    private function truncateQuizTables()
    {
        Schema::disableForeignKeyConstraints();
        truncateTable([
            'question_has_answer',
        ]);
        Answer::truncate();
        Question::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
