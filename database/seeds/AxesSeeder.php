<?php

use App\Models\Axe;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class AxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateAxesTable();

        if ($this->command->confirm('Create default axes ?')) {
            $axes = [
                "Développement web",
                "Animation 3D",
                "Jeux video",
                "Communication digitale & e-business",
                "Création et design",
            ];
            foreach ($axes as $axe) {
                Axe::create([
                    "name" => $axe,
                    "slug" => str_slug($axe)
                ]);
            }
            $this->command->info('AxesSeeder completed!');
        }
    }

    private function truncateAxesTable()
    {
        Schema::disableForeignKeyConstraints();
        Axe::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
