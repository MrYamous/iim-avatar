<?php

use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use Illuminate\Database\Seeder;

class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable(["cms_globals_chats", "user_has_conversation"]);
        $conversation = Conversation::create(['name' => 'Global']);
        if ($this->command->confirm("Do you want seed global chat with 100 messages ?")) {
            for ($i = 1; $i < 100; $i++) {
                $faker = \Faker\Factory::create();
                Message::create([
                    'message'            => $faker->sentence(rand(12, 34)),
                    'fk_user_id'         => rand(1, 3),
                    'fk_conversation_id' => $conversation->id,
                ]);
            }
        }
    }
}
