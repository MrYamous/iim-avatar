<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if ($this->command->confirm("Do you want to set passport?")) $this->setPassportInstall();

        $this->call([
            LaratrustSeeder::class,
            ChallengesSeeder::class,
            ChatSeeder::class,
            UsersSeeder::class,
            HouseSeeder::class,
            AxesSeeder::class,
            PromotionsSeeder::class,
            PagesSeeder::class,
            ShopItemsSeeder::class,
            QuizSeeder::class,
            UsersLevelsSeeder::class,
        ]);

        $this->command->info("Done your seed is complete !");
    }

    private function setPassportInstall()
    {
        truncateTable([
            "oauth_clients",
            "oauth_personal_access_clients",
            "oauth_refresh_tokens",
            "oauth_access_tokens",
        ]);
        $this->command->info("Passport installing..");
        exec("php artisan passport:install");
        exec("php artisan passport:install");
        $this->command->info("Done !");
    }


}
