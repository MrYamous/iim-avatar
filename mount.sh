#!/bin/bash
ENV="$1"

#cp .env.docker.example .env
composer install
#php artisan key:generate

case "$ENV" in
  "all") php artisan migrate:fresh && chown -R :www-data /var/api.nemesia.fr && chmod -R 777 /var/api.nemesia.fr
  ;;
esac

echo Setup Done
#php artisan serve --host=0.0.0.0 --port=8840
