<?php

Route::group(["prefix" => "/ssk", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "SskController@store"])->name('admin.ssk.store');
    Route::get('/create', ['uses' => "SskController@create"])->name('admin.ssk.create');
    Route::get('/', ['uses' => "SskController@index"])->name('admin.ssk.index');
    Route::get('/{slug}', ['uses' => "SskController@show"])->name('admin.ssk.show');
    Route::delete('/{slug}', ['uses' => "SskController@delete"])->name('admin.ssk.delete');
});