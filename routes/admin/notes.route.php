<?php

Route::group(["prefix" => "/notes"], function () {
    Route::get('/', 'NotesController@index')->name('admin.notes.index');
    Route::delete('/{id}', 'NotesController@destroy')->name('admin.notes.destroy');
});
