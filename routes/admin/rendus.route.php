<?php

Route::group(["prefix" => "/rendus", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "RenderController@store"])->name('admin.render.store');
    Route::get('/create', ['uses' => "RenderController@create"])->name('admin.render.create');
    Route::get('/', ['uses' => "RenderController@index"])->name('admin.render.index');
    Route::get('/{slug}', ['uses' => "RenderController@show"])->name('admin.render.show');
    Route::delete('/{slug}', ['uses' => "RenderController@delete"])->name('admin.render.delete');
});