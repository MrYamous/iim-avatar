<?php

Route::group(["prefix" => "/items", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "ItemsController@store"])->name('admin.items.store');
    Route::get('/create', ['uses' => "ItemsController@create"])->name('admin.items.create');
    Route::get('/', ['uses' => "ItemsController@index"])->name('admin.items.index');
    Route::get('/{slug}', ['uses' => "ItemsController@show"])->name('admin.items.show');
    Route::delete('/{slug}', ['uses' => "ItemsController@delete"])->name('admin.items.delete');
});