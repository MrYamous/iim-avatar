<?php

Route::group(["prefix" => "/constraints", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "ConstraintsController@store"])->name('admin.constraints.store');
    Route::get('/create', ['uses' => "ConstraintsController@create"])->name('admin.constraints.create');
    Route::get('/', ['uses' => "ConstraintsController@index"])->name('admin.constraints.index');
    Route::get('/{slug}', ['uses' => "ConstraintsController@show"])->name('admin.constraints.show');
    Route::delete('/{slug}', ['uses' => "ConstraintsController@delete"])->name('admin.constraints.delete');
});