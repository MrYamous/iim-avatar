<?php


Route::group(["prefix" => "/challenges-state"], function (){
    Route::get('/', ["uses" => "ChallengesStateController@index"])->name('admin.challenges_state.index');
    Route::get('/create', ["uses" => "ChallengesStateController@create"])->name('admin.challenges_state.create');
    Route::post('/', ["uses" => "ChallengesStateController@store"])->name('admin.challenges_state.store');
    Route::get('/{slug}', ["uses" => "ChallengesStateController@show"])->name('admin.challenges_state.show');
    Route::get('/edit/{slug}', ["uses" => "ChallengesStateController@edit"])->name('admin.challenges_state.edit');
    Route::post('/edit/{slug}', ["uses" => "ChallengesStateController@update"])->name('admin.challenges_state.update');
    Route::delete('/{slug}', ["uses" => "ChallengesStateController@delete"])->name('admin.challenges_state.delete');
});
