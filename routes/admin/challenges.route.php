<?php
Route::group(["prefix" => "/challenges"], function () {

    Route::group(["prefix" => "/unvalidated"], function () {

        Route::get('/', 'ChallengesController@getChallengesNonValides')->name('admin.challenges.index_challenges_non_valides');
        Route::get('/edit/{slug}', 'ChallengesController@edit')->name('admin.challenges.edit');
        Route::post('/edit/{slug}', 'ChallengesController@update')->name('admin.challenges.update');
//        Route::get('/create', 'ChallengesController@create')->name('admin.challenges.create');
//        Route::get('/set-winner/{slug}/{render}', 'ChallengesController@setWinner')->name('admin.challenges.winner');
//        Route::post('/create', 'ChallengesController@store')->name('admin.challenges.store');
        Route::get('/{slug}', 'ChallengesController@showChallengesNotValidated')->name('admin.challenges.showChallengesNotValidated');
        Route::post('/{slug}', 'ChallengesController@showUpdate')->name('admin.challenges.showUpdate');
        Route::delete('/{id}', 'ChallengesController@delete')->name('admin.challenges.destroy');
    });


    Route::get('/', 'ChallengesController@getChallengesValides')->name('admin.challenges.index_challenges_valides');

    Route::get('/edit/{slug}', 'ChallengesController@edit')->name('admin.challenges.edit');
    Route::post('/edit/{slug}', 'ChallengesController@update')->name('admin.challenges.update');
    Route::get('/create', 'ChallengesController@create')->name('admin.challenges.create');
    Route::get('/set-winner/{slug}/{id}', 'ChallengesController@setWinner')->name('admin.challenges.winner');
    Route::post('/create', 'ChallengesController@store')->name('admin.challenges.store');
    Route::get('/{slug}', 'ChallengesController@show')->name('admin.challenges.show');
    Route::delete('/{id}', 'ChallengesController@delete')->name('admin.challenges.destroy');


});
