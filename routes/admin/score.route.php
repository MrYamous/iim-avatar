<?php
Route::group(["prefix" => "/scores"], function () {
    Route::get('/', 'ScoresController@index')->name('admin.scores.index');
    Route::get('/{slug}', 'ScoresController@show')->name('admin.scores.show');
});