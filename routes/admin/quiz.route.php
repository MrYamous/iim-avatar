<?php

Route::group(["prefix" => "/quiz", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "QuizController@store"])->name('admin.quiz.store');
    Route::get('/create', ['uses' => "QuizController@create"])->name('admin.quiz.create');
    Route::get('/', ['uses' => "QuizController@index"])->name('admin.quiz.index');
    Route::get('/edit/{id}', ['uses' => "QuizController@edit"])->name('admin.quiz.edit');
    Route::post('/edit/{id}', ['uses' => "QuizController@update"])->name('admin.quiz.update');
    Route::get('/{slug}', ['uses' => "QuizController@show"])->name('admin.quiz.show');
    Route::delete('/{slug}', ['uses' => "QuizController@delete"])->name('admin.quiz.delete');
});
