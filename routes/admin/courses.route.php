<?php

Route::group(["prefix" => "/courses"], function () {
    Route::get('/', 'CoursesController@index')->name('admin.courses.index');
    Route::get('/{slug}', 'CoursesController@show')->name('admin.courses.show');
    Route::delete('/{id}', 'CoursesController@destroy')->name('admin.courses.destroy');
});
