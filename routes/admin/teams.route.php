<?php

Route::group(['prefix' => '/teams'], function (){
    Route::get('/', ['uses' => 'TeamsController@index'])->name('admin.teams.index');
    Route::get('/{id}', ['uses' => 'TeamsController@show'])->name('admin.teams.show');
    Route::delete('/{id}', ['uses' => 'TeamsController@delete'])->name('admin.teams.delete');
});
