<?php

Route::group(["prefix" => "/level", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "LevelsController@store"])->name('admin.levels.store');
    Route::get('/create', ['uses' => "LevelsController@create"])->name('admin.levels.create');
    Route::get('/', ['uses' => "LevelsController@index"])->name('admin.levels.index');
    Route::get('/{slug}', ['uses' => "LevelsController@show"])->name('admin.levels.show');
    Route::delete('/{slug}', ['uses' => "LevelsController@delete"])->name('admin.levels.delete');
});