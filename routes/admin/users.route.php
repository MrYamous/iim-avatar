<?php

Route::group(["prefix" => "/users"], function () {
    Route::group(['prefix' => '/roles'], function () {
        Route::get('/', ['uses' => "Cms\RolesController@index"]);
        Route::get('/create', ['uses' => "Cms\RolesController@create"]);
        Route::get('/{id}', ['uses' => "Cms\RolesController@edit"]);
        Route::put('/{id}', ['uses' => "Cms\RolesController@update"]);
        Route::post('/', ['uses' => "Cms\RolesController@store"]);
        Route::delete('/{id}', ['uses' => "Cms\RolesController@delete"]);
    });
    
    Route::get('/', 'UserController@index')->name('admin.users.index');
    Route::get('/create', 'UserController@create')->name('admin.users.create');
    Route::post('/', 'UserController@store')->name('admin.users.store');
    
    Route::get('/{id}', 'UserController@show')->name('admin.users.show');
    Route::delete('/{id}', 'UserController@destroy');
    
});

Route::group(["prefix" => "/levels/users", "namespace" => "Cms"], function () {
    Route::post('/', ['uses' => "UserLevelsController@store"])->name('admin.users.levels.store');
    Route::get('/create', ['uses' => "UserLevelsController@create"])->name('admin.users.levels.create');
    Route::get('/', ['uses' => "UserLevelsController@index"])->name('admin.users.levels.index');
    Route::get('/{id}', ['uses' => "UserLevelsController@show"])->name('admin.users.levels.show');
    Route::delete('/{id}', ['uses' => "UserLevelsController@delete"])->name('admin.users.levels.delete');
});