<?php

Route::group(["namespace" => "Cms"], function () {
    
    
    Route::group(["prefix" => "/clans"], function () {
        Route::post('/', ['uses' => "ClansController@store"])->name('admin.clans.store');
        Route::get('/create', ['uses' => "ClansController@create"])->name('admin.clans.create');
        Route::get('/', ['uses' => "ClansController@index"])->name('admin.clans.index');
        Route::get('/edit/{slug}', ['uses' => "ClansController@edit"])->name('admin.clans.edit');
        Route::post('/edit/{slug}', ['uses' => "ClansController@update"])->name('admin.clans.edit');
        Route::get('/{slug}', ['uses' => "ClansController@show"])->name('admin.clans.show');
        Route::delete('/{slug}', ['uses' => "ClansController@delete"])->name('admin.clans.delete');
    });
});