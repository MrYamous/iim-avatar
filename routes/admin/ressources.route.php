<?php

Route::resources([
    'pages'           => 'Cms\PagesController',
    'blog/post'       => 'Cms\Blog\PostController',
    'blog/categories' => 'Cms\Blog\CategoriesController'
]);