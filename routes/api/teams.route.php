<?php


Route::prefix("teams")->namespace('Teams')->group(function () {

    Route::get('/join/{team}', 'TeamsController@joinTeam');
    Route::get('/follow', 'TeamsController@getFollow');
    Route::get('/creator', 'TeamsController@getTeamWhereImCreator');
    Route::get('/follow/{id}', 'TeamsController@setFollow');
    Route::get('/invite', 'TeamsController@listInvitedTeam');
    Route::post('/leave/{team}', 'TeamsController@leaveTeam');

    Route::get('/', 'TeamsController@index');
    Route::post('/', 'TeamsController@store');
    Route::put('/{team}', 'TeamsController@update');
    Route::get('/{team}', 'TeamsController@show');
    Route::get('{team}/edit', 'TeamsController@edit');
    Route::delete('/{team}', 'TeamsController@destroy');

});
