<?php

require __DIR__ . '/file.route.php';

Route::get('/test', function () {
    return response()->json(['success' => "Api routes works"], 200);
});

Route::get('/scores/{slug}', 'ApiController@getScore');

Route::get('/search', 'ApiController@search');
Route::post('/login', ['uses' => "ApiController@loginUser"]);
Route::get('/logout', ['uses' => "ApiController@logoutUser"]);
Route::post('/register', ["uses" => "ApiController@registerUser"]);

Route::get('/', function () {
    return [
        "status"   => "success",
        "message"  => "Welcome To Nemesia Api ",
        "api_link" => request()->getUri(),
        "version"  => env("APP_VERSION")
    ];
});


Route::get('/message/{message}', function () {
    event(new \App\Events\newMessage(request()->message));
    
    return [
        'status' => "success"
    ];
});