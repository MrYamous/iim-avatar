<?php

Route::post('/files', ['uses' => "FileController@storeFile"]);
Route::delete('/files/{id}', ['uses' => "FileController@deleteFile"]);