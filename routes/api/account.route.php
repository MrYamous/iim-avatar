<?php

Route::group(["prefix" => "account"], function () {
    Route::get('/edit', ['uses' => "AccountController@edit"]);
    Route::post('/update', ['uses' => 'AccountController@update']);
});
