<?php

require __DIR__ . '/chat.route.php';
require __DIR__ . '/teams.route.php';
require __DIR__ . '/items.route.php';
require __DIR__ . '/challenge.route.php';
require __DIR__ . '/account.route.php';

Route::group(['prefix' => '/leaderboard'], function () {
    require __DIR__ . '/leaderboard.route.php';
}
);
Route::get('/dashboard', ['uses' => 'ApiController@dashboard']);

Route::group(['prefix' => 'quiz'], function () {
    Route::get('/', ['uses' => 'QuizController@index']);
    Route::post('/answers', ['uses' => 'QuizController@saveAnswers']);
});

Route::get('/dashboard', 'ApiController@getDashboard');

Route::group(["prefix" => "users"], function () {
    Route::get('/', ['uses' => "ApiController@getUser"]);
    Route::get('/clan', ['uses' => "UserController@getClan"]);
    Route::post('/avatar', ['uses' => "ApiController@saveAvatar"]);
    Route::get('/{id}', ['uses' => "ApiController@show"]);
});

Route::group(['prefix' => '/clans'], function () {
    require __DIR__ . '/clans.route.php';
});
