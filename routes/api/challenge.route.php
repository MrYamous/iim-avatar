<?php

Route::prefix('challenge')->group(function () {
    Route::get('/{type}', ['uses' => "ChallengesController@index"]);
    Route::get('/user', ['uses' => "ChallengesController@getChallengeOfUser"]);
    Route::get('/show/{id}', ['uses' => "ChallengesController@show"]);
    Route::put('/{id}', ['uses' => "ChallengesController@updateState"]);
    
    Route::post('/store', ['uses' => "ChallengesController@store"]);
    Route::post('/join/{challenge}', ["uses" => "ChallengesController@joinChallenge"]);
    Route::get('/follow', 'ChallengesController@getFollow');
    Route::get('/follow/{id}', 'ChallengesController@setFollow');
    Route::post('/{id}/update-state', 'ChallengesController@updateStateChallenge');
    
});
