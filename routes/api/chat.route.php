<?php


Route::group(['prefix' => '/chat', 'namespace' => "Chat"], function () {

    // Conversations
    Route::get('/conversations', 'ConversationsController@index');

    Route::get('/{conversation}', 'ChatController@show');
    Route::get('/', 'ChatController@index');
    Route::post('/', 'ChatController@createConversation');
    Route::post('/{conversation}', 'ChatController@storeMessage');

});
