<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "App\StaticsController@redirect");

Route::group(["middleware" => "guest"], function () {
    Route::get('/get/login', 'App\StaticsController@showLogin');
    Route::get('/get/register', 'App\StaticsController@showRegister');
});



//Route::get('/', ['uses' => "App\StaticsController@home"]);
Route::post('/', ['uses' => "App\StaticsController@knowEntrie"]);
Route::get('/success/{slug}', ['uses' => "App\StaticsController@successForClan"]);
Route::get('/clans', ['uses' => "App\ClanController@showHouses"])->name('house.show');
Route::get('/clans/{slug}', ['uses' => "App\ClanController@showUserClan"]);
Route::get('/classements', ['uses' => "App\LeaderBoardController@showLeaderBoard"]);
Route::get('/challenges', ['uses' => "App\ChallengesController@showChallenge"]);
Route::get('/submit/challenges/{slug}', ['uses' => "App\ChallengesController@submitChallenge"]);
Route::post('/render/challenges/{slug}', ['uses' => "App\ChallengesController@renderChallenger"]);
Route::get('/reset/password/{token}', 'App\StaticsController@setPassword');
Route::post('/reset/password', 'App\StaticsController@updatePassword');
Route::get('/questions', "App\StaticsController@showQuestion");
Route::post('/questions', "App\StaticsController@submitQuestion");
Route::get('/news/{slug}', "App\StaticsController@showPost");

Route::get('/logout', function () {
    Auth::logout();
    
    return redirect()->back()->with("success", "vous vous etes bien deco");
});

Auth::routes();

Route::group(["middleware" => "auth"], function () {
    Route::get('/account', ['uses' => "App\AccountController@showAccount"]);
    Route::post('/account/edit', ['uses' => "App\AccountController@update"]);
    Route::get('/account/edit', ['uses' => "App\AccountController@showEdit"]);
    Route::post('/account/buy/item/{id}', ['uses' => "App\AccountController@buyItem"]);
    Route::post('/account/submit/clan/{id}', ['uses' => "App\AccountController@submitToClan"]);
});


Route::group(["prefix" => "/admin"], function () {
    Route::get('/login', ['uses' => "Admin\AdminController@showLogin"]);
    Route::post('/login', ['uses' => "Admin\AdminController@postLogin"]);
});