<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('chat-{conversation}', function ($user, $conversation) {
    if ($conversation === 'global') {
        $conversation = \App\Models\Chat\Conversation::with('getMembers', 'getMessages')->find(1);
    } else {
        $conversation = \App\Models\Chat\Conversation::with('getMembers', 'getMessages')->find($conversation);
    }
    return new \App\Http\Resources\Chat\ConversationResource($conversation);
});

Broadcast::channel('invited.to.team.{id}', function ($user, $id) {
    return $user->id === $id;
});

Broadcast::channel('join.team.{id}', function ($user, $id) {
    return $user->id === $id;
});

Broadcast::channel('complete.team.{id}', function ($user, $id) {
    return $user->id === $id;
});