<?php
/**
 * Created by PhpStorm.
 * User: glrd
 * Date: 15/11/2018
 * Time: 11:57
 */

Route::get('/', ['uses' => 'DashboardController@dashboard'])->name('admin');;

Route::group(['namespace' => 'Cms'], function () {
    Route::get('/blog/post/publish/{id}', 'Blog\PostController@publish');
    Route::get('/blog/post/un/publish/{id}', 'Blog\PostController@unPublish');
    Route::get('block/pages/{id}', 'BlocksController@show');
    Route::post('block/update/block/{id}', 'BlocksController@updateBlock');
    Route::post('block/delete/{id}', 'BlocksController@deleteBlock');
    Route::post('block/pages/submit/block/{id}', 'BlocksController@createBlock');
    Route::post('block/pages/submit/folder/{id}', 'BlocksController@createFolder');
    Route::get('block/pages/folder/block/{id}/{slug}', 'BlocksController@showBlocks');
    Route::get('block/pages/folder/block/{id}/{slug}/{blockId}', 'BlocksController@showBlocksDetails');
});

// Import dynamically all files into the routes/api/folder
foreach (scandir(dirname(__FILE__) . '/admin') as $filename) {
    if ($filename !== "." && $filename !== "..") {
        $path = dirname(__FILE__) . '/admin/' . $filename;
        if (is_file($path)) require $path;
    }
}
